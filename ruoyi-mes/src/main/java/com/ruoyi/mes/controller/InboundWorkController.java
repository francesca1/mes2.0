package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.mes.domain.Vo.InboundVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.InboundWork;
import com.ruoyi.mes.service.IInboundWorkService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 完工入库Controller
 * 
 * @author g4
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/inboundwork")
public class InboundWorkController extends BaseController
{
    @Autowired
    private IInboundWorkService inboundWorkService;

    /**
     * 查询完工入库列表
     */
    @PreAuthorize("@ss.hasPermi('mes:inboundwork:list')")
    @GetMapping("/list")
    public TableDataInfo list(InboundWork inboundWork)
    {
        startPage();
        List<InboundWork> list = inboundWorkService.selectInboundWorkList(inboundWork);
        return getDataTable(list);
    }

    /**
     * 导出完工入库列表
     */
    @PreAuthorize("@ss.hasPermi('mes:inboundwork:export')")
    @Log(title = "完工入库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, InboundWork inboundWork)
    {
        List<InboundWork> list = inboundWorkService.selectInboundWorkList(inboundWork);
        ExcelUtil<InboundWork> util = new ExcelUtil<InboundWork>(InboundWork.class);
        util.exportExcel(response, list, "完工入库数据");
    }

    /**
     * 获取完工入库详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:inboundwork:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(inboundWorkService.selectInboundWorkById(id));
    }

    /**
     * 新增完工入库
     */
    @PreAuthorize("@ss.hasPermi('mes:inboundwork:add')")
    @Log(title = "完工入库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InboundWork inboundWork)
    {
        return toAjax(inboundWorkService.insertInboundWork(inboundWork));
    }

    /**
     * 修改完工入库
     */
    @PreAuthorize("@ss.hasPermi('mes:inboundwork:edit')")
    @Log(title = "完工入库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InboundWork inboundWork)
    {
        return toAjax(inboundWorkService.updateInboundWork(inboundWork));
    }

    /**
     * 删除完工入库
     */
    @PreAuthorize("@ss.hasPermi('mes:inboundwork:remove')")
    @Log(title = "完工入库", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(inboundWorkService.deleteInboundWorkByIds(ids));
    }

    /**
     * 查看完工入库单
     */
    @GetMapping("/lookInbound")
    public AjaxResult lookInbound(@RequestParam String taskcardCode){
        return inboundWorkService.lookInboundByCode(taskcardCode);
    }



    /**
     * 确认完工入库
     */
    @PostMapping("/submitInbound")
    public AjaxResult submitInbound(@RequestBody InboundVo inboundVo){
        return inboundWorkService.submitInboundByVo(inboundVo);
    }
}
