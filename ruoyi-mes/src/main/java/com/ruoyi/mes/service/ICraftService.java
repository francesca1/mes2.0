package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.Craft;
import com.ruoyi.mes.domain.Vo.CraftProVo;

/**
 * 工艺管理Service接口
 * 
 * @author g4
 * @date 2023-04-18
 */
public interface ICraftService 
{
    /**
     * 查询工艺管理
     * 
     * @param id 工艺管理主键
     * @return 工艺管理
     */
    public Craft selectCraftById(Long id);

    /**
     * 查询工艺管理列表
     * 
     * @param craft 工艺管理
     * @return 工艺管理集合
     */
    public List<Craft> selectCraftList(Craft craft);

    /**
     * 新增工艺管理
     *
     * @param craft 工艺管理
     * @return 结果
     */
    public AjaxResult insertCraft(Craft craft);

    /**
     * 修改工艺管理
     * 
     * @param craft 工艺管理
     * @return 结果
     */
    public int updateCraft(Craft craft);

    /**
     * 批量删除工艺管理
     * 
     * @param ids 需要删除的工艺管理主键集合
     * @return 结果
     */
    public int deleteCraftByIds(Long[] ids);

    /**
     * 删除工艺管理信息
     * 
     * @param id 工艺管理主键
     * @return 结果
     */
    public int deleteCraftById(Long id);

    String checkCraftCodeUnique(Craft craft);

    String checkCraftNameUnique(Craft craft);

    /**
     * 增- 工艺的工序
     * @param craftProVo
     * @return
     */
    AjaxResult insertCraftProcess(CraftProVo craftProVo);

    public AjaxResult updateCraft(CraftProVo craftProVo);


}
