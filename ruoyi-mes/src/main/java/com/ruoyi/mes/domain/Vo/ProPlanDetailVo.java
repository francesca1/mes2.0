package com.ruoyi.mes.domain.Vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.mes.domain.ProductInfo;
import com.ruoyi.mes.domain.ProductPlan;
import com.ruoyi.mes.domain.ProductPlanDetial;
import lombok.Data;

import java.util.Date;

@Data
public class ProPlanDetailVo {
    /**
     * 生产信息
     */
    private ProductInfo productInfo;
    /**
     * 生产计划信息
     */
    private ProductPlan productPlan;
}
