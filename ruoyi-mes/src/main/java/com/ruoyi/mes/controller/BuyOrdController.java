package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.mes.domain.MatPlan;
import com.ruoyi.mes.domain.Vo.BuyNeedOrdVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.BuyOrd;
import com.ruoyi.mes.service.IBuyOrdService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 采购订单Controller
 * 
 * @author g4
 * @date 2023-05-08
 */
@RestController
@RequestMapping("/mes/buyord")
public class BuyOrdController extends BaseController
{
    @Autowired
    private IBuyOrdService buyOrdService;

    /**
     * 查询采购订单列表
     */
    @PreAuthorize("@ss.hasPermi('mes:buyord:list')")
    @GetMapping("/list")
    public TableDataInfo list(BuyOrd buyOrd)
    {
        startPage();
        List<BuyOrd> list = buyOrdService.selectBuyOrdList(buyOrd);
        return getDataTable(list);
    }

    /**
     * 导出采购订单列表
     */
    @PreAuthorize("@ss.hasPermi('mes:buyord:export')")
    @Log(title = "采购订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BuyOrd buyOrd)
    {
        List<BuyOrd> list = buyOrdService.selectBuyOrdList(buyOrd);
        ExcelUtil<BuyOrd> util = new ExcelUtil<BuyOrd>(BuyOrd.class);
        util.exportExcel(response, list, "采购订单数据");
    }

    /**
     * 获取采购订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:buyord:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(buyOrdService.selectBuyOrdById(id));
    }

    /**
     * 新增采购订单
     */
    @PreAuthorize("@ss.hasPermi('mes:buyord:add')")
    @Log(title = "采购订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BuyOrd buyOrd)
    {
        return toAjax(buyOrdService.insertBuyOrd(buyOrd));
    }

    /**
     * 修改采购订单
     */
    @PreAuthorize("@ss.hasPermi('mes:buyord:edit')")
    @Log(title = "采购订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BuyOrd buyOrd)
    {
        return toAjax(buyOrdService.updateBuyOrd(buyOrd));
    }

    /**
     * 删除采购订单
     */
    @PreAuthorize("@ss.hasPermi('mes:buyord:remove')")
    @Log(title = "采购订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(buyOrdService.deleteBuyOrdByIds(ids));
    }

    /**
     * 新增采购订单-物料
     */
    @PostMapping("/addBuyOrd")
    public AjaxResult addBuyOrd(@RequestBody BuyNeedOrdVo buyNeedOrdVo)
    {
        return buyOrdService.insertAddBuyOrd(buyNeedOrdVo);
    }

    /**
     * 新增采购订单-all
     */
    @PostMapping("/addBuyOrdAll")
    public AjaxResult addBuyOrdAll(@RequestBody BuyNeedOrdVo buyNeedOrdVo)
    {
        return buyOrdService.insertAddBuyOrdAll(buyNeedOrdVo);
    }

    /**
     * 获取采购订单详细信息
     */
    @GetMapping(value = "/getOrdInfo/{buyordCode}")
    public AjaxResult getOrdInfo(@PathVariable("buyordCode") String buyordCode)
    {
        return buyOrdService.selectBuyOrdByCode(buyordCode);
    }

    /**
     * 确认到货
     */
    @PostMapping("/confirmArrival")
    public AjaxResult confirmArrival(@RequestBody BuyOrd buyOrd)
    {
        return buyOrdService.insertconfirmArrival(buyOrd);
    }


}
