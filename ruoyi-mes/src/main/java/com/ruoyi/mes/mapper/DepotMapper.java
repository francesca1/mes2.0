package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.Depot;

/**
 * 库房管理Mapper接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface DepotMapper 
{
    /**
     * 查询库房管理
     * 
     * @param id 库房管理主键
     * @return 库房管理
     */
    public Depot selectDepotById(Long id);

    /**
     * 查询库房管理列表
     * 
     * @param depot 库房管理
     * @return 库房管理集合
     */
    public List<Depot> selectDepotList(Depot depot);

    /**
     * 新增库房管理
     * 
     * @param depot 库房管理
     * @return 结果
     */
    public int insertDepot(Depot depot);

    /**
     * 修改库房管理
     * 
     * @param depot 库房管理
     * @return 结果
     */
    public int updateDepot(Depot depot);

    /**
     * 删除库房管理
     * 
     * @param id 库房管理主键
     * @return 结果
     */
    public int deleteDepotById(Long id);

    /**
     * 批量删除库房管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDepotByIds(Long[] ids);

    Depot selectDepotByName(String depotName);

}
