package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.MatPlan;
import com.ruoyi.mes.domain.Vo.MatneedVo;

/**
 * 物料计划Service接口
 * 
 * @author g4
 * @date 2023-05-08
 */
public interface IMatPlanService 
{
    /**
     * 查询物料计划
     * 
     * @param id 物料计划主键
     * @return 物料计划
     */
    public MatPlan selectMatPlanById(Long id);

    /**
     * 查询物料计划列表
     * 
     * @param matPlan 物料计划
     * @return 物料计划集合
     */
    public List<MatPlan> selectMatPlanList(MatPlan matPlan);

    /**
     * 新增物料计划
     * 
     * @param matPlan 物料计划
     * @return 结果
     */
    public int insertMatPlan(MatPlan matPlan);

    /**
     * 修改物料计划
     * 
     * @param matPlan 物料计划
     * @return 结果
     */
    public int updateMatPlan(MatPlan matPlan);

    /**
     * 批量删除物料计划
     * 
     * @param ids 需要删除的物料计划主键集合
     * @return 结果
     */
    public int deleteMatPlanByIds(Long[] ids);

    /**
     * 删除物料计划信息
     * 
     * @param id 物料计划主键
     * @return 结果
     */
    public int deleteMatPlanById(Long id);

    /**
     * 获取物料计划详细信息
     */
    AjaxResult selectMatPlanByProCode(String matplanCode);

    /**
     * 下达物料计划-生成采购需求单
     */
    AjaxResult insertcreatMatNeed(MatneedVo matneedVo);

}
