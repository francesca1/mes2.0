package com.ruoyi.mes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.OutboundPickMapper;
import com.ruoyi.mes.domain.OutboundPick;
import com.ruoyi.mes.service.IOutboundPickService;

/**
 * 领料出库Service业务层处理
 * 
 * @author g4
 * @date 2023-04-11
 */
@Service
public class OutboundPickServiceImpl implements IOutboundPickService 
{
    @Autowired
    private OutboundPickMapper outboundPickMapper;

    /**
     * 查询领料出库
     * 
     * @param id 领料出库主键
     * @return 领料出库
     */
    @Override
    public OutboundPick selectOutboundPickById(Long id)
    {
        return outboundPickMapper.selectOutboundPickById(id);
    }

    /**
     * 查询领料出库列表
     * 
     * @param outboundPick 领料出库
     * @return 领料出库
     */
    @Override
    public List<OutboundPick> selectOutboundPickList(OutboundPick outboundPick)
    {
        return outboundPickMapper.selectOutboundPickList(outboundPick);
    }

    /**
     * 新增领料出库
     * 
     * @param outboundPick 领料出库
     * @return 结果
     */
    @Override
    public int insertOutboundPick(OutboundPick outboundPick)
    {
        return outboundPickMapper.insertOutboundPick(outboundPick);
    }

    /**
     * 修改领料出库
     * 
     * @param outboundPick 领料出库
     * @return 结果
     */
    @Override
    public int updateOutboundPick(OutboundPick outboundPick)
    {
        return outboundPickMapper.updateOutboundPick(outboundPick);
    }

    /**
     * 批量删除领料出库
     * 
     * @param ids 需要删除的领料出库主键
     * @return 结果
     */
    @Override
    public int deleteOutboundPickByIds(Long[] ids)
    {
        return outboundPickMapper.deleteOutboundPickByIds(ids);
    }

    /**
     * 删除领料出库信息
     * 
     * @param id 领料出库主键
     * @return 结果
     */
    @Override
    public int deleteOutboundPickById(Long id)
    {
        return outboundPickMapper.deleteOutboundPickById(id);
    }
}
