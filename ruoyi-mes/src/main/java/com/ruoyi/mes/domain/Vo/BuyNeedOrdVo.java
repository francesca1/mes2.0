package com.ruoyi.mes.domain.Vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.mes.domain.MatPlanDetail;
import com.ruoyi.mes.domain.Material;
import lombok.Data;


import java.util.Date;
import java.util.List;

@Data
public class BuyNeedOrdVo {
    /** 采购需求单编码 */
    @Excel(name = "采购需求单编码")
    private String buyneedCode;

    /** 物料计划编码 */
    @Excel(name = "物料计划编码")
    private String matplanCode;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    /** 采购订单名称 */
    @Excel(name = "采购订单名称")
    private String name;

    /** 采购物料数量 */
    @Excel(name = "采购物料数量")
    private Long buyNum;

    /** 物料详情 */
    @Excel(name = "物料详情")
    private List<Material> materials;

    /** 到货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到货日期")
    private Date alreadyDate;


}
