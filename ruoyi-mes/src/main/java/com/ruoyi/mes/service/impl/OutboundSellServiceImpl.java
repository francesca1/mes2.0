package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.*;
import com.ruoyi.mes.domain.Vo.SellOutVo;
import com.ruoyi.mes.mapper.OrderTableMapper;
import com.ruoyi.mes.mapper.OrdproMapper;
import com.ruoyi.mes.mapper.ProductInfoMapper;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.OutboundSellMapper;
import com.ruoyi.mes.service.IOutboundSellService;

/**
 * 销售出库Service业务层处理
 * 
 * @author g4
 * @date 2023-04-11
 */
@Service
public class OutboundSellServiceImpl implements IOutboundSellService 
{
    @Autowired
    private OutboundSellMapper outboundSellMapper;


    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private OrdproMapper ordproMapper;
    /**
     * 查询销售出库
     * 
     * @param id 销售出库主键
     * @return 销售出库
     */
    @Override
    public OutboundSell selectOutboundSellById(Long id)
    {
        return outboundSellMapper.selectOutboundSellById(id);
    }

    /**
     * 查询销售出库列表
     * 
     * @param outboundSell 销售出库
     * @return 销售出库
     */
    @Override
    public List<OutboundSell> selectOutboundSellList(OutboundSell outboundSell)
    {
        return outboundSellMapper.selectOutboundSellList(outboundSell);
    }

    /**
     * 新增销售出库
     * 
     * @param outboundSell 销售出库
     * @return 结果
     */
    @Override
    public int insertOutboundSell(OutboundSell outboundSell)
    {
        return outboundSellMapper.insertOutboundSell(outboundSell);
    }

    /**
     * 修改销售出库
     * 
     * @param outboundSell 销售出库
     * @return 结果
     */
    @Override
    public int updateOutboundSell(OutboundSell outboundSell)
    {
        return outboundSellMapper.updateOutboundSell(outboundSell);
    }

    /**
     * 批量删除销售出库
     * 
     * @param ids 需要删除的销售出库主键
     * @return 结果
     */
    @Override
    public int deleteOutboundSellByIds(Long[] ids)
    {
        return outboundSellMapper.deleteOutboundSellByIds(ids);
    }

    /**
     * 删除销售出库信息
     * 
     * @param id 销售出库主键
     * @return 结果
     */
    @Override
    public int deleteOutboundSellById(Long id)
    {
        return outboundSellMapper.deleteOutboundSellById(id);
    }

    /**
     * 销售出货单
     * @param sellOutVo
     * @return
     */
    @Override
    public AjaxResult selloutByVo(SellOutVo sellOutVo) {
        ProductInfo productInfo = sellOutVo.getProductInfo();
        ProductPlan productPlan = sellOutVo.getProductPlan();
        if(productInfo.getInvenNum()>= productPlan.getNeedNum()) {
            productInfo.setInvenNum(productInfo.getInvenNum() - productPlan.getNeedNum());
            if (productInfo.getInvenNum() < 0) {
                productInfo.setInvenNum(0L);
            }
            OutboundSell outboundSell = outboundSellMapper.selectOutboundSellByCode(sellOutVo.getProductPlan().getProplanCode());
            outboundSell.setOutboundStatus("已出库");
            outboundSellMapper.updateOutboundSell(outboundSell);
            productInfoMapper.updateProductInfo(productInfo);
            return AjaxResult.success("出货成功");
        }else{
            return AjaxResult.error("未达出货要求");
        }

    }

    /**
     * 查看出货单
     */
    @Override
    public AjaxResult lookselloutByCode(String ordCode) {
        Ordpro ordpro = ordproMapper.selectOrdproByOrdCode(ordCode);
        OutboundSell outboundSell = outboundSellMapper.selectOutboundSellByCode(ordCode);
        outboundSell.setProductName(ordpro.getProductName());

        outboundSell.setOutboundStatus("已出库");
        outboundSellMapper.updateOutboundSell(outboundSell);
        return AjaxResult.success(outboundSell);
    }
}
