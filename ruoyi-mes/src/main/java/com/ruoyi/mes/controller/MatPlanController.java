package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.mes.domain.Vo.MatneedVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.MatPlan;
import com.ruoyi.mes.service.IMatPlanService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物料计划Controller
 * 
 * @author g4
 * @date 2023-05-08
 */
@RestController
@RequestMapping("/mes/MatPlan")
public class MatPlanController extends BaseController
{
    @Autowired
    private IMatPlanService matPlanService;

    /**
     * 查询物料计划列表
     */
    @PreAuthorize("@ss.hasPermi('mes:MatPlan:list')")
    @GetMapping("/list")
    public TableDataInfo list(MatPlan matPlan)
    {
        startPage();
        List<MatPlan> list = matPlanService.selectMatPlanList(matPlan);
        return getDataTable(list);
    }

    /**
     * 导出物料计划列表
     */
    @PreAuthorize("@ss.hasPermi('mes:MatPlan:export')")
    @Log(title = "物料计划", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MatPlan matPlan)
    {
        List<MatPlan> list = matPlanService.selectMatPlanList(matPlan);
        ExcelUtil<MatPlan> util = new ExcelUtil<MatPlan>(MatPlan.class);
        util.exportExcel(response, list, "物料计划数据");
    }

    /**
     * 获取物料计划详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:MatPlan:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(matPlanService.selectMatPlanById(id));
    }

    /**
     * 新增物料计划
     */
    @PreAuthorize("@ss.hasPermi('mes:MatPlan:add')")
    @Log(title = "物料计划", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MatPlan matPlan)
    {
        return toAjax(matPlanService.insertMatPlan(matPlan));
    }

    /**
     * 修改物料计划
     */
    @PreAuthorize("@ss.hasPermi('mes:MatPlan:edit')")
    @Log(title = "物料计划", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MatPlan matPlan)
    {
        return toAjax(matPlanService.updateMatPlan(matPlan));
    }

    /**
     * 删除物料计划
     */
    @PreAuthorize("@ss.hasPermi('mes:MatPlan:remove')")
    @Log(title = "物料计划", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(matPlanService.deleteMatPlanByIds(ids));
    }

    /**
     * 获取物料计划详细信息
     */
    @GetMapping("/getInfo")
    public AjaxResult getInfo(@RequestParam String matplanCode)
    {
        return matPlanService.selectMatPlanByProCode(matplanCode);
    }

    /**
     * 下达物料计划-生成采购需求单
     */
    @PostMapping("/createMatNeed")
    public AjaxResult creatMatNeed(@RequestBody MatneedVo matneedVo)
    {
        return matPlanService.insertcreatMatNeed(matneedVo);
    }
}
