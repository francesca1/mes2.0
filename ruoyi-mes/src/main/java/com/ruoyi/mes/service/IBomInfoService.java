package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.BomInfo;
import com.ruoyi.mes.domain.Vo.BomMatVo;

/**
 * BOM管理Service接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface IBomInfoService 
{
    /**
     * 查询BOM管理
     * 
     * @param id BOM管理主键
     * @return BOM管理
     */
    public BomInfo selectBomInfoById(Long id);

    /**
     * 查询BOM管理列表
     * 
     * @param bomInfo BOM管理
     * @return BOM管理集合
     */
    public List<BomInfo> selectBomInfoList(BomInfo bomInfo);

    /**
     * 新增BOM管理
     *
     * @param bomInfo BOM管理
     * @return 结果
     */
    public AjaxResult insertBomInfo(BomInfo bomInfo);

    /**
     * 修改BOM管理
     * 
     * @param bomInfo BOM管理
     * @return 结果
     */
    public int updateBomInfo(BomInfo bomInfo);

    /**
     * 批量删除BOM管理
     * 
     * @param ids 需要删除的BOM管理主键集合
     * @return 结果
     */
    public int deleteBomInfoByIds(Long[] ids);

    /**
     * 删除BOM管理信息
     * 
     * @param id BOM管理主键
     * @return 结果
     */
    public int deleteBomInfoById(Long id);


    /**
     * 增 - bom物料信息
     * @param bomMatVo
     * @return
     */
    AjaxResult insertBomMatInfo(BomMatVo bomMatVo);




    /**
     * 查 - bom物料信息 - bomcode
     * @param bomCode
     * @return
     */
    AjaxResult selectBomMatInfoByCode(String bomCode);


    /**
     * 修改BOM管理
     */
    int editbomBomInfo(BomMatVo bomMatVo);

    /**
     * 删除BOM
     */
    int deleteBomInfoByCode(String bomCode);

}
