package com.ruoyi.mes.domain.Vo;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class BinDepotVo {
    /** 采购订单编号 */
    @Excel(name = "采购订单编号")
    private String buyordCode;

    /** 库位编号 */
    @Excel(name = "库位编号")
    private String binLocation;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String depotName;

    /** 负责人 */
    @Excel(name = "负责人")
    private String headman;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

}
