package com.ruoyi.mes.domain.Vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.mes.domain.BomMaterial;
import com.ruoyi.mes.domain.Material;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class BomMatVo {


    /** bom编码 */
    private String bomCode;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 产品编码
     */
    private String productCode;

    /**
     * bom物料清单
     */
    private List<Material> materials;

}
