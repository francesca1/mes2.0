package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.Craft;
import com.ruoyi.mes.domain.Vo.CraftProVo;

/**
 * 工艺管理Mapper接口
 * 
 * @author g4
 * @date 2023-04-18
 */
public interface CraftMapper 
{
    /**
     * 查询工艺管理
     * 
     * @param id 工艺管理主键
     * @return 工艺管理
     */
    public Craft selectCraftById(Long id);

    /**
     * 查询工艺管理列表
     * 
     * @param craft 工艺管理
     * @return 工艺管理集合
     */
    public List<Craft> selectCraftList(Craft craft);

    /**
     * 新增工艺管理
     * 
     * @param craft 工艺管理
     * @return 结果
     */
    public int insertCraft(Craft craft);

    /**
     * 修改工艺管理
     * 
     * @param craft 工艺管理
     * @return 结果
     */
    public int updateCraft(Craft craft);

    /**
     * 删除工艺管理
     * 
     * @param id 工艺管理主键
     * @return 结果
     */
    public int deleteCraftById(Long id);

    /**
     * 批量删除工艺管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCraftByIds(Long[] ids);

    /**
     * 判重工艺编码
     * @param craft
     * @return
     */
    Craft checkCraftCodeUnique(Craft craft);

    /**
     * 判重工艺名称
     * @param craft
     * @return
     */
    Craft checkCraftNameUnique(Craft craft);



    List<Craft> selectCraftByCodeList(CraftProVo craftProVo1);


    Craft selectCraftByCode(String craftCode);

}
