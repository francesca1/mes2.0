package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.mes.domain.*;
import com.ruoyi.mes.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.service.ITaskcardService;

/**
 * 车间任务卡Service业务层处理
 * 
 * @author g4
 * @date 2023-04-11
 */
@Service
public class TaskcardServiceImpl implements ITaskcardService 
{
    @Autowired
    private TaskcardMapper taskcardMapper;

    @Autowired
    private WorkTaskMapper workTaskMapper;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private CraftMapper craftMapper;

    @Autowired
    private ProcessMaterialMapper processMaterialMapper;

    @Autowired
    private InboundWorkMapper inboundWorkMapper;

    /**
     * 查询车间任务卡
     * 
     * @param id 车间任务卡主键
     * @return 车间任务卡
     */
    @Override
    public Taskcard selectTaskcardById(Long id)
    {
        return taskcardMapper.selectTaskcardById(id);
    }

    /**
     * 查询车间任务卡列表
     * 
     * @param taskcard 车间任务卡
     * @return 车间任务卡
     */
    @Override
    public List<Taskcard> selectTaskcardList(Taskcard taskcard)
    {
        List<Taskcard> taskcards = taskcardMapper.selectTaskcardList(taskcard);
        if(taskcards.size()!=0){
            for (Taskcard taskcard1 : taskcards) {
                if(taskcard1.getPlanNum()==taskcard1.getAlreadyNum()){
                    taskcard1.setStatus("已完成");
                    taskcardMapper.updateTaskcard(taskcard1);
                }
                taskcard1.setTaskSchedule((float)taskcard1.getAlreadyNum()/ taskcard1.getPlanNum()*100);
                taskcardMapper.updateTaskcard(taskcard1);
            }
        }
        return taskcards;
    }

    /**
     * 新增车间任务卡
     * 
     * @param taskcard 车间任务卡
     * @return 结果
     */
    @Override
    public int insertTaskcard(Taskcard taskcard)
    {
        return taskcardMapper.insertTaskcard(taskcard);
    }

    /**
     * 修改车间任务卡
     * 
     * @param taskcard 车间任务卡
     * @return 结果
     */
    @Override
    public int updateTaskcard(Taskcard taskcard)
    {
        return taskcardMapper.updateTaskcard(taskcard);
    }

    /**
     * 批量删除车间任务卡
     * 
     * @param ids 需要删除的车间任务卡主键
     * @return 结果
     */
    @Override
    public int deleteTaskcardByIds(Long[] ids)
    {
        return taskcardMapper.deleteTaskcardByIds(ids);
    }

    /**
     * 删除车间任务卡信息
     * 
     * @param id 车间任务卡主键
     * @return 结果
     */
    @Override
    public int deleteTaskcardById(Long id)
    {
        return taskcardMapper.deleteTaskcardById(id);
    }
    /**
     * 获取工序
     * @return
     */
    @Override
    public AjaxResult getProcessInfo(Long id) {
        Taskcard taskcard = taskcardMapper.selectTaskcardById(id);
        WorkTask workTask = workTaskMapper.selectWorkTaskByCode(taskcard.getWorkTaskCode());
        ProductInfo productInfo = productInfoMapper.selectProductInfoByName(workTask.getProductName());
        Craft craft = craftMapper.selectCraftById(productInfo.getCraftId());
        List<ProcessMaterial> processMaterials = processMaterialMapper.selectProMatByCode(craft.getCraftCode());
        return AjaxResult.success(processMaterials);
    }

    /**
     * 确认报工
     */
    @Override
    public int insertsubmitReport(Long id) {
        Taskcard taskcard = taskcardMapper.selectTaskcardById(id);
        taskcard.setAlreadyNum(taskcard.getAlreadyNum()+1);
        taskcard.setStatus("报工中");

        if(taskcard.getAlreadyNum()==taskcard.getPlanNum()){
            taskcard.setStatus("已完成");
            WorkTask workTask = workTaskMapper.selectWorkTaskByCode(taskcard.getWorkTaskCode());
            workTask.setAlreadyNum(workTask.getAlreadyNum()+1);
            workTaskMapper.updateWorkTask(workTask);
            taskcardMapper.updateTaskcard(taskcard);


            InboundWork inboundWork = new InboundWork();
            inboundWork.setTaskcardCode(taskcard.getTaskcardCode());
            inboundWork.setInboundNum(1L);
            inboundWork.setInboundType("完工入库");
            inboundWork.setInboundStatus("未入库");
            inboundWork.setInboundDate(DateUtils.getNowDate());
            inboundWorkMapper.insertInboundWork(inboundWork);
        }
        taskcardMapper.updateTaskcard(taskcard);
        return 1;
    }
}
