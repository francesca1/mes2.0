package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.OutboundPick;
import com.ruoyi.mes.service.IOutboundPickService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 领料出库Controller
 * 
 * @author g4
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/pick")
public class OutboundPickController extends BaseController
{
    @Autowired
    private IOutboundPickService outboundPickService;

    /**
     * 查询领料出库列表
     */
    @PreAuthorize("@ss.hasPermi('mes:pick:list')")
    @GetMapping("/list")
    public TableDataInfo list(OutboundPick outboundPick)
    {
        startPage();
        List<OutboundPick> list = outboundPickService.selectOutboundPickList(outboundPick);
        return getDataTable(list);
    }

    /**
     * 导出领料出库列表
     */
    @PreAuthorize("@ss.hasPermi('mes:pick:export')")
    @Log(title = "领料出库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OutboundPick outboundPick)
    {
        List<OutboundPick> list = outboundPickService.selectOutboundPickList(outboundPick);
        ExcelUtil<OutboundPick> util = new ExcelUtil<OutboundPick>(OutboundPick.class);
        util.exportExcel(response, list, "领料出库数据");
    }

    /**
     * 获取领料出库详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:pick:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(outboundPickService.selectOutboundPickById(id));
    }

    /**
     * 新增领料出库
     */
    @PreAuthorize("@ss.hasPermi('mes:pick:add')")
    @Log(title = "领料出库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OutboundPick outboundPick)
    {
        return toAjax(outboundPickService.insertOutboundPick(outboundPick));
    }

    /**
     * 修改领料出库
     */
    @PreAuthorize("@ss.hasPermi('mes:pick:edit')")
    @Log(title = "领料出库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OutboundPick outboundPick)
    {
        return toAjax(outboundPickService.updateOutboundPick(outboundPick));
    }

    /**
     * 删除领料出库
     */
    @PreAuthorize("@ss.hasPermi('mes:pick:remove')")
    @Log(title = "领料出库", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(outboundPickService.deleteOutboundPickByIds(ids));
    }
}
