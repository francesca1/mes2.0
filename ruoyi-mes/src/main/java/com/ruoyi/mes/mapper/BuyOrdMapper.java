package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.BuyOrd;

/**
 * 采购订单Mapper接口
 * 
 * @author g4
 * @date 2023-05-08
 */
public interface BuyOrdMapper 
{
    /**
     * 查询采购订单
     * 
     * @param id 采购订单主键
     * @return 采购订单
     */
    public BuyOrd selectBuyOrdById(Long id);

    /**
     * 查询采购订单列表
     * 
     * @param buyOrd 采购订单
     * @return 采购订单集合
     */
    public List<BuyOrd> selectBuyOrdList(BuyOrd buyOrd);

    /**
     * 新增采购订单
     * 
     * @param buyOrd 采购订单
     * @return 结果
     */
    public int insertBuyOrd(BuyOrd buyOrd);

    /**
     * 修改采购订单
     * 
     * @param buyOrd 采购订单
     * @return 结果
     */
    public int updateBuyOrd(BuyOrd buyOrd);

    /**
     * 删除采购订单
     * 
     * @param id 采购订单主键
     * @return 结果
     */
    public int deleteBuyOrdById(Long id);

    /**
     * 批量删除采购订单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBuyOrdByIds(Long[] ids);

     BuyOrd selectBuyOrdByCode(String buyordCode);

}
