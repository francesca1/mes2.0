package com.ruoyi.mes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.PickingMapper;
import com.ruoyi.mes.domain.Picking;
import com.ruoyi.mes.service.IPickingService;

/**
 * 生产领料Service业务层处理
 * 
 * @author g4
 * @date 2023-05-23
 */
@Service
public class PickingServiceImpl implements IPickingService 
{
    @Autowired
    private PickingMapper pickingMapper;

    /**
     * 查询生产领料
     * 
     * @param id 生产领料主键
     * @return 生产领料
     */
    @Override
    public Picking selectPickingById(Long id)
    {
        return pickingMapper.selectPickingById(id);
    }

    /**
     * 查询生产领料列表
     * 
     * @param picking 生产领料
     * @return 生产领料
     */
    @Override
    public List<Picking> selectPickingList(Picking picking)
    {
        return pickingMapper.selectPickingList(picking);
    }

    /**
     * 新增生产领料
     * 
     * @param picking 生产领料
     * @return 结果
     */
    @Override
    public int insertPicking(Picking picking)
    {
        return pickingMapper.insertPicking(picking);
    }

    /**
     * 修改生产领料
     * 
     * @param picking 生产领料
     * @return 结果
     */
    @Override
    public int updatePicking(Picking picking)
    {
        return pickingMapper.updatePicking(picking);
    }

    /**
     * 批量删除生产领料
     * 
     * @param ids 需要删除的生产领料主键
     * @return 结果
     */
    @Override
    public int deletePickingByIds(Long[] ids)
    {
        return pickingMapper.deletePickingByIds(ids);
    }

    /**
     * 删除生产领料信息
     * 
     * @param id 生产领料主键
     * @return 结果
     */
    @Override
    public int deletePickingById(Long id)
    {
        return pickingMapper.deletePickingById(id);
    }
}
