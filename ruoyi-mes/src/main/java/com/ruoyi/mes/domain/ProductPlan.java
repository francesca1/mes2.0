package com.ruoyi.mes.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 生产计划对象 product_plan
 * 
 * @author g4
 * @date 2023-05-08
 */
public class ProductPlan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 生产计划 */
    private Long id;

    /** 生产计划编码 */
    @Excel(name = "生产计划编码")
    private String proplanCode;

    /** 订单编码 */
    @Excel(name = "订单编码")
    private String ordCode;

    /** 订单名称 */
    @Excel(name = "订单名称")
    private String ordName;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 产品编码 */
    @Excel(name = "产品编码")
    private String productCode;

    /** 需求数量 */
    @Excel(name = "需求数量")
    private Long needNum;

    /** 计划状态 */
    @Excel(name = "计划状态")
    private String planStatus;

    /** 创建人 */
    @Excel(name = "创建人")
    private String userId;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProplanCode(String proplanCode) 
    {
        this.proplanCode = proplanCode;
    }

    public String getProplanCode() 
    {
        return proplanCode;
    }
    public void setOrdCode(String ordCode) 
    {
        this.ordCode = ordCode;
    }

    public String getOrdCode() 
    {
        return ordCode;
    }
    public void setOrdName(String ordName) 
    {
        this.ordName = ordName;
    }

    public String getOrdName() 
    {
        return ordName;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setProductCode(String productCode) 
    {
        this.productCode = productCode;
    }

    public String getProductCode() 
    {
        return productCode;
    }
    public void setNeedNum(Long needNum) 
    {
        this.needNum = needNum;
    }

    public Long getNeedNum() 
    {
        return needNum;
    }
    public void setPlanStatus(String planStatus) 
    {
        this.planStatus = planStatus;
    }

    public String getPlanStatus() 
    {
        return planStatus;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("proplanCode", getProplanCode())
            .append("ordCode", getOrdCode())
            .append("ordName", getOrdName())
            .append("productName", getProductName())
            .append("productCode", getProductCode())
            .append("needNum", getNeedNum())
            .append("planStatus", getPlanStatus())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("note", getNote())
            .toString();
    }
}
