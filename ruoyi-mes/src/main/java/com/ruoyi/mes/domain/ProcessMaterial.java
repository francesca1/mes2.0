package com.ruoyi.mes.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 工序物料对象 process_material
 * 
 * @author g4
 * @date 2023-04-20
 */
@Data
public class ProcessMaterial extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 工序物料id */
    private Long id;

    /** 工序物料编码 */
    @Excel(name = "工艺编码")
    private String craftCode;

    /** 工序名称 */
    @Excel(name = "工序名称")
    private String proccessName;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    /** 所需物料数量 */
    @Excel(name = "所需物料数量")
    private Long proMatNum;

    /** 所需设备*/
    @Excel(name = "所需设备")
    private String equName;

    /** 物料集合 */
    @TableField
    private List<Material> materials;

    @TableField(exist = false)
    private Long needNum;

    public String getEquName() {
        return equName;
    }

    public void setEquName(String equName) {
        this.equName = equName;
    }


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public String getCraftCode() {
        return craftCode;
    }

    public void setCraftCode(String craftCode) {
        this.craftCode = craftCode;
    }

    public void setProccessName(String proccessName)
    {
        this.proccessName = proccessName;
    }

    public String getProccessName() 
    {
        return proccessName;
    }
    public void setMaterialName(String materialName) 
    {
        this.materialName = materialName;
    }

    public String getMaterialName() 
    {
        return materialName;
    }
    public void setProMatNum(Long proMatNum) 
    {
        this.proMatNum = proMatNum;
    }

    public Long getProMatNum() 
    {
        return proMatNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("craftCode", getCraftCode())
            .append("proccessName", getProccessName())
            .append("materialName", getMaterialName())
            .append("proMatNum", getProMatNum())
                .append("equName", getEquName())
            .toString();
    }
}
