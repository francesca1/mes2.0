package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.MaterialMapper;
import com.ruoyi.mes.domain.Material;
import com.ruoyi.mes.service.IMaterialService;

/**
 * 物料信息Service业务层处理
 * 
 * @author g4
 * @date 2023-04-17
 */
@Service
public class MaterialServiceImpl implements IMaterialService 
{
    @Autowired
    private MaterialMapper materialMapper;

    /**
     * 查询物料信息
     * 
     * @param id 物料信息主键
     * @return 物料信息
     */
    @Override
    public Material selectMaterialById(Long id)
    {
        return materialMapper.selectMaterialById(id);
    }

    /**
     * 查询物料信息列表
     * 
     * @param material 物料信息
     * @return 物料信息
     */
    @Override
    public List<Material> selectMaterialList(Material material)
    {
        return materialMapper.selectMaterialList(material);
    }

    /**
     * 新增物料信息
     *
     * @param material 物料信息
     * @return 结果
     */
    @Override
    public AjaxResult insertMaterial(Material material)
    {
        Material material1 = new Material();
        material1.setMaterialName(material.getMaterialName());
        List<Material> materials = materialMapper.selectMaterialList(material1);
        if(materials.size() !=0){
            return AjaxResult.error("物料名称已存在");
        }
        materialMapper.insertMaterial(material);
        return AjaxResult.success("新增成功");
    }

    /**
     * 修改物料信息
     * 
     * @param material 物料信息
     * @return 结果
     */
    @Override
    public int updateMaterial(Material material)
    {
        return materialMapper.updateMaterial(material);
    }

    /**
     * 批量删除物料信息
     * 
     * @param ids 需要删除的物料信息主键
     * @return 结果
     */
    @Override
    public int deleteMaterialByIds(Long[] ids)
    {
        return materialMapper.deleteMaterialByIds(ids);
    }

    /**
     * 删除物料信息信息
     * 
     * @param id 物料信息主键
     * @return 结果
     */
    @Override
    public int deleteMaterialById(Long id)
    {
        return materialMapper.deleteMaterialById(id);
    }


}
