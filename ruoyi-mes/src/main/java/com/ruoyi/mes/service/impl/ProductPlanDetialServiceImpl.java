package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.mes.Util.CodeUtils;
import com.ruoyi.mes.domain.ProductPlan;
import com.ruoyi.mes.domain.Vo.PlanIssueVo;
import com.ruoyi.mes.domain.WorkTask;
import com.ruoyi.mes.mapper.ProductPlanMapper;
import com.ruoyi.mes.mapper.WorkTaskMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.ProductPlanDetialMapper;
import com.ruoyi.mes.domain.ProductPlanDetial;
import com.ruoyi.mes.service.IProductPlanDetialService;

/**
 * 生产计划详情Service业务层处理
 * 
 * @author g4
 * @date 2023-05-15
 */
@Service
public class ProductPlanDetialServiceImpl implements IProductPlanDetialService 
{
    @Autowired
    private ProductPlanDetialMapper productPlanDetialMapper;

    @Autowired
    private WorkTaskMapper workTaskMapper;

    @Autowired
    private ProductPlanMapper productPlanMapper;


    /**
     * 查询生产计划详情
     * 
     * @param id 生产计划详情主键
     * @return 生产计划详情
     */
    @Override
    public ProductPlanDetial selectProductPlanDetialById(Long id)
    {
        return productPlanDetialMapper.selectProductPlanDetialById(id);
    }

    /**
     * 查询生产计划详情列表
     * 
     * @param productPlanDetial 生产计划详情
     * @return 生产计划详情
     */
    @Override
    public List<ProductPlanDetial> selectProductPlanDetialList(ProductPlanDetial productPlanDetial)
    {
        return productPlanDetialMapper.selectProductPlanDetialList(productPlanDetial);
    }

    /**
     * 新增生产计划详情
     * 
     * @param productPlanDetial 生产计划详情
     * @return 结果
     */
    @Override
    public int insertProductPlanDetial(ProductPlanDetial productPlanDetial)
    {
        return productPlanDetialMapper.insertProductPlanDetial(productPlanDetial);
    }

    /**
     * 修改生产计划详情
     * 
     * @param productPlanDetial 生产计划详情
     * @return 结果
     */
    @Override
    public int updateProductPlanDetial(ProductPlanDetial productPlanDetial)
    {
        return productPlanDetialMapper.updateProductPlanDetial(productPlanDetial);
    }

    /**
     * 批量删除生产计划详情
     * 
     * @param ids 需要删除的生产计划详情主键
     * @return 结果
     */
    @Override
    public int deleteProductPlanDetialByIds(Long[] ids)
    {
        return productPlanDetialMapper.deleteProductPlanDetialByIds(ids);
    }

    /**
     * 删除生产计划详情信息
     * 
     * @param id 生产计划详情主键
     * @return 结果
     */
    @Override
    public int deleteProductPlanDetialById(Long id)
    {
        return productPlanDetialMapper.deleteProductPlanDetialById(id);
    }

    /**
     * 计划下达
     *
     * @param planIssueVo
     * @return
     */
    @Override
    public AjaxResult insertplanIssue(PlanIssueVo planIssueVo) {
        ProductPlan productPlan1 = productPlanMapper.selectProductPlanByCode(planIssueVo.getProplanCode());
        productPlan1.setPlanStatus("已生成");
        productPlanMapper.updateProductPlan(productPlan1);

        ProductPlanDetial productPlanDetial = new ProductPlanDetial();
        productPlanDetial.setProplanCode(planIssueVo.getProplanCode());
        productPlanDetial.setIssueStatus("已下达");
        productPlanDetial.setIssueDate(DateUtils.getNowDate());
        productPlanDetial.setIssueNum(planIssueVo.getIssueNum());
        productPlanDetial.setPlanStartTime(planIssueVo.getPlanStartTime());
        productPlanDetial.setPlanOverTime(planIssueVo.getPlanOverTime());
        productPlanDetialMapper.insertProductPlanDetial(productPlanDetial);


        ProductPlan productPlan = productPlanMapper.selectProductPlanByCode(planIssueVo.getProplanCode());
        WorkTask workTask = new WorkTask();
        String workTaskCode = CodeUtils.workTaskCodeCreat();
        workTask.setWorkTaskCode(workTaskCode);
        workTask.setStartTime(DateUtils.getNowDate());
        workTask.setProplanCode(planIssueVo.getProplanCode());
        workTask.setProductName(productPlan.getProductName());
        workTask.setProductCode(productPlan.getProductCode());
        workTask.setTaskStatus("未开始");
        workTask.setAlreadyNum(0L);
        workTask.setProSchedule(0L);
        workTask.setPlanNum(planIssueVo.getIssueNum());
        //workTask.setAlreadyNum();   以生产数量
//        workTask.setStartTime(DateUtils.getNowDate());  开工时间
        workTaskMapper.insertWorkTask(workTask);

        return AjaxResult.success("下达成功");
    }

    /**
     * 获取下达计划详情
     */
    @Override
    public AjaxResult selectgetIssueInfoByCode(String proplanCode) {
        List<ProductPlanDetial> productPlanDetials = productPlanDetialMapper.selectProductPlanDetialByCode(proplanCode);
        return AjaxResult.success(productPlanDetials);
    }
}
