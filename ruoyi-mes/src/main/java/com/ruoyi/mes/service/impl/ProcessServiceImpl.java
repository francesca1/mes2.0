package com.ruoyi.mes.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.mes.domain.*;
import com.ruoyi.mes.domain.Process;
import com.ruoyi.mes.domain.Vo.CraftProVo;
import com.ruoyi.mes.domain.Vo.ProMatVo;
import com.ruoyi.mes.mapper.*;
import com.ruoyi.mes.service.IBomMaterialService;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.service.IProcessService;

/**
 * 工序管理Service业务层处理
 * 
 * @author g4
 * @date 2023-04-11
 */
@Service
public class ProcessServiceImpl implements IProcessService {
    @Autowired
    private CraftMapper craftMapper;

    @Autowired
    private ProcessMapper processMapper;
    @Autowired
    private ProcessMaterialMapper processMaterialMapper;

    @Autowired
    private MaterialMapper materialMapper;

    @Autowired
    private EquipmentMapper equipmentMapper;

    @Autowired
    private ProductInfoMapper productInfoMapper;


    /**
     * 查询工序管理
     *
     * @param id 工序管理主键
     * @return 工序管理
     */
    @Override
    public Process selectProcessById(Long id) {
        return processMapper.selectProcessById(id);
    }

    /**
     * 查询工序管理列表
     *
     * @param process 工序管理
     * @return 工序管理
     */
    @Override
    public List<Process> selectProcessList(Process process) {
        return processMapper.selectProcessList(process);
    }

    /**
     * 检查工序号是否唯一
     * @param process
     * @return
     */
    @Override
    public String checkProcessCodeUnique(Process process) {
        Process process1 = processMapper.checkProcessCodeUnique(process);
        Long processId = process.getId()==null?-1L: process.getId();
        if(StringUtils.isNotNull(process1) && process1.getId().longValue() != processId.longValue()){
            return UserConstants.PRONOT_UNIQUE;
        }
        return UserConstants.PROUNIQUE;
    }

    /**
     * 检查工序名称是否唯一
     * @param process
     * @return
     */
    @Override
    public String checkProcessNameUnique(Process process) {
        Process process1 = processMapper.checkProcessNameUnique(process);
        Long processId = process.getId()==null?-1L: process.getId();
        if(StringUtils.isNotNull(process1) && process1.getId().longValue() != processId.longValue()){
            return UserConstants.PRONOT_UNIQUE;
        }
        return UserConstants.PROUNIQUE;
    }



    /**
     * 新增工序管理
     *
     * @param process 工序管理
     * @return 结果
     */
    @Override
    public AjaxResult insertProcess(Process process) {
        process.setCreateTime(DateUtils.getNowDate());
        processMapper.insertProcess(process);
        //工序与物料绑定

        return AjaxResult.success();
    }

    /**
     * 修改工序管理
     *
     * @param process 工序管理
     * @return 结果
     */
    @Override
    public int updateProcess(Process process) {
        return processMapper.updateProcess(process);
    }

    /**
     * 批量删除工序管理
     *
     * @param ids 需要删除的工序管理主键
     * @return 结果
     */
    @Override
    public int deleteProcessByIds(Long[] ids) {
        return processMapper.deleteProcessByIds(ids);
    }

    /**
     * 删除工序管理信息
     *
     * @param id 工序管理主键
     * @return 结果
     */
    @Override
    public int deleteProcessById(Long id) {
        return processMapper.deleteProcessById(id);
    }

    /**
     * 查看工艺的工序
     * @param craftId
     * @return
     */
    @Override
    public AjaxResult getCraftDetails(Long craftId) {
        CraftProVo craftProVo = new CraftProVo();
        Craft craft = craftMapper.selectCraftById(craftId);
        ProductInfo productInfo = productInfoMapper.selectProductInfoByCraftId(craftId);
        craftProVo.setCraftCode(craft.getCraftCode());
        craftProVo.setCraftName(craft.getCraftName());
        craftProVo.setProductName(productInfo.getProductName());

        List<ProcessMaterial> processMaterials = processMaterialMapper.selectProMatByCode(craft.getCraftCode());
        craftProVo.setProcessMaterials(processMaterials);
        return AjaxResult.success(craftProVo);

    }

    /**
     * 查工序物料
     * @param proMatVo
     * @return
     */
    @Override
    public AjaxResult getMatInfo(ProMatVo proMatVo) {
        List<ProcessMaterial> processMaterials = processMaterialMapper.selectProMatByNameCode(proMatVo.getCraftCode(), proMatVo.getProccessName());
        for (ProcessMaterial processMaterial : processMaterials) {
            List<Material> materials = materialMapper.selectMaterialByMatName(processMaterial.getMaterialName());
            processMaterial.setMaterials(materials);
        }
        return AjaxResult.success(processMaterials);
    }


    //读取工序集合
//        List<Process> processes = processMapper.selectcoedList(craft.getCraftCode());

//        for (Process process : processes) {
//            List<ProcessMaterial> processMaterials = processMaterialMapper.selectProMatByNameCode(process.getCraftCode(), process.getProcessName());
//
            //读取物料集合信息
//            Material material = new Material();
//            for (ProcessMaterial mtr : processMaterials) {
//                BeanUtils.copyProperties(mtr,material);
//            }
//            materials.add(material);
//
//            process.setMaterials(materials);
//        }





}

