package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import io.lettuce.core.dynamic.annotation.Param;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.Taskcard;
import com.ruoyi.mes.service.ITaskcardService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 车间任务卡Controller
 * 
 * @author g4
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/taskcard")
public class TaskcardController extends BaseController
{
    @Autowired
    private ITaskcardService taskcardService;

    /**
     * 查询车间任务卡列表
     */
    @PreAuthorize("@ss.hasPermi('mes:taskcard:list')")
    @GetMapping("/list")
    public TableDataInfo list(Taskcard taskcard)
    {
        startPage();
        List<Taskcard> list = taskcardService.selectTaskcardList(taskcard);
        return getDataTable(list);
    }

    /**
     * 导出车间任务卡列表
     */
    @PreAuthorize("@ss.hasPermi('mes:taskcard:export')")
    @Log(title = "车间任务卡", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Taskcard taskcard)
    {
        List<Taskcard> list = taskcardService.selectTaskcardList(taskcard);
        ExcelUtil<Taskcard> util = new ExcelUtil<Taskcard>(Taskcard.class);
        util.exportExcel(response, list, "车间任务卡数据");
    }

    /**
     * 获取车间任务卡详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:taskcard:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(taskcardService.selectTaskcardById(id));
    }

    /**
     * 新增车间任务卡
     */
    @PreAuthorize("@ss.hasPermi('mes:taskcard:add')")
    @Log(title = "车间任务卡", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Taskcard taskcard)
    {
        return toAjax(taskcardService.insertTaskcard(taskcard));
    }

    /**
     * 修改车间任务卡
     */
    @PreAuthorize("@ss.hasPermi('mes:taskcard:edit')")
    @Log(title = "车间任务卡", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Taskcard taskcard)
    {
        return toAjax(taskcardService.updateTaskcard(taskcard));
    }

    /**
     * 删除车间任务卡
     */
    @PreAuthorize("@ss.hasPermi('mes:taskcard:remove')")
    @Log(title = "车间任务卡", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(taskcardService.deleteTaskcardByIds(ids));
    }

    /**
     * 获取工序
     * @return
     */
    @GetMapping("/getProcessInfo/{id}")
    public AjaxResult getProcessInfo(@PathVariable("id") Long id){
        return taskcardService.getProcessInfo(id);
    }


    /**
     * 确认报工
     */
    @PostMapping("/submitReport/{id}")
    public AjaxResult submitReport(@PathVariable("id") Long id)
    {
        return toAjax(taskcardService.insertsubmitReport(id));
    }
}
