package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.BomInfo;
import com.ruoyi.mes.domain.Vo.BomMatVo;

/**
 * BOM管理Mapper接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface BomInfoMapper 
{
    /**
     * 查询BOM管理
     * 
     * @param id BOM管理主键
     * @return BOM管理
     */
    public BomInfo selectBomInfoById(Long id);

    /**
     * 查询BOM管理列表
     * 
     * @param bomInfo BOM管理
     * @return BOM管理集合
     */
    public List<BomInfo> selectBomInfoList(BomInfo bomInfo);

    /**
     * 新增BOM管理
     * 
     * @param bomInfo BOM管理
     * @return 结果
     */
    public int insertBomInfo(BomInfo bomInfo);

    /**
     * 修改BOM管理
     * 
     * @param bomInfo BOM管理
     * @return 结果
     */
    public int updateBomInfo(BomInfo bomInfo);

    /**
     * 删除BOM管理
     * 
     * @param id BOM管理主键
     * @return 结果
     */
    public int deleteBomInfoById(Long id);

    /**
     * 批量删除BOM管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBomInfoByIds(Long[] ids);

    /**
     * 查询产品bom信息
     * @param bomCode
     * @return
     */
    BomInfo selectBomInfoByCode(String bomCode);


    void deleteBomInfoByCode(String bomCode);

}
