package com.ruoyi.mes.domain.Vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.mes.domain.ProcessMaterial;
import lombok.Data;

import java.util.List;

@Data
public class PickVo {

    /** 车间任务编号 */
    @Excel(name = "车间任务编号")
    private String workTaskCode;

    /** 工艺编码 */
    @Excel(name = "工艺编码")
    private String craftCode;

    /** 物料集合 */
    private List<ProcessMaterial> processMaterials;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    /** 所需数量 */
    @Excel(name = "所需数量")
    private Long needNum;
}
