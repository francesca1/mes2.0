package com.ruoyi.mes.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 工序管理对象 process
 * 
 * @author g4
 * @date 2023-04-11
 */
@Data
public class Process extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 工序编码 */
    private String processCode;

    /** 工艺编码 */
    private String craftCode;

    /** 工序名称 */
    @Excel(name = "工序名称")
    private String processName;

    /** 工序号 */
    @Excel(name = "工序号")
    private Long processNum;

    /** 工位 */
    private String station;

    /** 备注 */
    private String note;

    /** 创建人 */
    @Excel(name = "创建人")
    private String userId;

    /** 物料集合 */
    @TableField(exist = false)
    private List<Material> materials;

    /** 所需物料数量 */
    @TableField(exist = false)
    @Excel(name = "所需物料数量")
    private Long proMatNum;

    /** 设备 */
    @TableField(exist = false)
    @Excel(name = "设备名称")
    private String equName;

    public String getEquName() {
        return equName;
    }

    public void setEquName(String equName) {
        this.equName = equName;
    }

    private List<ProcessMaterial> processMaterials;

    public List<ProcessMaterial> getProcessMaterials() {
        return processMaterials;
    }

    public void setProcessMaterials(List<ProcessMaterial> processMaterials) {
        this.processMaterials = processMaterials;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProcessCode(String processCode) 
    {
        this.processCode = processCode;
    }

    public String getProcessCode()
    {
        return processCode;
    }

    public String getCraftCode() {
        return craftCode;
    }

    public void setCraftCode(String craftCode) {
        this.craftCode = craftCode;
    }

    public void setProcessName(String processName)
    {
        this.processName = processName;
    }

    public String getProcessName() 
    {
        return processName;
    }
    public void setProcessNum(Long processNum) 
    {
        this.processNum = processNum;
    }

    public Long getProcessNum() 
    {
        return processNum;
    }
    public void setStation(String station) 
    {
        this.station = station;
    }

    public String getStation() 
    {
        return station;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("processCode", getProcessCode())
            .append("craftCode", getCraftCode())
            .append("processName", getProcessName())
            .append("processNum", getProcessNum())
            .append("station", getStation())
            .append("note", getNote())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .toString();
    }
}
