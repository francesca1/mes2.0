package com.ruoyi.mes.Util;

import com.ruoyi.common.utils.DateUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 编码自动生成工具类
 */

public class CodeUtils {

    private static LocalDate date = LocalDate.now();

    private static int progCode = 1;//生产计划编号
    private static int mtrPlanCode = 1;//物料计划编号
    private static int buyNeedCode = 1;//采购需求编号
    private static int buyOrderCode = 1;//采购需求编号

    private static int workTaskCode = 1;//车间任务编号


    private static final String PREFIX = "PL-";//生产计划前缀
    private static final String MTRPLAN = "MRP-";//物料计划前缀
    private static final String BUYNEED = "BN-";//采购需求前缀
    private static final String BUYORDER = "BO-";//采购订单前缀

    private static final String WORKTASK ="RW-";//车间任务前缀

    private static final String SEPARATOR = "-";
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyMMdd");

    /**
     * 根据当前时间生成 生产计划编号
     *
     * @return
     */
    public static String ProgCodeCreat() {
        if (date == null) {
            throw new IllegalArgumentException("Date must not be null.");
        }
        StringBuilder builder = new StringBuilder(PREFIX);
        builder.append(date.format(FORMATTER));
        builder.append(SEPARATOR);
        builder.append(progCode++);
        return builder.toString();
    }

    /**
     * 根据当前时间生成 物料计划编号
     *
     * @return
     */
    public static String MtrPlanCodeCreat() {
        if (date == null) {
            throw new IllegalArgumentException("Date must not be null.");
        }
        StringBuilder builder = new StringBuilder(MTRPLAN);
        builder.append(date.format(FORMATTER));
        builder.append(SEPARATOR);
        builder.append(mtrPlanCode++);
        return builder.toString();
    }

    /**
     * 根据当前时间生成 采购需求编号
     *
     * @return
     */
    public static String BuyNeedCodeCreat() {
        if (date == null) {
            throw new IllegalArgumentException("Date must not be null.");
        }
        StringBuilder builder = new StringBuilder(BUYNEED);
        builder.append(date.format(FORMATTER));
        builder.append(SEPARATOR);
        builder.append(buyNeedCode++);
        return builder.toString();
    }

    /**
     * 根据当前时间生成 采购需求编号
     *
     * @return
     */
    public static String BuyOrderCodeCreat() {
        if (date == null) {
            throw new IllegalArgumentException("Date must not be null.");
        }
        StringBuilder builder = new StringBuilder(BUYORDER);
        builder.append(date.format(FORMATTER));
        builder.append(SEPARATOR);
        builder.append(buyOrderCode++);
        return builder.toString();
    }

    /**
     * 根据当前时间生成 车间任务编号
     *
     * @return
     */
    public static String workTaskCodeCreat() {
        if (date == null) {
            throw new IllegalArgumentException("Date must not be null.");
        }
        StringBuilder builder = new StringBuilder(WORKTASK);
        builder.append(date.format(FORMATTER));
        builder.append(SEPARATOR);
        builder.append(workTaskCode++);
        return builder.toString();
    }

}
