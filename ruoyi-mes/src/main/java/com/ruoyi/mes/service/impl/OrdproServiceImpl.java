package com.ruoyi.mes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.OrdproMapper;
import com.ruoyi.mes.domain.Ordpro;
import com.ruoyi.mes.service.IOrdproService;

/**
 * 订单产品表Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-05-05
 */
@Service
public class OrdproServiceImpl implements IOrdproService 
{
    @Autowired
    private OrdproMapper ordproMapper;

    /**
     * 查询订单产品表
     * 
     * @param ordCode 订单产品表主键
     * @return 订单产品表
     */
    @Override
    public Ordpro selectOrdproByOrdCode(String ordCode)
    {
        return ordproMapper.selectOrdproByOrdCode(ordCode);
    }

    /**
     * 查询订单产品表列表
     * 
     * @param ordpro 订单产品表
     * @return 订单产品表
     */
    @Override
    public List<Ordpro> selectOrdproList(Ordpro ordpro)
    {
        return ordproMapper.selectOrdproList(ordpro);
    }

    /**
     * 新增订单产品表
     * 
     * @param ordpro 订单产品表
     * @return 结果
     */
    @Override
    public int insertOrdpro(Ordpro ordpro)
    {
        return ordproMapper.insertOrdpro(ordpro);
    }

    /**
     * 修改订单产品表
     * 
     * @param ordpro 订单产品表
     * @return 结果
     */
    @Override
    public int updateOrdpro(Ordpro ordpro)
    {
        return ordproMapper.updateOrdpro(ordpro);
    }

    /**
     * 批量删除订单产品表
     * 
     * @param ordCodes 需要删除的订单产品表主键
     * @return 结果
     */
    @Override
    public int deleteOrdproByOrdCodes(String[] ordCodes)
    {
        return ordproMapper.deleteOrdproByOrdCodes(ordCodes);
    }

    /**
     * 删除订单产品表信息
     * 
     * @param ordCode 订单产品表主键
     * @return 结果
     */
    @Override
    public int deleteOrdproByOrdCode(String ordCode)
    {
        return ordproMapper.deleteOrdproByOrdCode(ordCode);
    }
}
