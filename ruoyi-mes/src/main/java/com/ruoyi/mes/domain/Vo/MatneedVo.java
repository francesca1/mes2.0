package com.ruoyi.mes.domain.Vo;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class MatneedVo {
    /** 物料计划编码 */
    @Excel(name = "物料计划编码")
    private String matplanCode;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String ordCode;
}
