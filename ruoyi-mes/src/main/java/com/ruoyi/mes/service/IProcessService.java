package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.Process;
import com.ruoyi.mes.domain.Vo.ProMatVo;

/**
 * 工序管理Service接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface IProcessService 
{
    /**
     * 查询工序管理
     * 
     * @param id 工序管理主键
     * @return 工序管理
     */
    public Process selectProcessById(Long id);

    /**
     * 查询工序管理列表
     * 
     * @param process 工序管理
     * @return 工序管理集合
     */
    public List<Process> selectProcessList(Process process);

    public String checkProcessCodeUnique(Process process);
    public String checkProcessNameUnique(Process process);

    /**
     * 新增工序管理
     *
     * @param process 工序管理
     * @return 结果
     */
    public AjaxResult insertProcess(Process process);

    /**
     * 修改工序管理
     * 
     * @param process 工序管理
     * @return 结果
     */
    public int updateProcess(Process process);

    /**
     * 批量删除工序管理
     * 
     * @param ids 需要删除的工序管理主键集合
     * @return 结果
     */
    public int deleteProcessByIds(Long[] ids);

    /**
     * 删除工序管理信息
     * 
     * @param id 工序管理主键
     * @return 结果
     */
    public int deleteProcessById(Long id);
    /**
     * 查看工艺的工序
     * @param craftId
     * @return
     */
    AjaxResult getCraftDetails(Long craftId);

    /**
     * 查工序物料
     * @param proMatVo
     * @return
     */
    AjaxResult getMatInfo(ProMatVo proMatVo);

}
