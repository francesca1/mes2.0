package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.ProductPlanDetial;

/**
 * 生产计划详情Mapper接口
 * 
 * @author g4
 * @date 2023-05-15
 */
public interface ProductPlanDetialMapper 
{
    /**
     * 查询生产计划详情
     * 
     * @param id 生产计划详情主键
     * @return 生产计划详情
     */
    public ProductPlanDetial selectProductPlanDetialById(Long id);

    /**
     * 查询生产计划详情列表
     * 
     * @param productPlanDetial 生产计划详情
     * @return 生产计划详情集合
     */
    public List<ProductPlanDetial> selectProductPlanDetialList(ProductPlanDetial productPlanDetial);

    /**
     * 新增生产计划详情
     * 
     * @param productPlanDetial 生产计划详情
     * @return 结果
     */
    public int insertProductPlanDetial(ProductPlanDetial productPlanDetial);

    /**
     * 修改生产计划详情
     * 
     * @param productPlanDetial 生产计划详情
     * @return 结果
     */
    public int updateProductPlanDetial(ProductPlanDetial productPlanDetial);

    /**
     * 删除生产计划详情
     * 
     * @param id 生产计划详情主键
     * @return 结果
     */
    public int deleteProductPlanDetialById(Long id);

    /**
     * 批量删除生产计划详情
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductPlanDetialByIds(Long[] ids);

    List<ProductPlanDetial> selectProductPlanDetialByCode(String proplanCode);

}
