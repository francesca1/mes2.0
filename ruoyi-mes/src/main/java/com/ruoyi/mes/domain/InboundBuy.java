package com.ruoyi.mes.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 采购入库对象 inbound_buy
 * 
 * @author g4
 * @date 2023-04-11
 */
@Data
public class InboundBuy extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String depotName;

    /** 采购id */
    @Excel(name = "采购订单编码")
    private String buyordCode;

    /** 入库数量 */
    @Excel(name = "入库数量")
    private Long inboundNum;

    /** 入库状态 */
    @Excel(name = "入库状态")
    private String inboundStatus;

    /** 入库类型 */
    @Excel(name = "入库类型")
    private String inboundType;

    /** 入库日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入库日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date inboundDate;

    /** 库位 */
    @Excel(name = "库位")
    private String binLocation;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 物料编号 */
    @TableField(exist = false)
    @Excel(name = "物料编号")
    private String materialCode;

    /** 物料名称 */
    @TableField(exist = false)
    @Excel(name = "物料名称")
    private String materialName;



    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public String getDepotName() {
        return depotName;
    }

    public void setDepotName(String depotName) {
        this.depotName = depotName;
    }

    public String getBuyordCode() {
        return buyordCode;
    }

    public void setBuyordCode(String buyordCode) {
        this.buyordCode = buyordCode;
    }

    public void setInboundNum(Long inboundNum)
    {
        this.inboundNum = inboundNum;
    }

    public Long getInboundNum() 
    {
        return inboundNum;
    }
    public void setInboundStatus(String inboundStatus) 
    {
        this.inboundStatus = inboundStatus;
    }

    public String getInboundStatus() 
    {
        return inboundStatus;
    }
    public void setInboundType(String inboundType) 
    {
        this.inboundType = inboundType;
    }

    public String getInboundType() 
    {
        return inboundType;
    }
    public void setInboundDate(Date inboundDate) 
    {
        this.inboundDate = inboundDate;
    }

    public Date getInboundDate() 
    {
        return inboundDate;
    }

    public String getBinLocation() {
        return binLocation;
    }

    public void setBinLocation(String binLocation) {
        this.binLocation = binLocation;
    }

    public void setNote(String note)
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("depotName", getDepotName())
            .append("buyordCode", getBuyordCode())
            .append("inboundNum", getInboundNum())
            .append("inboundStatus", getInboundStatus())
            .append("inboundType", getInboundType())
            .append("inboundDate", getInboundDate())
            .append("binLocation", getBinLocation())
            .append("note", getNote())
            .toString();
    }
}
