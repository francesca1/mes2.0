package com.ruoyi.mes.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 领料出库对象 outbound_pick
 * 
 * @author g4
 * @date 2023-04-11
 */
public class OutboundPick extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 领料出库编码 */
    @Excel(name = "领料出库编码")
    private String pickoutCode;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Long depotId;

    /** 车间任务id */
    @Excel(name = "车间任务id")
    private Long workTaskId;

    /** 出库状态 */
    @Excel(name = "出库状态")
    private String outboundStatus;

    /** 出库数量 */
    @Excel(name = "出库数量")
    private Long outboundNum;

    /** 出库类型 */
    @Excel(name = "出库类型")
    private Long outboundType;

    /** 库位 */
    @Excel(name = "库位")
    private Long binLocation;

    /** 备注 */
    private String note;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPickoutCode(String pickoutCode) 
    {
        this.pickoutCode = pickoutCode;
    }

    public String getPickoutCode() 
    {
        return pickoutCode;
    }
    public void setDepotId(Long depotId) 
    {
        this.depotId = depotId;
    }

    public Long getDepotId() 
    {
        return depotId;
    }
    public void setWorkTaskId(Long workTaskId) 
    {
        this.workTaskId = workTaskId;
    }

    public Long getWorkTaskId() 
    {
        return workTaskId;
    }
    public void setOutboundStatus(String outboundStatus) 
    {
        this.outboundStatus = outboundStatus;
    }

    public String getOutboundStatus() 
    {
        return outboundStatus;
    }
    public void setOutboundNum(Long outboundNum) 
    {
        this.outboundNum = outboundNum;
    }

    public Long getOutboundNum() 
    {
        return outboundNum;
    }
    public void setOutboundType(Long outboundType) 
    {
        this.outboundType = outboundType;
    }

    public Long getOutboundType() 
    {
        return outboundType;
    }
    public void setBinLocation(Long binLocation) 
    {
        this.binLocation = binLocation;
    }

    public Long getBinLocation() 
    {
        return binLocation;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pickoutCode", getPickoutCode())
            .append("depotId", getDepotId())
            .append("workTaskId", getWorkTaskId())
            .append("outboundStatus", getOutboundStatus())
            .append("outboundNum", getOutboundNum())
            .append("outboundType", getOutboundType())
            .append("binLocation", getBinLocation())
            .append("note", getNote())
            .toString();
    }
}
