package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.BomMaterial;
import com.ruoyi.mes.service.IBomMaterialService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * bom物料Controller
 * 
 * @author g4
 * @date 2023-04-20
 */
@RestController
@RequestMapping("/mes/bommaterial")
public class BomMaterialController extends BaseController
{
    @Autowired
    private IBomMaterialService bomMaterialService;

    /**
     * 查询bom物料列表
     */
    @PreAuthorize("@ss.hasPermi('mes:bommaterial:list')")
    @GetMapping("/list")
    public TableDataInfo list(BomMaterial bomMaterial)
    {
        startPage();
        List<BomMaterial> list = bomMaterialService.selectBomMaterialList(bomMaterial);
        return getDataTable(list);
    }

    /**
     * 导出bom物料列表
     */
    @PreAuthorize("@ss.hasPermi('mes:bommaterial:export')")
    @Log(title = "bom物料", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BomMaterial bomMaterial)
    {
        List<BomMaterial> list = bomMaterialService.selectBomMaterialList(bomMaterial);
        ExcelUtil<BomMaterial> util = new ExcelUtil<BomMaterial>(BomMaterial.class);
        util.exportExcel(response, list, "bom物料数据");
    }

    /**
     * 获取bom物料详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:bommaterial:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bomMaterialService.selectBomMaterialById(id));
    }

    /**
     * 新增bom物料
     */
    @PreAuthorize("@ss.hasPermi('mes:bommaterial:add')")
    @Log(title = "bom物料", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BomMaterial bomMaterial)
    {
        return toAjax(bomMaterialService.insertBomMaterial(bomMaterial));
    }

    /**
     * 修改bom物料
     */
    @PreAuthorize("@ss.hasPermi('mes:bommaterial:edit')")
    @Log(title = "bom物料", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BomMaterial bomMaterial)
    {
        return toAjax(bomMaterialService.updateBomMaterial(bomMaterial));
    }

    /**
     * 删除bom物料
     */
    @PreAuthorize("@ss.hasPermi('mes:bommaterial:remove')")
    @Log(title = "bom物料", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bomMaterialService.deleteBomMaterialByIds(ids));
    }
}
