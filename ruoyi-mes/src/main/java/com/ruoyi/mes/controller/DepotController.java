package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.Depot;
import com.ruoyi.mes.service.IDepotService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 库房管理Controller
 * 
 * @author g4
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/depot")
public class DepotController extends BaseController
{
    @Autowired
    private IDepotService depotService;

    /**
     * 查询库房管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:depot:list')")
    @GetMapping("/list")
    public TableDataInfo list(Depot depot)
    {
        startPage();
        List<Depot> list = depotService.selectDepotList(depot);
        return getDataTable(list);
    }

    /**
     * 导出库房管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:depot:export')")
    @Log(title = "库房管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Depot depot)
    {
        List<Depot> list = depotService.selectDepotList(depot);
        ExcelUtil<Depot> util = new ExcelUtil<Depot>(Depot.class);
        util.exportExcel(response, list, "库房管理数据");
    }

    /**
     * 获取库房管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:depot:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(depotService.selectDepotById(id));
    }

    /**
     * 新增库房管理
     */
    @PreAuthorize("@ss.hasPermi('mes:depot:add')")
    @Log(title = "库房管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Depot depot)
    {
        return toAjax(depotService.insertDepot(depot));
    }

    /**
     * 修改库房管理
     */
    @PreAuthorize("@ss.hasPermi('mes:depot:edit')")
    @Log(title = "库房管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Depot depot)
    {
        return toAjax(depotService.updateDepot(depot));
    }

    /**
     * 删除库房管理
     */
    @PreAuthorize("@ss.hasPermi('mes:depot:remove')")
    @Log(title = "库房管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(depotService.deleteDepotByIds(ids));
    }
}
