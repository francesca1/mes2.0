package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.mes.domain.*;
import com.ruoyi.mes.domain.Process;
import com.ruoyi.mes.domain.Vo.CraftProVo;
import com.ruoyi.mes.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.service.ICraftService;

/**
 * 工艺管理Service业务层处理
 * 
 * @author g4
 * @date 2023-04-18
 */
@Service
public class CraftServiceImpl implements ICraftService 
{

    @Autowired
    private ICraftService craftService;
    @Autowired
    private CraftMapper craftMapper;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private ProcessMapper processMapper;

    @Autowired
    private ProcessMaterialMapper processMaterialMapper;

    @Autowired
    private EquipmentMapper equipmentMapper;

    @Autowired
    private ProcessMaterialServiceImpl processMaterialService;

    /**
     * 查询工艺管理
     * 
     * @param id 工艺管理主键
     * @return 工艺管理
     */
    @Override
    public Craft selectCraftById(Long id)
    {
        return craftMapper.selectCraftById(id);
    }

    /**
     * 查询工艺管理列表
     * 
     * @param craft 工艺管理
     * @return 工艺管理
     */
    @Override
    public List<Craft> selectCraftList(Craft craft)
    {
        return craftMapper.selectCraftList(craft);
    }

    /**
     * 新增工艺管理
     *
     * @param craft 工艺管理
     * @return 结果
     */
    @Override
    public AjaxResult insertCraft(Craft craft)
    {
        craft.setCreateTime(DateUtils.getNowDate());
        craftMapper.insertCraft(craft);
        return AjaxResult.success();
    }

    /**
     * 修改工艺管理
     * 
     * @param craft 工艺管理
     * @return 结果
     */
    @Override
    public int updateCraft(Craft craft)
    {
        return craftService.updateCraft(craft);
    }


    /**
     * 批量删除工艺管理
     * 
     * @param ids 需要删除的工艺管理主键
     * @return 结果
     */
    @Override
    public int deleteCraftByIds(Long[] ids)
    {
        return craftMapper.deleteCraftByIds(ids);
    }

    /**
     * 删除工艺管理信息
     * 
     * @param id 工艺管理主键
     * @return 结果
     */
    @Override
    public int deleteCraftById(Long id)
    {
        ProductInfo productInfo = productInfoMapper.selectProductInfoByCraftId(id);
        productInfo.setCraftId(null);
        productInfoMapper.updateProductInfo(productInfo);
        return craftMapper.deleteCraftById(id);
    }

    /**
     * 判断工艺编码是否唯一
     * @param craft
     * @return
     */
    @Override
    public String checkCraftCodeUnique(Craft craft) {
        Craft craft1 = craftMapper.checkCraftCodeUnique(craft);
        Long craftId = craft.getId()==null?-1L: craft.getId();
        if(StringUtils.isNotNull(craft1) && craft1.getId().longValue() != craftId.longValue()){
            return UserConstants.PRONOT_UNIQUE;
        }
        return UserConstants.PROUNIQUE;
    }

    /**
     * 判断工艺名称是否唯一
     * @param craft
     * @return
     */
    @Override
    public String checkCraftNameUnique(Craft craft) {
        Craft craft1 = craftMapper.checkCraftNameUnique(craft);
        Long craftId = craft.getId()==null?-1L: craft.getId();
        if(StringUtils.isNotNull(craft1) && craft1.getId().longValue() != craftId.longValue()){
            return UserConstants.PRONOT_UNIQUE;
        }
        return UserConstants.PROUNIQUE;
    }

    /**
     * 新增工艺
     * @param craftProVo
     * @return
     */
    @Override
    public AjaxResult insertCraftProcess(CraftProVo craftProVo) {

//        CraftProVo craftProVo1 = new CraftProVo();
//        craftProVo1.setCraftCode(craftProVo.getCraftCode());
//        List<Craft> crafts = craftMapper.selectCraftByCodeList(craftProVo1);
//        if(crafts.size() !=0){
//            return AjaxResult.error("工艺编码已存在");
//        }

        //新增工艺
        Craft craft = new Craft();
        craft.setCraftName(craftProVo.getCraftName());
        craft.setCraftCode(craftProVo.getCraftCode());
        craft.setProductName(craftProVo.getProductName());
        craftService.insertCraft(craft);

        //读取对应工序
        List<Process> processes = craftProVo.getProcesses();
        for (Process process : processes) {
            //读取对应物料
            List<Material> materials = process.getMaterials();
            for (Material material : materials) {
                ProcessMaterial processMaterial = new ProcessMaterial();
                processMaterial.setCraftCode(craftProVo.getCraftCode());
                processMaterial.setProccessName(process.getProcessName());
                processMaterial.setMaterialName(material.getMaterialName());
                processMaterial.setProMatNum(material.getMaterialNum());
                processMaterial.setEquName(process.getEquName());
                processMaterialMapper.insertProcessMaterial(processMaterial);
            }

            //新增工序
//            Process process1 = new Process();
//            process1.setProcessCode(process.getProcessCode());
//            process1.setCraftCode(craftProVo.getCraftCode());
//            process1.setProcessName(process.getProcessName());
//            process1.setStation(process.getStation());
//            processMapper.insertProcess(process1);
        }

        //将工艺id添加到产品表
        Craft craft1 = craftMapper.selectCraftByCode(craftProVo.getCraftCode());
        ProductInfo productInfo = productInfoMapper.selectProductInfoByName(craftProVo.getProductName());
        productInfo.setCraftId(craft1.getId());
        productInfoMapper.updateProductInfo(productInfo);
        return AjaxResult.success("新增成功！");
    }

    /**
     * 修改工艺管理
     *
     * @param craftProVo 工艺管理
     * @return 结果
     */
    @Override
    public AjaxResult updateCraft(CraftProVo craftProVo) {
        deleteCraftById(craftProVo.getCraftId());

        //新增工艺
        Craft craft = new Craft();
        craft.setCraftName(craftProVo.getCraftName());
        craft.setCraftCode(craftProVo.getCraftCode());
        craft.setProductName(craftProVo.getProductName());
        craftService.insertCraft(craft);

        List<Process> processes = craftProVo.getProcesses();
        for (Process process : processes) {
            //读取对应物料
            List<Material> materials = process.getMaterials();
            for (Material material : materials) {
                ProcessMaterial processMaterial = new ProcessMaterial();
                processMaterial.setCraftCode(craftProVo.getCraftCode());
                processMaterial.setProccessName(process.getProcessName());
                processMaterial.setMaterialName(material.getMaterialName());
                processMaterial.setProMatNum(material.getMaterialNum());
                processMaterial.setEquName(process.getEquName());
                processMaterialMapper.insertProcessMaterial(processMaterial);
            }
        }
        Craft craft1 = craftMapper.selectCraftByCode(craftProVo.getCraftCode());
        ProductInfo productInfo = productInfoMapper.selectProductInfoByName(craftProVo.getProductName());
        productInfo.setCraftId(craft1.getId());
        productInfoMapper.updateProductInfo(productInfo);
        return AjaxResult.success("修改成功");
    }

}
