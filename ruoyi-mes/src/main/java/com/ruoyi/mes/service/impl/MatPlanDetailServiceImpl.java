package com.ruoyi.mes.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.MatPlanDetailMapper;
import com.ruoyi.mes.domain.MatPlanDetail;
import com.ruoyi.mes.service.IMatPlanDetailService;

/**
 * 物料计划详细Service业务层处理
 * 
 * @author g4
 * @date 2023-05-08
 */
@Service
public class MatPlanDetailServiceImpl implements IMatPlanDetailService 
{
    @Autowired
    private MatPlanDetailMapper matPlanDetailMapper;

    /**
     * 查询物料计划详细
     * 
     * @param id 物料计划详细主键
     * @return 物料计划详细
     */
    @Override
    public MatPlanDetail selectMatPlanDetailById(Long id)
    {
        return matPlanDetailMapper.selectMatPlanDetailById(id);
    }

    /**
     * 查询物料计划详细列表
     * 
     * @param matPlanDetail 物料计划详细
     * @return 物料计划详细
     */
    @Override
    public List<MatPlanDetail> selectMatPlanDetailList(MatPlanDetail matPlanDetail)
    {
        return matPlanDetailMapper.selectMatPlanDetailList(matPlanDetail);
    }

    /**
     * 新增物料计划详细
     * 
     * @param matPlanDetail 物料计划详细
     * @return 结果
     */
    @Override
    public int insertMatPlanDetail(MatPlanDetail matPlanDetail)
    {
        return matPlanDetailMapper.insertMatPlanDetail(matPlanDetail);
    }

    /**
     * 修改物料计划详细
     * 
     * @param matPlanDetail 物料计划详细
     * @return 结果
     */
    @Override
    public int updateMatPlanDetail(MatPlanDetail matPlanDetail)
    {
        return matPlanDetailMapper.updateMatPlanDetail(matPlanDetail);
    }

    /**
     * 批量删除物料计划详细
     * 
     * @param ids 需要删除的物料计划详细主键
     * @return 结果
     */
    @Override
    public int deleteMatPlanDetailByIds(Long[] ids)
    {
        return matPlanDetailMapper.deleteMatPlanDetailByIds(ids);
    }

    /**
     * 删除物料计划详细信息
     * 
     * @param id 物料计划详细主键
     * @return 结果
     */
    @Override
    public int deleteMatPlanDetailById(Long id)
    {
        return matPlanDetailMapper.deleteMatPlanDetailById(id);
    }
}
