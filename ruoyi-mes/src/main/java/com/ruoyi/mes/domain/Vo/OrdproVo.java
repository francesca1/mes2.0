package com.ruoyi.mes.domain.Vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.mes.domain.ProductInfo;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class OrdproVo {
    /** 订单编号 */
    @Excel(name = "订单编号")
    private String ordCode;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String ordName;

    /** 订单类型 */
    @Excel(name = "订单类型")
    private String ordType;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 签订日期 */
    @Excel(name = "签订日期")
    private Date signDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    /** 交货日期 */
    @Excel(name = "交货日期")
    private Date deadline;

    /** 客户 */
    @Excel(name = "客户")
    private String customName;
    /**
     * 产品列表
     */
    private List<ProductInfo> productInfos;

}
