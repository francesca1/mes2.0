package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.mes.domain.Vo.BomMatVo;
import com.ruoyi.mes.domain.Vo.ProMatVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.BomInfo;
import com.ruoyi.mes.service.IBomInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * BOM管理Controller
 * 
 * @author g4
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/BOM")
public class BomInfoController extends BaseController
{
    @Autowired
    private IBomInfoService bomInfoService;

    /**
     * 查询BOM管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:BOM:list')")
    @GetMapping("/list")
    public TableDataInfo list(BomInfo bomInfo)
    {
        startPage();
        List<BomInfo> list = bomInfoService.selectBomInfoList(bomInfo);
        return getDataTable(list);
    }


    /**
     * 导出BOM管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:BOM:export')")
    @Log(title = "BOM管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BomInfo bomInfo)
    {
        List<BomInfo> list = bomInfoService.selectBomInfoList(bomInfo);
        ExcelUtil<BomInfo> util = new ExcelUtil<BomInfo>(BomInfo.class);
        util.exportExcel(response, list, "BOM管理数据");
    }

    /**
     * 获取BOM管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:BOM:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bomInfoService.selectBomInfoById(id));
    }



    /**
     * 新增BOM管理
     */
    @PreAuthorize("@ss.hasPermi('mes:BOM:add')")
    @Log(title = "BOM管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BomInfo bomInfo)
    {
        return bomInfoService.insertBomInfo(bomInfo);
    }


    /**
     * 修改BOM管理
     */
    @PreAuthorize("@ss.hasPermi('mes:BOM:edit')")
    @Log(title = "BOM管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BomInfo bomInfo)
    {
        return toAjax(bomInfoService.updateBomInfo(bomInfo));
    }

    /**
     * 删除BOM管理
     */
    @PreAuthorize("@ss.hasPermi('mes:BOM:remove')")
    @Log(title = "BOM管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bomInfoService.deleteBomInfoByIds(ids));
    }

    //********************************************

    /**
     * 新增BOM物料管理
     */

    @PreAuthorize("@ss.hasPermi('mes:BOM:add')")
    @PostMapping("/addmat")
    public AjaxResult addBomMat(@RequestBody BomMatVo bomMatVo)
    {
        System.out.println(bomMatVo.getProductName());
        return bomInfoService.insertBomMatInfo(bomMatVo);

    }


    /**
     * 查看bom对应物料
     * @param bomCode
     * @return
     */
    @PreAuthorize("@ss.hasPermi('mes:BOM:list')")
    @GetMapping("getMatInfoList/{bomCode}")
    public AjaxResult getMatInfo(@PathVariable("bomCode") String bomCode)
    {
        return bomInfoService.selectBomMatInfoByCode(bomCode);
    }


    /**
     * 修改BOM管理
     */

    @PutMapping("/editbom")
    public AjaxResult editBomInfo(@RequestBody BomMatVo bomMatVo)
    {
        return toAjax(bomInfoService.editbomBomInfo(bomMatVo));
    }


    /**
     * 删除BOM
     */
    @DeleteMapping("deleteBomInfo/{bomCode}")
    public AjaxResult deleteBomInfo(@PathVariable String bomCode)
    {
        return toAjax(bomInfoService.deleteBomInfoByCode(bomCode));
    }

}
