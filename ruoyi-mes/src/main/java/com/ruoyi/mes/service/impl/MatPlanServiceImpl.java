package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.mes.Util.CodeUtils;
import com.ruoyi.mes.domain.*;
import com.ruoyi.mes.domain.Vo.MatneedVo;
import com.ruoyi.mes.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.service.IMatPlanService;

/**
 * 物料计划Service业务层处理
 *
 * @author g4
 * @date 2023-05-08
 */
@Service
public class MatPlanServiceImpl implements IMatPlanService {
    @Autowired
    private MatPlanMapper matPlanMapper;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private MatPlanDetailMapper matPlanDetailMapper;

    @Autowired
    private BomInfoMapper bomInfoMapper;

    @Autowired
    private BomMaterialMapper bomMaterialMapper;

    @Autowired
    private MaterialMapper materialMapper;

    @Autowired
    private BuyNeedMapper buyNeedMapper;


    /**
     * 查询物料计划
     *
     * @param id 物料计划主键
     * @return 物料计划
     */
    @Override
    public MatPlan selectMatPlanById(Long id) {
        return matPlanMapper.selectMatPlanById(id);
    }

    /**
     * 查询物料计划列表
     *
     * @param matPlan 物料计划
     * @return 物料计划
     */
    @Override
    public List<MatPlan> selectMatPlanList(MatPlan matPlan) {
        return matPlanMapper.selectMatPlanList(matPlan);
    }

    /**
     * 新增物料计划
     *
     * @param matPlan 物料计划
     * @return 结果
     */
    @Override
    public int insertMatPlan(MatPlan matPlan) {
        matPlan.setCreateTime(DateUtils.getNowDate());
        return matPlanMapper.insertMatPlan(matPlan);
    }

    /**
     * 修改物料计划
     *
     * @param matPlan 物料计划
     * @return 结果
     */
    @Override
    public int updateMatPlan(MatPlan matPlan) {
        return matPlanMapper.updateMatPlan(matPlan);
    }

    /**
     * 批量删除物料计划
     *
     * @param ids 需要删除的物料计划主键
     * @return 结果
     */
    @Override
    public int deleteMatPlanByIds(Long[] ids) {
        return matPlanMapper.deleteMatPlanByIds(ids);
    }

    /**
     * 删除物料计划信息
     *
     * @param id 物料计划主键
     * @return 结果
     */
    @Override
    public int deleteMatPlanById(Long id) {
        return matPlanMapper.deleteMatPlanById(id);
    }

    /**
     * 查看物料计划明细
     *
     * @param matplanCode
     * @return
     */
    @Override
    public AjaxResult selectMatPlanByProCode(String matplanCode) {

        List<MatPlanDetail> matPlanDetails = matPlanDetailMapper.selectMatPlanDetailListByCode(matplanCode);
        //设置物料库存数量
        for (MatPlanDetail planDetail : matPlanDetails) {
            Material material1 = materialMapper.selectMaterialByName(planDetail.getMaterialName());
            planDetail.setMaterialNum(material1.getMaterialNum());
        }

        return AjaxResult.success(matPlanDetails);
    }

    /**
     * 下达物料计划-生成采购需求单
     *
     * @param matneedVo
     * @return
     */
    @Override
    public AjaxResult insertcreatMatNeed(MatneedVo matneedVo) {
        String buyNeedCode = CodeUtils.BuyNeedCodeCreat();
        BuyNeed buyNeed = new BuyNeed();
        buyNeed.setBuyneedCode(buyNeedCode);
        buyNeed.setMatplanCode(matneedVo.getMatplanCode());
        buyNeed.setOrdCode(matneedVo.getOrdCode());

        MatPlanDetail matPlanDetail = matPlanDetailMapper.selectMatPlanDetailByCodeName(matneedVo.getMatplanCode(), matneedVo.getMaterialName());

        //判断物料是否充足
        Material material = materialMapper.selectMaterialByName(matPlanDetail.getMaterialName());
        if (matPlanDetail.getNeedNum() < material.getMaterialNum()) {
            return AjaxResult.error("物料充足");
        }

        buyNeed.setMaterialName(material.getMaterialName());
        buyNeed.setStatus("未生成");
        matPlanDetail.setBuyNum(matPlanDetail.getNeedNum() - material.getMaterialNum());
        buyNeedMapper.insertBuyNeed(buyNeed);
        return AjaxResult.success("采购需求单生成成功！");
    }
}
