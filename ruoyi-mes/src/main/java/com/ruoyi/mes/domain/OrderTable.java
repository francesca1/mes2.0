package com.ruoyi.mes.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * 销售订单对象 order_table
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
@Data
public class OrderTable extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String ordCode;

    /** 订单名称 */
    @Excel(name = "订单名称")
    private String ordName;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private String ordStatus;

    /** 订单价格 */
    @Excel(name = "订单价格")
    private Long ordPrice;

    /** 订单类型 */
    @Excel(name = "订单类型")
    private String ordType;

    /** 产品名字 */
    private String productName;

    /** 产品数量 */
    private Long productNum;

    @JsonFormat(pattern = "yyyy-MM-dd")
    /** 签订日期 */
    @Excel(name = "签订日期")
    private Date signDate;


    /** 交货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "交货日期")
    private Date deadline;

    /** 客户 */
    @Excel(name = "客户")
    private String customName;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long salerId;

    /** 备注 */
    private String note;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setOrdCode(String ordCode) 
    {
        this.ordCode = ordCode;
    }

    public String getOrdCode() 
    {
        return ordCode;
    }
    public void setOrdName(String ordName) 
    {
        this.ordName = ordName;
    }

    public String getOrdName() 
    {
        return ordName;
    }
    public void setOrdStatus(String ordStatus) 
    {
        this.ordStatus = ordStatus;
    }

    public String getOrdStatus() 
    {
        return ordStatus;
    }
    public void setOrdPrice(Long ordPrice) 
    {
        this.ordPrice = ordPrice;
    }

    public Long getOrdPrice() 
    {
        return ordPrice;
    }
    public void setOrdType(String ordType) 
    {
        this.ordType = ordType;
    }

    public String getOrdType() 
    {
        return ordType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public void setProductNum(Long productNum)
    {
        this.productNum = productNum;
    }

    public Long getProductNum() {
        return productNum;
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public Long getSalerId() {
        return salerId;
    }

    public void setSalerId(Long salerId) {
        this.salerId = salerId;
    }

    public void setNote(String note)
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ordCode", getOrdCode())
            .append("ordName", getOrdName())
            .append("ordStatus", getOrdStatus())
            .append("ordPrice", getOrdPrice())
            .append("ordType", getOrdType())
            .append("productName", getProductName())
            .append("productNum", getProductNum())
            .append("signDate", getSignDate())
            .append("deadline", getDeadline())
            .append("customName", getCustomName())
            .append("salerId", getSalerId())
            .append("createTime", getCreateTime())
            .append("note", getNote())
            .toString();
    }
}
