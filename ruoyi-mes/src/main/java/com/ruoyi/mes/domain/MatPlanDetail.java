package com.ruoyi.mes.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 物料计划详细对象 mat_plan_detail
 * 
 * @author g4
 * @date 2023-05-08
 */
@Data
public class MatPlanDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 物料计划明细id */
    private Long id;

    /** 物料计划id */
    @Excel(name = "物料计划id")
    private String matplanCode;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    /** 物料编码 */
    @Excel(name = "物料编码")
    private String materialCode;

    /** 所需物料数量 */
    @Excel(name = "所需物料数量")
    private Long needNum;

    @JsonFormat(pattern = "yyyy-MM-dd")
    /** 到货日期 */
    @Excel(name = "到货日期")
    private Date alreadyDate;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 物料库存数量 */
    @TableField(exist = false)
    @Excel(name = "物料库存数量")
    private Long materialNum;

    /** 采购物料数量 */
    @TableField(exist = false)
    @Excel(name = "采购物料数量")
    private Long buyNum;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public String getMatplanCode() {
        return matplanCode;
    }

    public void setMatplanCode(String matplanCode) {
        this.matplanCode = matplanCode;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public Long getNeedNum() {
        return needNum;
    }

    public void setNeedNum(Long needNum) {
        this.needNum = needNum;
    }

    public void setNote(String note)
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }


    public Date getAlreadyDate() {
        return alreadyDate;
    }

    public void setAlreadyDate(Date alreadyDate) {
        this.alreadyDate = alreadyDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("matplanCode", getMatplanCode())
            .append("materialName", getMaterialName())
            .append("materialCode", getMaterialCode())
            .append("needNum", getNeedNum())
            .append("alreadyDate", getAlreadyDate())
            .append("note", getNote())
            .toString();
    }
}
