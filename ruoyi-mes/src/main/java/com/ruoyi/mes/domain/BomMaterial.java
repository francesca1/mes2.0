package com.ruoyi.mes.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * bom物料对象 bom_material
 * 
 * @author g4
 * @date 2023-04-20
 */
@Data
public class BomMaterial extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** bom所需物料id */
    private Long id;

    /** bom编码 */
    @Excel(name = "bom编码")
    private String bomCode;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    /** bom所需物料 */
    @Excel(name = "bom所需物料")
    private String bomNeedMat;

    /** 所需物料数量 */
    @Excel(name = "所需物料数量")
    private Long needMatNum;

    /** 物料库存数量 */
    @TableField(exist = false)
    @Excel(name = "物料库存数量")
    private Long materialNum;

    /** 物料编号 */
    @TableField(exist = false)
    @Excel(name = "物料编号")
    private String materialCode;

    /** 产品所需物料数量 */
    @TableField(exist = false)
    @Excel(name = "产品所需物料数量")
    private Long needNum;



    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBomCode(String bomCode) 
    {
        this.bomCode = bomCode;
    }

    public String getBomCode() 
    {
        return bomCode;
    }
    public void setMaterialName(String materialName) 
    {
        this.materialName = materialName;
    }

    public String getMaterialName() 
    {
        return materialName;
    }
    public void setBomNeedMat(String bomNeedMat) 
    {
        this.bomNeedMat = bomNeedMat;
    }

    public String getBomNeedMat() 
    {
        return bomNeedMat;
    }
    public void setNeedMatNum(Long needMatNum) 
    {
        this.needMatNum = needMatNum;
    }

    public Long getNeedMatNum() 
    {
        return needMatNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("bomCode", getBomCode())
            .append("materialName", getMaterialName())
            .append("bomNeedMat", getBomNeedMat())
            .append("needMatNum", getNeedMatNum())
            .toString();
    }
}
