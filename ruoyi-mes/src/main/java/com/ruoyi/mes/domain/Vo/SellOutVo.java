package com.ruoyi.mes.domain.Vo;

import com.ruoyi.mes.domain.ProductInfo;
import com.ruoyi.mes.domain.ProductPlan;
import lombok.Data;

@Data
public class SellOutVo {
    /**
     * 生产计划信息
     */
    private ProductPlan productPlan;


    /**
     * 产品信息
     */
    private ProductInfo productInfo;


}
