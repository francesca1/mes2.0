package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.OutboundSell;

/**
 * 销售出库Mapper接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface OutboundSellMapper 
{
    /**
     * 查询销售出库
     * 
     * @param id 销售出库主键
     * @return 销售出库
     */
    public OutboundSell selectOutboundSellById(Long id);

    /**
     * 查询销售出库列表
     * 
     * @param outboundSell 销售出库
     * @return 销售出库集合
     */
    public List<OutboundSell> selectOutboundSellList(OutboundSell outboundSell);

    /**
     * 新增销售出库
     * 
     * @param outboundSell 销售出库
     * @return 结果
     */
    public int insertOutboundSell(OutboundSell outboundSell);

    /**
     * 修改销售出库
     * 
     * @param outboundSell 销售出库
     * @return 结果
     */
    public int updateOutboundSell(OutboundSell outboundSell);

    /**
     * 删除销售出库
     * 
     * @param id 销售出库主键
     * @return 结果
     */
    public int deleteOutboundSellById(Long id);

    /**
     * 批量删除销售出库
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOutboundSellByIds(Long[] ids);

    OutboundSell selectOutboundSellByCode(String ordCode);
}
