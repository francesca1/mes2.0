/*
 Navicat Premium Data Transfer

 Source Server         : admin
 Source Server Type    : MySQL
 Source Server Version : 50739
 Source Host           : localhost:3306
 Source Schema         : mes1

 Target Server Type    : MySQL
 Target Server Version : 50739
 File Encoding         : 65001

 Date: 13/06/2023 16:52:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2377 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 2, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2023-04-07 07:41:44', 'admin', '2023-04-07 13:41:38', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 3, 'monitor', NULL, '', 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2023-04-07 07:41:44', 'admin', '2023-04-07 13:41:23', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 4, 'tool', NULL, '', 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2023-04-07 07:41:44', 'admin', '2023-04-07 13:41:27', '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '若依官网', 0, 5, 'http://ruoyi.vip', NULL, '', 0, 0, 'M', '0', '0', '', 'guide', 'admin', '2023-04-07 07:41:44', 'admin', '2023-04-07 13:41:30', '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2023-04-07 07:41:44', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2023-04-07 07:41:44', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2023-04-07 07:41:44', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2023-04-07 07:41:44', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2023-04-07 07:41:44', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2023-04-07 07:41:44', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2023-04-07 07:41:44', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2023-04-07 07:41:44', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', '0', '0', '', 'log', 'admin', '2023-04-07 07:41:44', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2023-04-07 07:41:44', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2023-04-07 07:41:44', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2023-04-07 07:41:44', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2023-04-07 07:41:44', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2023-04-07 07:41:44', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '缓存列表', 2, 6, 'cacheList', 'monitor/cache/list', '', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis-list', 'admin', '2023-04-07 07:41:44', '', NULL, '缓存列表菜单');
INSERT INTO `sys_menu` VALUES (115, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2023-04-07 07:41:44', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (116, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2023-04-07 07:41:44', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (117, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2023-04-07 07:41:44', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2023-04-07 07:41:44', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2023-04-07 07:41:44', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '日志导出', 500, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '账户解锁', 501, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:unlock', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 6, '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 116, 1, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 116, 2, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 116, 3, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 116, 4, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 116, 5, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 116, 6, '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2023-04-07 07:41:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2056, '工人管理查询', 2055, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:worker:query', '#', 'admin', '2022-05-27 15:29:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2057, '工人管理新增', 2055, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:worker:add', '#', 'admin', '2022-05-27 15:29:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2058, '工人管理修改', 2055, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:worker:edit', '#', 'admin', '2022-05-27 15:29:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2059, '工人管理删除', 2055, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:worker:remove', '#', 'admin', '2022-05-27 15:29:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2060, '工人管理导出', 2055, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:worker:export', '#', 'admin', '2022-05-27 15:29:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2061, '车间订单', 2007, 1, 'workshopOrder', 'system/workshopOrder/index', NULL, 1, 0, 'C', '0', '0', 'system:workshopOrder:list', '#', 'admin', '2022-05-27 15:29:47', '', NULL, '车间订单菜单');
INSERT INTO `sys_menu` VALUES (2062, '车间订单查询', 2061, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workshopOrder:query', '#', 'admin', '2022-05-27 15:29:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2063, '车间订单新增', 2061, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workshopOrder:add', '#', 'admin', '2022-05-27 15:29:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2064, '车间订单修改', 2061, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workshopOrder:edit', '#', 'admin', '2022-05-27 15:29:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2065, '车间订单删除', 2061, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workshopOrder:remove', '#', 'admin', '2022-05-27 15:29:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2066, '车间订单导出', 2061, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workshopOrder:export', '#', 'admin', '2022-05-27 15:29:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2067, 'BOM管理', 2010, 6, 'BOMView', 'system/BOMView/index', NULL, 1, 0, 'C', '0', '0', 'system:BOMView:list', '#', 'admin', '2022-05-27 15:49:39', 'admin', '2022-06-13 16:35:31', 'BOM管理菜单');
INSERT INTO `sys_menu` VALUES (2068, 'BOM管理查询', 2067, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:BOMView:query', '#', 'admin', '2022-05-27 15:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2069, 'BOM管理新增', 2067, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:BOMView:add', '#', 'admin', '2022-05-27 15:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2070, 'BOM管理修改', 2067, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:BOMView:edit', '#', 'admin', '2022-05-27 15:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2071, 'BOM管理删除', 2067, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:BOMView:remove', '#', 'admin', '2022-05-27 15:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2072, 'BOM管理导出', 2067, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:BOMView:export', '#', 'admin', '2022-05-27 15:49:39', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2073, '工艺管理', 2010, 999, 'craft', 'system/craft/index', NULL, 1, 0, 'C', '0', '1', 'system:craft:list', '#', 'admin', '2022-05-27 15:49:51', 'admin', '2022-06-13 16:32:52', '工艺管理菜单');
INSERT INTO `sys_menu` VALUES (2074, '工艺管理查询', 2073, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:craft:query', '#', 'admin', '2022-05-27 15:49:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2075, '工艺管理新增', 2073, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:craft:add', '#', 'admin', '2022-05-27 15:49:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2076, '工艺管理修改', 2073, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:craft:edit', '#', 'admin', '2022-05-27 15:49:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2077, '工艺管理删除', 2073, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:craft:remove', '#', 'admin', '2022-05-27 15:49:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2078, '工艺管理导出', 2073, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:craft:export', '#', 'admin', '2022-05-27 15:49:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2079, '工艺工序明细', 2010, 999, 'craftProcessRelation', 'system/craftProcessRelation/index', NULL, 1, 0, 'C', '0', '1', 'system:craftProcessRelation:list', '#', 'admin', '2022-05-27 15:50:08', 'admin', '2022-06-13 16:33:16', '工艺工序明细菜单');
INSERT INTO `sys_menu` VALUES (2080, '工艺工序明细查询', 2079, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:craftProcessRelation:query', '#', 'admin', '2022-05-27 15:50:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2081, '工艺工序明细新增', 2079, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:craftProcessRelation:add', '#', 'admin', '2022-05-27 15:50:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2082, '工艺工序明细修改', 2079, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:craftProcessRelation:edit', '#', 'admin', '2022-05-27 15:50:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2083, '工艺工序明细删除', 2079, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:craftProcessRelation:remove', '#', 'admin', '2022-05-27 15:50:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2084, '工艺工序明细导出', 2079, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:craftProcessRelation:export', '#', 'admin', '2022-05-27 15:50:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2103, '工序管理', 2010, 4, 'process', 'system/process/index', NULL, 1, 0, 'C', '0', '0', 'system:process:list', '#', 'admin', '2022-05-27 15:50:59', 'admin', '2022-06-13 16:35:36', '工序管理菜单');
INSERT INTO `sys_menu` VALUES (2104, '工序管理查询', 2103, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:process:query', '#', 'admin', '2022-05-27 15:50:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2105, '工序管理新增', 2103, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:process:add', '#', 'admin', '2022-05-27 15:50:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2106, '工序管理修改', 2103, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:process:edit', '#', 'admin', '2022-05-27 15:50:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2107, '工序管理删除', 2103, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:process:remove', '#', 'admin', '2022-05-27 15:50:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2108, '工序管理导出', 2103, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:process:export', '#', 'admin', '2022-05-27 15:50:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2109, '流水线工位明细', 2007, 999, 'productiLineStationRelation', 'system/productiLineStationRelation/index', NULL, 1, 0, 'C', '0', '1', 'system:productiLineStationRelation:list', '#', 'admin', '2022-05-27 15:51:10', 'admin', '2022-06-13 16:36:53', '流水线工位明细菜单');
INSERT INTO `sys_menu` VALUES (2110, '流水线工位明细查询', 2109, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productiLineStationRelation:query', '#', 'admin', '2022-05-27 15:51:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2111, '流水线工位明细新增', 2109, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productiLineStationRelation:add', '#', 'admin', '2022-05-27 15:51:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2112, '流水线工位明细修改', 2109, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productiLineStationRelation:edit', '#', 'admin', '2022-05-27 15:51:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2113, '流水线工位明细删除', 2109, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productiLineStationRelation:remove', '#', 'admin', '2022-05-27 15:51:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2114, '流水线工位明细导出', 2109, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productiLineStationRelation:export', '#', 'admin', '2022-05-27 15:51:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2115, '流水线管理', 2007, 999, 'productioLine', 'system/productioLine/index', NULL, 1, 0, 'C', '0', '1', 'system:productioLine:list', '#', 'admin', '2022-05-27 15:51:22', 'admin', '2022-06-13 16:36:46', '流水线管理菜单');
INSERT INTO `sys_menu` VALUES (2116, '流水线管理查询', 2115, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productioLine:query', '#', 'admin', '2022-05-27 15:51:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2117, '流水线管理新增', 2115, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productioLine:add', '#', 'admin', '2022-05-27 15:51:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2118, '流水线管理修改', 2115, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productioLine:edit', '#', 'admin', '2022-05-27 15:51:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2119, '流水线管理删除', 2115, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productioLine:remove', '#', 'admin', '2022-05-27 15:51:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2120, '流水线管理导出', 2115, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productioLine:export', '#', 'admin', '2022-05-27 15:51:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2121, '产品管理', 2010, 1, 'product', 'system/product/index', NULL, 1, 0, 'C', '0', '0', 'system:product:list', '#', 'admin', '2022-05-27 15:51:35', '', NULL, '产品管理菜单');
INSERT INTO `sys_menu` VALUES (2122, '产品管理查询', 2121, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:product:query', '#', 'admin', '2022-05-27 15:51:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2123, '产品管理新增', 2121, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:product:add', '#', 'admin', '2022-05-27 15:51:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2124, '产品管理修改', 2121, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:product:edit', '#', 'admin', '2022-05-27 15:51:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2125, '产品管理删除', 2121, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:product:remove', '#', 'admin', '2022-05-27 15:51:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2126, '产品管理导出', 2121, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:product:export', '#', 'admin', '2022-05-27 15:51:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2127, '订单明细', 2000, 999, 'orderProductRelation', 'system/orderProductRelation/index', NULL, 1, 0, 'C', '0', '1', 'system:orderProductRelation:list', '#', 'admin', '2022-05-27 16:06:28', 'admin', '2022-06-13 16:31:20', '订单明细菜单');
INSERT INTO `sys_menu` VALUES (2128, '订单明细查询', 2127, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:orderProductRelation:query', '#', 'admin', '2022-05-27 16:06:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2129, '订单明细新增', 2127, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:orderProductRelation:add', '#', 'admin', '2022-05-27 16:06:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2130, '订单明细修改', 2127, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:orderProductRelation:edit', '#', 'admin', '2022-05-27 16:06:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2131, '订单明细删除', 2127, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:orderProductRelation:remove', '#', 'admin', '2022-05-27 16:06:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2132, '订单明细导出', 2127, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:orderProductRelation:export', '#', 'admin', '2022-05-27 16:06:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2133, '产品工艺明细', 2010, 999, 'productCraftRelation', 'system/productCraftRelation/index', NULL, 1, 0, 'C', '0', '1', 'system:productCraftRelation:list', '#', 'admin', '2022-05-27 16:06:35', 'admin', '2022-06-13 16:32:45', '产品工艺明细菜单');
INSERT INTO `sys_menu` VALUES (2134, '产品工艺明细查询', 2133, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productCraftRelation:query', '#', 'admin', '2022-05-27 16:06:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2135, '产品工艺明细新增', 2133, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productCraftRelation:add', '#', 'admin', '2022-05-27 16:06:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2136, '产品工艺明细修改', 2133, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productCraftRelation:edit', '#', 'admin', '2022-05-27 16:06:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2137, '产品工艺明细删除', 2133, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productCraftRelation:remove', '#', 'admin', '2022-05-27 16:06:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2138, '产品工艺明细导出', 2133, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productCraftRelation:export', '#', 'admin', '2022-05-27 16:06:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2139, '产品物料明细', 2010, 999, 'productMaterialRelation', 'system/productMaterialRelation/index', NULL, 1, 0, 'C', '0', '1', 'system:productMaterialRelation:list', '#', 'admin', '2022-05-27 16:06:43', 'admin', '2022-06-13 16:35:22', '产品物料明细菜单');
INSERT INTO `sys_menu` VALUES (2140, '产品物料明细查询', 2139, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productMaterialRelation:query', '#', 'admin', '2022-05-27 16:06:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2141, '产品物料明细新增', 2139, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productMaterialRelation:add', '#', 'admin', '2022-05-27 16:06:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2142, '产品物料明细修改', 2139, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productMaterialRelation:edit', '#', 'admin', '2022-05-27 16:06:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2143, '产品物料明细删除', 2139, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productMaterialRelation:remove', '#', 'admin', '2022-05-27 16:06:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2144, '产品物料明细导出', 2139, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:productMaterialRelation:export', '#', 'admin', '2022-05-27 16:06:43', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2145, '工位管理', 2007, 999, 'station', 'system/station/index', NULL, 1, 0, 'C', '0', '1', 'system:station:list', '#', 'admin', '2022-05-27 16:06:51', 'admin', '2022-06-13 16:37:16', '工位管理菜单');
INSERT INTO `sys_menu` VALUES (2146, '工位管理查询', 2145, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:station:query', '#', 'admin', '2022-05-27 16:06:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2147, '工位管理新增', 2145, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:station:add', '#', 'admin', '2022-05-27 16:06:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2148, '工位管理修改', 2145, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:station:edit', '#', 'admin', '2022-05-27 16:06:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2149, '工位管理删除', 2145, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:station:remove', '#', 'admin', '2022-05-27 16:06:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2150, '工位管理导出', 2145, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:station:export', '#', 'admin', '2022-05-27 16:06:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2151, '报工管理', 2007, 999, 'workRecord', 'system/workRecord/index', NULL, 1, 0, 'C', '0', '1', 'system:workRecord:list', '#', 'admin', '2022-05-27 16:06:58', 'admin', '2022-06-13 16:37:39', '报工管理菜单');
INSERT INTO `sys_menu` VALUES (2152, '报工管理查询', 2151, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workRecord:query', '#', 'admin', '2022-05-27 16:06:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2153, '报工管理新增', 2151, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workRecord:add', '#', 'admin', '2022-05-27 16:06:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2154, '报工管理修改', 2151, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workRecord:edit', '#', 'admin', '2022-05-27 16:06:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2155, '报工管理删除', 2151, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workRecord:remove', '#', 'admin', '2022-05-27 16:06:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2156, '报工管理导出', 2151, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workRecord:export', '#', 'admin', '2022-05-27 16:06:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2157, '车间任务卡', 2007, 2, 'workshopTaskCardview', 'system/workshopTaskCardview/index', NULL, 1, 0, 'C', '0', '0', 'system:workshopTaskCardview:list', '#', 'admin', '2022-05-27 16:07:06', 'admin', '2022-06-13 16:36:14', '车间任务卡菜单');
INSERT INTO `sys_menu` VALUES (2158, '车间任务卡查询', 2157, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workshopTaskCardview:query', '#', 'admin', '2022-05-27 16:07:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2159, '车间任务卡新增', 2157, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workshopTaskCardview:add', '#', 'admin', '2022-05-27 16:07:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2160, '车间任务卡修改', 2157, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workshopTaskCardview:edit', '#', 'admin', '2022-05-27 16:07:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2161, '车间任务卡删除', 2157, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workshopTaskCardview:remove', '#', 'admin', '2022-05-27 16:07:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2162, '车间任务卡导出', 2157, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workshopTaskCardview:export', '#', 'admin', '2022-05-27 16:07:06', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2163, '车间任务', 2007, 999, 'workShopView', 'system/workShopView/index', NULL, 1, 0, 'C', '0', '1', 'system:workShopView:list', '#', 'admin', '2022-05-27 16:07:14', 'admin', '2022-06-13 16:36:08', '车间任务菜单');
INSERT INTO `sys_menu` VALUES (2164, '车间任务查询', 2163, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workShopView:query', '#', 'admin', '2022-05-27 16:07:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2165, '车间任务新增', 2163, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workShopView:add', '#', 'admin', '2022-05-27 16:07:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2166, '车间任务修改', 2163, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workShopView:edit', '#', 'admin', '2022-05-27 16:07:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2167, '车间任务删除', 2163, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workShopView:remove', '#', 'admin', '2022-05-27 16:07:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2168, '车间任务导出', 2163, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:workShopView:export', '#', 'admin', '2022-05-27 16:07:14', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2169, '发货管理', 2000, 999, 'deliveryRecord', 'system/deliveryRecord/index', NULL, 1, 0, 'C', '0', '1', 'system:deliveryRecord:list', '#', 'admin', '2022-05-27 16:07:22', 'admin', '2022-06-13 16:31:45', '发货管理菜单');
INSERT INTO `sys_menu` VALUES (2170, '发货管理查询', 2169, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:deliveryRecord:query', '#', 'admin', '2022-05-27 16:07:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2171, '发货管理新增', 2169, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:deliveryRecord:add', '#', 'admin', '2022-05-27 16:07:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2172, '发货管理修改', 2169, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'system:deliveryRecord:edit', '#', 'admin', '2022-05-27 16:07:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2173, '产品管理', 0, 1, 'product', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'build', 'admin', '2023-04-10 21:16:50', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2180, '产品管理', 2173, 1, 'product', 'mes/product/index', NULL, 1, 0, 'C', '0', '0', 'mes:product:list', '#', 'admin', '2023-04-10 22:07:05', '', NULL, '产品管理菜单');
INSERT INTO `sys_menu` VALUES (2181, '产品管理查询', 2180, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:product:query', '#', 'admin', '2023-04-10 22:07:05', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2182, '产品管理新增', 2180, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:product:add', '#', 'admin', '2023-04-10 22:07:05', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2183, '产品管理修改', 2180, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:product:edit', '#', 'admin', '2023-04-10 22:07:05', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2184, '产品管理删除', 2180, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:product:remove', '#', 'admin', '2023-04-10 22:07:05', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2185, '产品管理导出', 2180, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:product:export', '#', 'admin', '2023-04-10 22:07:05', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2186, 'BOM管理', 2173, 1, 'BOM', 'mes/BOM/index', NULL, 1, 0, 'C', '0', '0', 'mes:BOM:list', '#', 'admin', '2023-04-10 22:38:46', '', NULL, 'BOM管理菜单');
INSERT INTO `sys_menu` VALUES (2187, 'BOM管理查询', 2186, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:BOM:query', '#', 'admin', '2023-04-10 22:38:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2188, 'BOM管理新增', 2186, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:BOM:add', '#', 'admin', '2023-04-10 22:38:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2189, 'BOM管理修改', 2186, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:BOM:edit', '#', 'admin', '2023-04-10 22:38:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2190, 'BOM管理删除', 2186, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:BOM:remove', '#', 'admin', '2023-04-10 22:38:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2191, 'BOM管理导出', 2186, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:BOM:export', '#', 'admin', '2023-04-10 22:38:46', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2192, '销售管理', 0, 1, 'order', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'money', 'admin', '2023-04-10 22:47:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2193, '计划管理', 0, 1, 'plan', NULL, NULL, 1, 0, 'M', '0', '0', '', 'edit', 'admin', '2023-04-10 22:52:44', 'admin', '2023-04-10 22:55:59', '');
INSERT INTO `sys_menu` VALUES (2194, '采购管理', 0, 1, 'buy', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'dashboard', 'admin', '2023-04-10 23:01:55', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2195, '生产制造', 0, 1, 'manufacture', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'swagger', 'admin', '2023-04-10 23:08:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2196, '库存管理', 0, 1, 'store', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'druid', 'admin', '2023-04-10 23:22:33', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2197, '采购需求单', 2194, 1, 'needcard', 'mes/needcard/index', NULL, 1, 0, 'C', '0', '0', 'mes:needcard:list', '#', 'admin', '2023-04-10 23:34:59', '', NULL, '采购需求单菜单');
INSERT INTO `sys_menu` VALUES (2198, '采购需求单查询', 2197, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:needcard:query', '#', 'admin', '2023-04-10 23:34:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2199, '采购需求单新增', 2197, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:needcard:add', '#', 'admin', '2023-04-10 23:34:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2200, '采购需求单修改', 2197, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:needcard:edit', '#', 'admin', '2023-04-10 23:34:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2201, '采购需求单删除', 2197, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:needcard:remove', '#', 'admin', '2023-04-10 23:34:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2202, '采购需求单导出', 2197, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:needcard:export', '#', 'admin', '2023-04-10 23:34:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2209, '工艺管理', 2173, 1, 'craft', 'mes/craft/index', NULL, 1, 0, 'C', '0', '0', 'mes:craft:list', '#', 'admin', '2023-04-10 23:36:11', '', NULL, '工艺管理菜单');
INSERT INTO `sys_menu` VALUES (2210, '工艺管理查询', 2209, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:craft:query', '#', 'admin', '2023-04-10 23:36:11', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2211, '工艺管理新增', 2209, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:craft:add', '#', 'admin', '2023-04-10 23:36:11', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2212, '工艺管理修改', 2209, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:craft:edit', '#', 'admin', '2023-04-10 23:36:11', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2213, '工艺管理删除', 2209, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:craft:remove', '#', 'admin', '2023-04-10 23:36:11', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2214, '工艺管理导出', 2209, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:craft:export', '#', 'admin', '2023-04-10 23:36:11', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2221, '完工入库', 2196, 2, 'inboundwork', 'mes/inboundwork/index', NULL, 1, 0, 'C', '0', '0', 'mes:inboundwork:list', '#', 'admin', '2023-04-10 23:37:15', 'admin', '2023-04-11 14:35:51', '完工入库菜单');
INSERT INTO `sys_menu` VALUES (2222, '完工入库查询', 2221, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:inboundwork:query', '#', 'admin', '2023-04-10 23:37:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2223, '完工入库新增', 2221, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:inboundwork:add', '#', 'admin', '2023-04-10 23:37:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2224, '完工入库修改', 2221, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:inboundwork:edit', '#', 'admin', '2023-04-10 23:37:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2225, '完工入库删除', 2221, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:inboundwork:remove', '#', 'admin', '2023-04-10 23:37:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2226, '完工入库导出', 2221, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:inboundwork:export', '#', 'admin', '2023-04-10 23:37:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2233, '销售订单', 2192, 1, 'order', 'mes/order/index', NULL, 1, 0, 'C', '0', '0', 'mes:order:list', '#', 'admin', '2023-04-10 23:38:21', '', NULL, '销售订单菜单');
INSERT INTO `sys_menu` VALUES (2234, '销售订单查询', 2233, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:order:query', '#', 'admin', '2023-04-10 23:38:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2235, '销售订单新增', 2233, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:order:add', '#', 'admin', '2023-04-10 23:38:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2236, '销售订单修改', 2233, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:order:edit', '#', 'admin', '2023-04-10 23:38:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2237, '销售订单删除', 2233, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:order:remove', '#', 'admin', '2023-04-10 23:38:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2238, '销售订单导出', 2233, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:order:export', '#', 'admin', '2023-04-10 23:38:21', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2239, '领料出库', 2196, 1, 'pick', 'mes/pick/index', NULL, 1, 0, 'C', '0', '0', 'mes:pick:list', '#', 'admin', '2023-04-10 23:41:25', '', NULL, '领料出库菜单');
INSERT INTO `sys_menu` VALUES (2240, '领料出库查询', 2239, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:pick:query', '#', 'admin', '2023-04-10 23:41:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2241, '领料出库新增', 2239, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:pick:add', '#', 'admin', '2023-04-10 23:41:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2242, '领料出库修改', 2239, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:pick:edit', '#', 'admin', '2023-04-10 23:41:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2243, '领料出库删除', 2239, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:pick:remove', '#', 'admin', '2023-04-10 23:41:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2244, '领料出库导出', 2239, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:pick:export', '#', 'admin', '2023-04-10 23:41:25', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2245, '工序管理', 2173, 1, 'process', 'mes/process/index', NULL, 1, 0, 'C', '0', '0', 'mes:process:list', '#', 'admin', '2023-04-10 23:42:28', '', NULL, '工序管理菜单');
INSERT INTO `sys_menu` VALUES (2246, '工序管理查询', 2245, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:process:query', '#', 'admin', '2023-04-10 23:42:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2247, '工序管理新增', 2245, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:process:add', '#', 'admin', '2023-04-10 23:42:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2248, '工序管理修改', 2245, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:process:edit', '#', 'admin', '2023-04-10 23:42:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2249, '工序管理删除', 2245, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:process:remove', '#', 'admin', '2023-04-10 23:42:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2250, '工序管理导出', 2245, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:process:export', '#', 'admin', '2023-04-10 23:42:28', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2257, '车间任务卡', 2195, 1, 'taskcard', 'mes/taskcard/index', NULL, 1, 0, 'C', '0', '0', 'mes:taskcard:list', '#', 'admin', '2023-04-10 23:43:58', '', NULL, '车间任务卡菜单');
INSERT INTO `sys_menu` VALUES (2258, '车间任务卡查询', 2257, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:taskcard:query', '#', 'admin', '2023-04-10 23:43:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2259, '车间任务卡新增', 2257, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:taskcard:add', '#', 'admin', '2023-04-10 23:43:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2260, '车间任务卡修改', 2257, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:taskcard:edit', '#', 'admin', '2023-04-10 23:43:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2261, '车间任务卡删除', 2257, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:taskcard:remove', '#', 'admin', '2023-04-10 23:43:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2262, '车间任务卡导出', 2257, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:taskcard:export', '#', 'admin', '2023-04-10 23:43:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2263, '车间任务', 2195, 1, 'worktask', 'mes/worktask/index', NULL, 1, 0, 'C', '0', '0', 'mes:worktask:list', '#', 'admin', '2023-04-10 23:44:26', '', NULL, '车间任务菜单');
INSERT INTO `sys_menu` VALUES (2264, '车间任务查询', 2263, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:worktask:query', '#', 'admin', '2023-04-10 23:44:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2265, '车间任务新增', 2263, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:worktask:add', '#', 'admin', '2023-04-10 23:44:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2266, '车间任务修改', 2263, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:worktask:edit', '#', 'admin', '2023-04-10 23:44:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2267, '车间任务删除', 2263, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:worktask:remove', '#', 'admin', '2023-04-10 23:44:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2268, '车间任务导出', 2263, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:worktask:export', '#', 'admin', '2023-04-10 23:44:26', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2275, '销售出库', 2196, 1, 'sell', 'mes/sell/index', NULL, 1, 0, 'C', '0', '0', 'mes:sell:list', '#', 'admin', '2023-04-11 14:07:35', '', NULL, '销售出库菜单');
INSERT INTO `sys_menu` VALUES (2276, '销售出库查询', 2275, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:sell:query', '#', 'admin', '2023-04-11 14:07:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2277, '销售出库新增', 2275, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:sell:add', '#', 'admin', '2023-04-11 14:07:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2278, '销售出库修改', 2275, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:sell:edit', '#', 'admin', '2023-04-11 14:07:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2279, '销售出库删除', 2275, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:sell:remove', '#', 'admin', '2023-04-11 14:07:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2280, '销售出库导出', 2275, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:sell:export', '#', 'admin', '2023-04-11 14:07:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2281, '采购入库', 2196, 2, 'Inboundbuy', 'mes/Inboundbuy/index', NULL, 1, 0, 'C', '0', '0', 'mes:Inboundbuy:list', '#', 'admin', '2023-04-11 14:34:49', 'admin', '2023-04-11 14:35:43', '采购入库菜单');
INSERT INTO `sys_menu` VALUES (2282, '采购入库查询', 2281, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:Inboundbuy:query', '#', 'admin', '2023-04-11 14:34:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2283, '采购入库新增', 2281, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:Inboundbuy:add', '#', 'admin', '2023-04-11 14:34:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2284, '采购入库修改', 2281, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:Inboundbuy:edit', '#', 'admin', '2023-04-11 14:34:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2285, '采购入库删除', 2281, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:Inboundbuy:remove', '#', 'admin', '2023-04-11 14:34:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2286, '采购入库导出', 2281, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:Inboundbuy:export', '#', 'admin', '2023-04-11 14:34:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2287, '库房管理', 2196, 1, 'depot', 'mes/depot/index', NULL, 1, 0, 'C', '0', '0', 'mes:depot:list', '#', 'admin', '2023-04-11 14:44:59', '', NULL, '库房管理菜单');
INSERT INTO `sys_menu` VALUES (2288, '库房管理查询', 2287, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:depot:query', '#', 'admin', '2023-04-11 14:44:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2289, '库房管理新增', 2287, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:depot:add', '#', 'admin', '2023-04-11 14:44:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2290, '库房管理修改', 2287, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:depot:edit', '#', 'admin', '2023-04-11 14:44:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2291, '库房管理删除', 2287, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:depot:remove', '#', 'admin', '2023-04-11 14:44:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2292, '库房管理导出', 2287, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:depot:export', '#', 'admin', '2023-04-11 14:44:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2293, '库位管理', 2196, 1, 'location', 'mes/location/index', NULL, 1, 0, 'C', '0', '0', 'mes:location:list', '#', 'admin', '2023-04-11 14:48:58', '', NULL, '库位管理菜单');
INSERT INTO `sys_menu` VALUES (2294, '库位管理查询', 2293, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:location:query', '#', 'admin', '2023-04-11 14:48:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2295, '库位管理新增', 2293, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:location:add', '#', 'admin', '2023-04-11 14:48:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2296, '库位管理修改', 2293, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:location:edit', '#', 'admin', '2023-04-11 14:48:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2297, '库位管理删除', 2293, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:location:remove', '#', 'admin', '2023-04-11 14:48:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2298, '库位管理导出', 2293, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:location:export', '#', 'admin', '2023-04-11 14:48:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2299, '物料信息', 2196, 1, 'material', 'mes/material/index', NULL, 1, 0, 'C', '0', '0', 'mes:material:list', '#', 'admin', '2023-04-17 18:30:17', '', NULL, '物料信息菜单');
INSERT INTO `sys_menu` VALUES (2300, '物料信息查询', 2299, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:material:query', '#', 'admin', '2023-04-17 18:30:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2301, '物料信息新增', 2299, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:material:add', '#', 'admin', '2023-04-17 18:30:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2302, '物料信息修改', 2299, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:material:edit', '#', 'admin', '2023-04-17 18:30:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2303, '物料信息删除', 2299, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:material:remove', '#', 'admin', '2023-04-17 18:30:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2304, '物料信息导出', 2299, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:material:export', '#', 'admin', '2023-04-17 18:30:17', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2305, '设备信息', 2196, 1, 'equipment', 'mes/equipment/index', NULL, 1, 0, 'C', '0', '0', 'mes:equipment:list', '#', 'admin', '2023-04-17 18:32:44', '', NULL, '设备信息菜单');
INSERT INTO `sys_menu` VALUES (2306, '设备信息查询', 2305, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:equipment:query', '#', 'admin', '2023-04-17 18:32:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2307, '设备信息新增', 2305, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:equipment:add', '#', 'admin', '2023-04-17 18:32:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2308, '设备信息修改', 2305, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:equipment:edit', '#', 'admin', '2023-04-17 18:32:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2309, '设备信息删除', 2305, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:equipment:remove', '#', 'admin', '2023-04-17 18:32:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2310, '设备信息导出', 2305, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:equipment:export', '#', 'admin', '2023-04-17 18:32:44', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2311, '工序物料', 2245, 1, 'material', 'mes/material/index', NULL, 1, 0, 'C', '0', '0', 'mes:material:list', '#', 'admin', '2023-04-18 10:51:09', '', NULL, '工序物料菜单');
INSERT INTO `sys_menu` VALUES (2312, '工序物料查询', 2311, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:material:query', '#', 'admin', '2023-04-18 10:51:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2313, '工序物料新增', 2311, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:material:add', '#', 'admin', '2023-04-18 10:51:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2314, '工序物料修改', 2311, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:material:edit', '#', 'admin', '2023-04-18 10:51:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2315, '工序物料删除', 2311, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:material:remove', '#', 'admin', '2023-04-18 10:51:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2316, '工序物料导出', 2311, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:material:export', '#', 'admin', '2023-04-18 10:51:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2317, 'bom物料', 2186, 1, 'bommaterial', 'mes/bommaterial/index', NULL, 1, 0, 'C', '0', '0', 'mes:bommaterial:list', '#', 'admin', '2023-04-20 11:49:51', '', NULL, 'bom物料菜单');
INSERT INTO `sys_menu` VALUES (2318, 'bom物料查询', 2317, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:bommaterial:query', '#', 'admin', '2023-04-20 11:49:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2319, 'bom物料新增', 2317, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:bommaterial:add', '#', 'admin', '2023-04-20 11:49:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2320, 'bom物料修改', 2317, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:bommaterial:edit', '#', 'admin', '2023-04-20 11:49:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2321, 'bom物料删除', 2317, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:bommaterial:remove', '#', 'admin', '2023-04-20 11:49:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2322, 'bom物料导出', 2317, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:bommaterial:export', '#', 'admin', '2023-04-20 11:49:51', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2323, '订单产品表', 2192, 1, 'ordpro', 'mes/ordpro/index', NULL, 1, 0, 'C', '0', '0', 'mes:ordpro:list', '#', 'admin', '2023-05-05 15:08:10', '', NULL, '订单产品表菜单');
INSERT INTO `sys_menu` VALUES (2324, '订单产品表查询', 2323, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:ordpro:query', '#', 'admin', '2023-05-05 15:08:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2325, '订单产品表新增', 2323, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:ordpro:add', '#', 'admin', '2023-05-05 15:08:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2326, '订单产品表修改', 2323, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:ordpro:edit', '#', 'admin', '2023-05-05 15:08:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2327, '订单产品表删除', 2323, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:ordpro:remove', '#', 'admin', '2023-05-05 15:08:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2328, '订单产品表导出', 2323, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:ordpro:export', '#', 'admin', '2023-05-05 15:08:10', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2329, '物料计划', 2193, 1, 'MatPlan', 'mes/MatPlan/index', NULL, 1, 0, 'C', '0', '0', 'mes:MatPlan:list', '#', 'admin', '2023-05-08 19:24:09', '', NULL, '物料计划菜单');
INSERT INTO `sys_menu` VALUES (2330, '物料计划查询', 2329, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:MatPlan:query', '#', 'admin', '2023-05-08 19:24:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2331, '物料计划新增', 2329, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:MatPlan:add', '#', 'admin', '2023-05-08 19:24:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2332, '物料计划修改', 2329, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:MatPlan:edit', '#', 'admin', '2023-05-08 19:24:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2333, '物料计划删除', 2329, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:MatPlan:remove', '#', 'admin', '2023-05-08 19:24:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2334, '物料计划导出', 2329, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:MatPlan:export', '#', 'admin', '2023-05-08 19:24:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2341, '生产计划', 2193, 1, 'Proplan', 'mes/Proplan/index', NULL, 1, 0, 'C', '0', '0', 'mes:Proplan:list', '#', 'admin', '2023-05-08 20:46:24', '', NULL, '生产计划菜单');
INSERT INTO `sys_menu` VALUES (2342, '生产计划查询', 2341, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:Proplan:query', '#', 'admin', '2023-05-08 20:46:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2343, '生产计划新增', 2341, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:Proplan:add', '#', 'admin', '2023-05-08 20:46:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2344, '生产计划修改', 2341, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:Proplan:edit', '#', 'admin', '2023-05-08 20:46:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2345, '生产计划删除', 2341, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:Proplan:remove', '#', 'admin', '2023-05-08 20:46:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2346, '生产计划导出', 2341, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:Proplan:export', '#', 'admin', '2023-05-08 20:46:24', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2347, '物料计划详细', 2329, 1, 'matplandetail', 'mes/matplandetail/index', NULL, 1, 0, 'C', '0', '0', 'mes:matplandetail:list', '#', 'admin', '2023-05-08 21:23:09', '', NULL, '物料计划详细菜单');
INSERT INTO `sys_menu` VALUES (2348, '物料计划详细查询', 2347, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:matplandetail:query', '#', 'admin', '2023-05-08 21:23:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2349, '物料计划详细新增', 2347, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:matplandetail:add', '#', 'admin', '2023-05-08 21:23:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2350, '物料计划详细修改', 2347, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:matplandetail:edit', '#', 'admin', '2023-05-08 21:23:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2351, '物料计划详细删除', 2347, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:matplandetail:remove', '#', 'admin', '2023-05-08 21:23:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2352, '物料计划详细导出', 2347, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:matplandetail:export', '#', 'admin', '2023-05-08 21:23:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2353, '采购订单', 2194, 1, 'buyord', 'mes/buyord/index', NULL, 1, 0, 'C', '0', '0', 'mes:buyord:list', '#', 'admin', '2023-05-08 21:44:02', '', NULL, '采购订单菜单');
INSERT INTO `sys_menu` VALUES (2354, '采购订单查询', 2353, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:buyord:query', '#', 'admin', '2023-05-08 21:44:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2355, '采购订单新增', 2353, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:buyord:add', '#', 'admin', '2023-05-08 21:44:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2356, '采购订单修改', 2353, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:buyord:edit', '#', 'admin', '2023-05-08 21:44:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2357, '采购订单删除', 2353, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:buyord:remove', '#', 'admin', '2023-05-08 21:44:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2358, '采购订单导出', 2353, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:buyord:export', '#', 'admin', '2023-05-08 21:44:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2359, '生产计划详情', 2341, 1, 'proplandetial', 'mes/proplandetial/index', NULL, 1, 0, 'C', '0', '0', 'mes:proplandetial:list', '#', 'admin', '2023-05-15 21:47:45', '', NULL, '生产计划详情菜单');
INSERT INTO `sys_menu` VALUES (2360, '生产计划详情查询', 2359, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:proplandetial:query', '#', 'admin', '2023-05-15 21:47:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2361, '生产计划详情新增', 2359, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:proplandetial:add', '#', 'admin', '2023-05-15 21:47:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2362, '生产计划详情修改', 2359, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:proplandetial:edit', '#', 'admin', '2023-05-15 21:47:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2363, '生产计划详情删除', 2359, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:proplandetial:remove', '#', 'admin', '2023-05-15 21:47:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2364, '生产计划详情导出', 2359, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:proplandetial:export', '#', 'admin', '2023-05-15 21:47:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2371, '生产领料', 2195, 1, 'picking', 'mes/picking/index', NULL, 1, 0, 'C', '0', '0', 'mes:picking:list', '#', 'admin', '2023-05-23 11:30:37', '', NULL, '生产领料菜单');
INSERT INTO `sys_menu` VALUES (2372, '生产领料查询', 2371, 1, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:picking:query', '#', 'admin', '2023-05-23 11:30:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2373, '生产领料新增', 2371, 2, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:picking:add', '#', 'admin', '2023-05-23 11:30:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2374, '生产领料修改', 2371, 3, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:picking:edit', '#', 'admin', '2023-05-23 11:30:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2375, '生产领料删除', 2371, 4, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:picking:remove', '#', 'admin', '2023-05-23 11:30:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2376, '生产领料导出', 2371, 5, '#', '', NULL, 1, 0, 'F', '0', '0', 'mes:picking:export', '#', 'admin', '2023-05-23 11:30:37', '', NULL, '');

SET FOREIGN_KEY_CHECKS = 1;
