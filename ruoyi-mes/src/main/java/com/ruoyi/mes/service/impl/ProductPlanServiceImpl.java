package com.ruoyi.mes.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.mes.domain.ProductInfo;
import com.ruoyi.mes.domain.ProductPlanDetial;
import com.ruoyi.mes.domain.Vo.PlanIssueVo;
import com.ruoyi.mes.domain.Vo.ProPlanDetailVo;
import com.ruoyi.mes.mapper.ProductInfoMapper;
import com.ruoyi.mes.mapper.ProductPlanDetialMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.ProductPlanMapper;
import com.ruoyi.mes.domain.ProductPlan;
import com.ruoyi.mes.service.IProductPlanService;

/**
 * 生产计划Service业务层处理
 * 
 * @author g4
 * @date 2023-05-08
 */
@Service
public class ProductPlanServiceImpl implements IProductPlanService 
{
    @Autowired
    private ProductPlanMapper productPlanMapper;

    @Autowired
    private ProductPlanDetialMapper productPlanDetialMapper;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    /**
     * 查询生产计划
     * 
     * @param id 生产计划主键
     * @return 生产计划
     */
    @Override
    public ProductPlan selectProductPlanById(Long id)
    {
        return productPlanMapper.selectProductPlanById(id);
    }

    /**
     * 查询生产计划列表
     * 
     * @param productPlan 生产计划
     * @return 生产计划
     */
    @Override
    public List<ProductPlan> selectProductPlanList(ProductPlan productPlan)
    {
        return productPlanMapper.selectProductPlanList(productPlan);
    }

    /**
     * 新增生产计划
     * 
     * @param productPlan 生产计划
     * @return 结果
     */
    @Override
    public int insertProductPlan(ProductPlan productPlan)
    {
        productPlan.setCreateTime(DateUtils.getNowDate());
        return productPlanMapper.insertProductPlan(productPlan);
    }

    /**
     * 修改生产计划
     * 
     * @param productPlan 生产计划
     * @return 结果
     */
    @Override
    public int updateProductPlan(ProductPlan productPlan)
    {
        return productPlanMapper.updateProductPlan(productPlan);
    }

    /**
     * 批量删除生产计划
     * 
     * @param ids 需要删除的生产计划主键
     * @return 结果
     */
    @Override
    public int deleteProductPlanByIds(Long[] ids)
    {
        return productPlanMapper.deleteProductPlanByIds(ids);
    }

    /**
     * 删除生产计划信息
     * 
     * @param id 生产计划主键
     * @return 结果
     */
    @Override
    public int deleteProductPlanById(Long id)
    {
        return productPlanMapper.deleteProductPlanById(id);
    }



    /**
     * 获取生产计划明细
     */
    @Override
    public ProPlanDetailVo selectProductPlanByproplanCode(String proplanCode) {
        ProPlanDetailVo proPlanDetailVo = new ProPlanDetailVo();
        ProductPlan productPlan = productPlanMapper.selectProductPlanByCode(proplanCode);
        ProductInfo productInfo = productInfoMapper.selectProductInfoByName(productPlan.getProductName());
        proPlanDetailVo.setProductPlan(productPlan);
        proPlanDetailVo.setProductInfo(productInfo);
        return proPlanDetailVo;
    }


}
