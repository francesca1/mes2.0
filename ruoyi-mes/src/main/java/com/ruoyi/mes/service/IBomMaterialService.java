package com.ruoyi.mes.service;

import java.util.List;
import com.ruoyi.mes.domain.BomMaterial;
import com.ruoyi.mes.domain.Material;

/**
 * bom物料Service接口
 * 
 * @author g4
 * @date 2023-04-20
 */
public interface IBomMaterialService 
{
    /**
     * 查询bom物料
     * 
     * @param id bom物料主键
     * @return bom物料
     */
    public BomMaterial selectBomMaterialById(Long id);

    /**
     * 查询bom物料列表
     * 
     * @param bomMaterial bom物料
     * @return bom物料集合
     */
    public List<BomMaterial> selectBomMaterialList(BomMaterial bomMaterial);

    /**
     * 新增bom物料
     * 
     * @param bomMaterial bom物料
     * @return 结果
     */
    public int insertBomMaterial(BomMaterial bomMaterial);

    /**
     * 修改bom物料
     * 
     * @param bomMaterial bom物料
     * @return 结果
     */
    public int updateBomMaterial(BomMaterial bomMaterial);

    /**
     * 批量删除bom物料
     * 
     * @param ids 需要删除的bom物料主键集合
     * @return 结果
     */
    public int deleteBomMaterialByIds(Long[] ids);

    /**
     * 删除bom物料信息
     * 
     * @param id bom物料主键
     * @return 结果
     */
    public int deleteBomMaterialById(Long id);

    /**
     * 查 - Bom物料信息 - bomCode
     * @param bomCode
     * @return
     */
    public List<BomMaterial> selectMaterialsByBomCode(String bomCode);
}
