package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.mes.domain.Vo.PickVo;
import com.ruoyi.mes.domain.Vo.WorkTaskVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.WorkTask;
import com.ruoyi.mes.service.IWorkTaskService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 车间任务Controller
 * 
 * @author g4
 * @date 2023-05-22
 */
@RestController
@RequestMapping("/mes/worktask")
public class WorkTaskController extends BaseController
{
    @Autowired
    private IWorkTaskService workTaskService;

    /**
     * 查询车间任务列表
     */
    @PreAuthorize("@ss.hasPermi('mes:worktask:list')")
    @GetMapping("/list")
    public TableDataInfo list(WorkTask workTask)
    {
        startPage();
        List<WorkTask> list = workTaskService.selectWorkTaskList(workTask);
        return getDataTable(list);
    }

    /**
     * 导出车间任务列表
     */
    @PreAuthorize("@ss.hasPermi('mes:worktask:export')")
    @Log(title = "车间任务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WorkTask workTask)
    {
        List<WorkTask> list = workTaskService.selectWorkTaskList(workTask);
        ExcelUtil<WorkTask> util = new ExcelUtil<WorkTask>(WorkTask.class);
        util.exportExcel(response, list, "车间任务数据");
    }

    /**
     * 获取车间任务详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:worktask:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(workTaskService.selectWorkTaskById(id));
    }

    /**
     * 新增车间任务
     */
    @PreAuthorize("@ss.hasPermi('mes:worktask:add')")
    @Log(title = "车间任务", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WorkTask workTask)
    {
        return toAjax(workTaskService.insertWorkTask(workTask));
    }

    /**
     * 修改车间任务
     */
    @PreAuthorize("@ss.hasPermi('mes:worktask:edit')")
    @Log(title = "车间任务", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WorkTask workTask)
    {
        return toAjax(workTaskService.updateWorkTask(workTask));
    }

    /**
     * 删除车间任务
     */
    @PreAuthorize("@ss.hasPermi('mes:worktask:remove')")
    @Log(title = "车间任务", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(workTaskService.deleteWorkTaskByIds(ids));
    }

    /**
     * 确认开工
     */
    @PostMapping("/submitStart")
    public AjaxResult submitStart(@RequestBody WorkTaskVo workTaskVo)
    {
        return workTaskService.insertsubmitStart(workTaskVo);
    }

    /**
     * 领料清单
     */
    @GetMapping("/getPickMat/{workTaskCode}")
    public AjaxResult getPickMat(@PathVariable("workTaskCode") String workTaskCode){
        return workTaskService.selectgetPickMatByCode(workTaskCode);
    }

    /**
     * 确定领料
     */
    @PostMapping("/submitPicking")
    public AjaxResult submitPicking(@RequestBody PickVo pickVo){
        return workTaskService.insertsubmitPicking(pickVo);
    }



}
