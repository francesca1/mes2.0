package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.OrderTable;

/**
 * 销售订单Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
public interface OrderTableMapper 
{
    /**
     * 查询销售订单
     * 
     * @param id 销售订单主键
     * @return 销售订单
     */
    public OrderTable selectOrderTableById(Long id);

    /**
     * 查询销售订单列表
     * 
     * @param orderTable 销售订单
     * @return 销售订单集合
     */
    public List<OrderTable> selectOrderTableList(OrderTable orderTable);

    /**
     * 新增销售订单
     * 
     * @param orderTable 销售订单
     * @return 结果
     */
    public int insertOrderTable(OrderTable orderTable);

    /**
     * 修改销售订单
     * 
     * @param orderTable 销售订单
     * @return 结果
     */
    public int updateOrderTable(OrderTable orderTable);

    /**
     * 删除销售订单
     * 
     * @param id 销售订单主键
     * @return 结果
     */
    public int deleteOrderTableById(Long id);

    /**
     * 批量删除销售订单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrderTableByIds(Long[] ids);

    OrderTable selectOrderTableByCode(String ordCode);

}
