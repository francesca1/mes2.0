package com.ruoyi.mes.domain.Vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.mes.domain.Material;
import lombok.Data;

import java.util.List;

@Data
public class BuyOrdMatVo {
    /** 采购订单编号 */
    @Excel(name = "采购订单编号")
    private String buyordCode;

    /** 采购订单名称 */
    @Excel(name = "采购订单名称")
    private String name;

    /** 采购需求单编号 */
    @Excel(name = "采购需求单编号")
    private String buyneedCode;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractNum;



    private List<Material> materials;
}
