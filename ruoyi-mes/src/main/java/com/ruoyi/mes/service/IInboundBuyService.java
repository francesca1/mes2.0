package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.InboundBuy;
import com.ruoyi.mes.domain.Vo.BinDepotVo;

/**
 * 采购入库Service接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface IInboundBuyService 
{
    /**
     * 查询采购入库
     * 
     * @param id 采购入库主键
     * @return 采购入库
     */
    public InboundBuy selectInboundBuyById(Long id);

    /**
     * 查询采购入库列表
     * 
     * @param inboundBuy 采购入库
     * @return 采购入库集合
     */
    public List<InboundBuy> selectInboundBuyList(InboundBuy inboundBuy);

    /**
     * 新增采购入库
     * 
     * @param inboundBuy 采购入库
     * @return 结果
     */
    public int insertInboundBuy(InboundBuy inboundBuy);

    /**
     * 修改采购入库
     * 
     * @param inboundBuy 采购入库
     * @return 结果
     */
    public int updateInboundBuy(InboundBuy inboundBuy);

    /**
     * 批量删除采购入库
     * 
     * @param ids 需要删除的采购入库主键集合
     * @return 结果
     */
    public int deleteInboundBuyByIds(Long[] ids);

    /**
     * 删除采购入库信息
     * 
     * @param id 采购入库主键
     * @return 结果
     */
    public int deleteInboundBuyById(Long id);

    /**
     * 确认采购入库-确认入库库存增加
     */
    AjaxResult insertaddstocks(BinDepotVo binDepotVo);

}
