package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.Depot;
import com.ruoyi.mes.domain.Vo.BinDepotVo;
import com.ruoyi.mes.mapper.DepotMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.BinLocationMapper;
import com.ruoyi.mes.domain.BinLocation;
import com.ruoyi.mes.service.IBinLocationService;

/**
 * 库位管理Service业务层处理
 * 
 * @author g4
 * @date 2023-04-11
 */
@Service
public class BinLocationServiceImpl implements IBinLocationService 
{
    @Autowired
    private BinLocationMapper binLocationMapper;

    @Autowired
    private DepotMapper depotMapper;

    /**
     * 查询库位管理
     * 
     * @param id 库位管理主键
     * @return 库位管理
     */
    @Override
    public BinLocation selectBinLocationById(Integer id)
    {
        return binLocationMapper.selectBinLocationById(id);
    }

    /**
     * 查询库位管理列表
     * 
     * @param binLocation 库位管理
     * @return 库位管理
     */
    @Override
    public List<BinLocation> selectBinLocationList(BinLocation binLocation)
    {
        return binLocationMapper.selectBinLocationList(binLocation);
    }

    /**
     * 新增库位管理
     * 
     * @param binLocation 库位管理
     * @return 结果
     */
    @Override
    public int insertBinLocation(BinLocation binLocation)
    {
        return binLocationMapper.insertBinLocation(binLocation);
    }

    /**
     * 修改库位管理
     * 
     * @param binLocation 库位管理
     * @return 结果
     */
    @Override
    public int updateBinLocation(BinLocation binLocation)
    {
        return binLocationMapper.updateBinLocation(binLocation);
    }

    /**
     * 批量删除库位管理
     * 
     * @param ids 需要删除的库位管理主键
     * @return 结果
     */
    @Override
    public int deleteBinLocationByIds(Integer[] ids)
    {
        return binLocationMapper.deleteBinLocationByIds(ids);
    }

    /**
     * 删除库位管理信息
     * 
     * @param id 库位管理主键
     * @return 结果
     */
    @Override
    public int deleteBinLocationById(Integer id)
    {
        return binLocationMapper.deleteBinLocationById(id);
    }

    /**
     * 新增库位
     */
    @Override
    public AjaxResult insertaddLocation(BinDepotVo binDepotVo) {
        BinLocation binLocation = new BinLocation();
        binLocation.setBinLocation(binDepotVo.getBinLocation());
        binLocation.setDepotName(binDepotVo.getDepotName());
        binLocation.setHeadman(binDepotVo.getHeadman());
        binLocationMapper.insertBinLocation(binLocation);
        return AjaxResult.success("添加成功");
    }
}
