package com.ruoyi.mes.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单产品表对象 ordpro
 * 
 * @author ruoyi
 * @date 2023-05-05
 */
public class Ordpro extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String ordCode;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 产品编码 */
    @Excel(name = "产品编码")
    private String productCode;

    /** 产品需求数量 */
    @Excel(name = "产品需求数量")
    private Long productNum;

    /** 产品价格 */
    @Excel(name = "产品价格")
    private BigDecimal proprice;

    public void setOrdCode(String ordCode) 
    {
        this.ordCode = ordCode;
    }

    public String getOrdCode() 
    {
        return ordCode;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setProductCode(String productCode) 
    {
        this.productCode = productCode;
    }

    public String getProductCode() 
    {
        return productCode;
    }
    public void setProductNum(Long productNum) 
    {
        this.productNum = productNum;
    }

    public Long getProductNum() 
    {
        return productNum;
    }

    public BigDecimal getProprice() {
        return proprice;
    }

    public void setProprice(BigDecimal proprice) {
        this.proprice = proprice;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("ordCode", getOrdCode())
            .append("productName", getProductName())
            .append("productCode", getProductCode())
            .append("productNum", getProductNum())
            .append("proprice", getProprice())
            .toString();
    }
}
