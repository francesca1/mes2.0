package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.Picking;
import com.ruoyi.mes.service.IPickingService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 生产领料Controller
 * 
 * @author g4
 * @date 2023-05-23
 */
@RestController
@RequestMapping("/mes/picking")
public class PickingController extends BaseController
{
    @Autowired
    private IPickingService pickingService;

    /**
     * 查询生产领料列表
     */
    @PreAuthorize("@ss.hasPermi('mes:picking:list')")
    @GetMapping("/list")
    public TableDataInfo list(Picking picking)
    {
        startPage();
        List<Picking> list = pickingService.selectPickingList(picking);
        return getDataTable(list);
    }

    /**
     * 导出生产领料列表
     */
    @PreAuthorize("@ss.hasPermi('mes:picking:export')")
    @Log(title = "生产领料", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Picking picking)
    {
        List<Picking> list = pickingService.selectPickingList(picking);
        ExcelUtil<Picking> util = new ExcelUtil<Picking>(Picking.class);
        util.exportExcel(response, list, "生产领料数据");
    }

    /**
     * 获取生产领料详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:picking:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(pickingService.selectPickingById(id));
    }

    /**
     * 新增生产领料
     */
    @PreAuthorize("@ss.hasPermi('mes:picking:add')")
    @Log(title = "生产领料", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Picking picking)
    {
        return toAjax(pickingService.insertPicking(picking));
    }

    /**
     * 修改生产领料
     */
    @PreAuthorize("@ss.hasPermi('mes:picking:edit')")
    @Log(title = "生产领料", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Picking picking)
    {
        return toAjax(pickingService.updatePicking(picking));
    }

    /**
     * 删除生产领料
     */
    @PreAuthorize("@ss.hasPermi('mes:picking:remove')")
    @Log(title = "生产领料", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(pickingService.deletePickingByIds(ids));
    }
}
