package com.ruoyi.mes.domain.Vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;
@Data
public class PlanIssueVo {
    //Q1：下达状态如何改变

    /** 生产计划编码 */
    @Excel(name = "生产计划编码")
    private String proplanCode;

    /** 下达日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "下达日期")
    private Date issueDate;

    /** 下达数量 */
    @Excel(name = "下达数量")
    private Long issueNum;

    /** 计划开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planStartTime;

    /** 计划截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划截止时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planOverTime;

    /** 计划下达状态 */
    @Excel(name = "计划下达状态")
    private String issueStatus;

}
