package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.MatPlanDetail;
import org.apache.ibatis.annotations.Param;

/**
 * 物料计划详细Mapper接口
 * 
 * @author g4
 * @date 2023-05-08
 */
public interface MatPlanDetailMapper 
{
    /**
     * 查询物料计划详细
     * 
     * @param id 物料计划详细主键
     * @return 物料计划详细
     */
    public MatPlanDetail selectMatPlanDetailById(Long id);

    /**
     * 查询物料计划详细列表
     * 
     * @param matPlanDetail 物料计划详细
     * @return 物料计划详细集合
     */
    public List<MatPlanDetail> selectMatPlanDetailList(MatPlanDetail matPlanDetail);

    /**
     * 新增物料计划详细
     * 
     * @param matPlanDetail 物料计划详细
     * @return 结果
     */
    public int insertMatPlanDetail(MatPlanDetail matPlanDetail);

    /**
     * 修改物料计划详细
     * 
     * @param matPlanDetail 物料计划详细
     * @return 结果
     */
    public int updateMatPlanDetail(MatPlanDetail matPlanDetail);

    /**
     * 删除物料计划详细
     * 
     * @param id 物料计划详细主键
     * @return 结果
     */
    public int deleteMatPlanDetailById(Long id);

    /**
     * 批量删除物料计划详细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMatPlanDetailByIds(Long[] ids);


    MatPlanDetail selectMatPlanDetailByMatName(String materialName);

    /**
     * 查看物料计划明细
     */
    List<MatPlanDetail> selectMatPlanDetailListByCodeName(@Param(value = "matplanCode") String matplanCode, @Param(value = "materialName") String materialName);

    MatPlanDetail selectMatPlanDetailByCode(String matplanCode);

    List<MatPlanDetail> selectMatPlanDetailListByCode(String matplanCode);

    MatPlanDetail selectMatPlanDetailByCodeName(@Param(value = "matplanCode") String matplanCode, @Param(value = "materialName") String materialName);
}
