package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.ProcessMaterial;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 工序物料Mapper接口
 * 
 * @author g4
 * @date 2023-04-20
 */
public interface ProcessMaterialMapper 
{
    /**
     * 查询工序物料
     * 
     * @param id 工序物料主键
     * @return 工序物料
     */
    public ProcessMaterial selectProcessMaterialById(Long id);

    /**
     * 查询工序物料列表
     * 
     * @param processMaterial 工序物料
     * @return 工序物料集合
     */
    public List<ProcessMaterial> selectProcessMaterialList(ProcessMaterial processMaterial);

    /**
     * 新增工序物料
     * 
     * @param processMaterial 工序物料
     * @return 结果
     */
    public int insertProcessMaterial(ProcessMaterial processMaterial);

    /**
     * 修改工序物料
     * 
     * @param processMaterial 工序物料
     * @return 结果
     */
    public int updateProcessMaterial(ProcessMaterial processMaterial);

    /**
     * 删除工序物料
     * 
     * @param id 工序物料主键
     * @return 结果
     */
    public int deleteProcessMaterialById(Long id);

    /**
     * 批量删除工序物料
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProcessMaterialByIds(Long[] ids);

    /**
     * 查 - processmaterial -工艺编码 工序名称
     * @param craftCode
     * @param proccessName
     * @return
     */

    List<ProcessMaterial> selectProMatByNameCode(@Param(value = "craftCode") String craftCode, @Param(value = "proccessName") String proccessName);


    List<ProcessMaterial> selectProMatByCode(String craftCode);

}
