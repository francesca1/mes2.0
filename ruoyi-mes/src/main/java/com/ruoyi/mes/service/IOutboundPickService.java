package com.ruoyi.mes.service;

import java.util.List;
import com.ruoyi.mes.domain.OutboundPick;

/**
 * 领料出库Service接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface IOutboundPickService 
{
    /**
     * 查询领料出库
     * 
     * @param id 领料出库主键
     * @return 领料出库
     */
    public OutboundPick selectOutboundPickById(Long id);

    /**
     * 查询领料出库列表
     * 
     * @param outboundPick 领料出库
     * @return 领料出库集合
     */
    public List<OutboundPick> selectOutboundPickList(OutboundPick outboundPick);

    /**
     * 新增领料出库
     * 
     * @param outboundPick 领料出库
     * @return 结果
     */
    public int insertOutboundPick(OutboundPick outboundPick);

    /**
     * 修改领料出库
     * 
     * @param outboundPick 领料出库
     * @return 结果
     */
    public int updateOutboundPick(OutboundPick outboundPick);

    /**
     * 批量删除领料出库
     * 
     * @param ids 需要删除的领料出库主键集合
     * @return 结果
     */
    public int deleteOutboundPickByIds(Long[] ids);

    /**
     * 删除领料出库信息
     * 
     * @param id 领料出库主键
     * @return 结果
     */
    public int deleteOutboundPickById(Long id);
}
