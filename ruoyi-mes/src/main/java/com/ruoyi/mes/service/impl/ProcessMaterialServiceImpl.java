package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.ProcessMaterialMapper;
import com.ruoyi.mes.domain.ProcessMaterial;
import com.ruoyi.mes.service.IProcessMaterialService;

/**
 * 工序物料Service业务层处理
 * 
 * @author g4
 * @date 2023-04-20
 */
@Service
public class ProcessMaterialServiceImpl implements IProcessMaterialService 
{
    @Autowired
    private ProcessMaterialMapper processMaterialMapper;

    /**
     * 查询工序物料
     * 
     * @param id 工序物料主键
     * @return 工序物料
     */
    @Override
    public ProcessMaterial selectProcessMaterialById(Long id)
    {
        return processMaterialMapper.selectProcessMaterialById(id);
    }

    /**
     * 查询工序物料列表
     * 
     * @param processMaterial 工序物料
     * @return 工序物料
     */
    @Override
    public List<ProcessMaterial> selectProcessMaterialList(ProcessMaterial processMaterial)
    {
        return processMaterialMapper.selectProcessMaterialList(processMaterial);
    }

    /**
     * 新增工序物料
     * 
     * @param processMaterial 工序物料
     * @return 结果
     */
    @Override
    public int insertProcessMaterial(ProcessMaterial processMaterial)
    {
        return processMaterialMapper.insertProcessMaterial(processMaterial);
    }

    /**
     * 修改工序物料
     * 
     * @param processMaterial 工序物料
     * @return 结果
     */
    @Override
    public int updateProcessMaterial(ProcessMaterial processMaterial)
    {
        return processMaterialMapper.updateProcessMaterial(processMaterial);
    }

    /**
     * 批量删除工序物料
     * 
     * @param ids 需要删除的工序物料主键
     * @return 结果
     */
    @Override
    public int deleteProcessMaterialByIds(Long[] ids)
    {
        return processMaterialMapper.deleteProcessMaterialByIds(ids);
    }

    /**
     * 删除工序物料信息
     * 
     * @param id 工序物料主键
     * @return 结果
     */
    @Override
    public int deleteProcessMaterialById(Long id)
    {
        return processMaterialMapper.deleteProcessMaterialById(id);
    }

    /**
     * 查 - 工序对应物料和设备
     * @param processName
     * @return
     */
    @Override
    public AjaxResult selectProcessMaterialByName(String processName) {
        return null;
    }

    public void deleteProcessMaterialByCodeName() {


    }
}
