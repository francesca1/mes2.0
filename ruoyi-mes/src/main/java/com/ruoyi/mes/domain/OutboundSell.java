package com.ruoyi.mes.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 销售出库对象 outbound_sell
 * 
 * @author g4
 * @date 2023-04-11
 */
@Data
public class OutboundSell extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 生产计划编码 */
    @Excel(name = "生产计划编码")
    private String proplanCode;

    /** 仓库id */
    @Excel(name = "仓库名称")
    private String depotName;

    /** 订单编码 */
    @Excel(name = "订单编码")
    private String ordCode;

    /** 出库状态 */
    @Excel(name = "出库状态")
    private String outboundStatus;

    /** 出库数量 */
    @Excel(name = "出库数量")
    private Long outboundNum;

    /** 出库类型 */
    @Excel(name = "出库类型")
    private String outboundType;

    /** 库位 */
    @Excel(name = "库位")
    private String binLocation;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    @TableField(exist = false)
    private String productName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public String getProplanCode() {
        return proplanCode;
    }

    public void setProplanCode(String proplanCode) {
        this.proplanCode = proplanCode;
    }

    public String getDepotName() {
        return depotName;
    }

    public void setDepotName(String depotName) {
        this.depotName = depotName;
    }

    public String getOrdCode() {
        return ordCode;
    }

    public void setOrdCode(String ordCode) {
        this.ordCode = ordCode;
    }

    public void setOutboundStatus(String outboundStatus)
    {
        this.outboundStatus = outboundStatus;
    }

    public String getOutboundStatus() 
    {
        return outboundStatus;
    }
    public void setOutboundNum(Long outboundNum) 
    {
        this.outboundNum = outboundNum;
    }

    public Long getOutboundNum() 
    {
        return outboundNum;
    }

    public String getOutboundType() {
        return outboundType;
    }

    public void setOutboundType(String outboundType) {
        this.outboundType = outboundType;
    }

    public String getBinLocation() {
        return binLocation;
    }

    public void setBinLocation(String binLocation) {
        this.binLocation = binLocation;
    }

    public void setNote(String note)
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("proplanCode", getProplanCode())
            .append("depotName", getDepotName())
            .append("ordCode", getOrdCode())
            .append("outboundStatus", getOutboundStatus())
            .append("outboundNum", getOutboundNum())
            .append("outboundType", getOutboundType())
            .append("binLocation", getBinLocation())
            .append("note", getNote())
            .toString();
    }
}
