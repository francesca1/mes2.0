package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.InboundBuy;

/**
 * 采购入库Mapper接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface InboundBuyMapper 
{
    /**
     * 查询采购入库
     * 
     * @param id 采购入库主键
     * @return 采购入库
     */
    public InboundBuy selectInboundBuyById(Long id);

    /**
     * 查询采购入库列表
     * 
     * @param inboundBuy 采购入库
     * @return 采购入库集合
     */
    public List<InboundBuy> selectInboundBuyList(InboundBuy inboundBuy);

    /**
     * 新增采购入库
     * 
     * @param inboundBuy 采购入库
     * @return 结果
     */
    public int insertInboundBuy(InboundBuy inboundBuy);

    /**
     * 修改采购入库
     * 
     * @param inboundBuy 采购入库
     * @return 结果
     */
    public int updateInboundBuy(InboundBuy inboundBuy);

    /**
     * 删除采购入库
     * 
     * @param id 采购入库主键
     * @return 结果
     */
    public int deleteInboundBuyById(Long id);

    /**
     * 批量删除采购入库
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteInboundBuyByIds(Long[] ids);

    InboundBuy selectInboundBuyByCode(String buyordCode);

}
