package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.mes.domain.Vo.BinDepotVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.BinLocation;
import com.ruoyi.mes.service.IBinLocationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 库位管理Controller
 * 
 * @author g4
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/location")
public class BinLocationController extends BaseController
{
    @Autowired
    private IBinLocationService binLocationService;

    /**
     * 查询库位管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:location:list')")
    @GetMapping("/list")
    public TableDataInfo list(BinLocation binLocation)
    {
        startPage();
        List<BinLocation> list = binLocationService.selectBinLocationList(binLocation);
        return getDataTable(list);
    }

    /**
     * 导出库位管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:location:export')")
    @Log(title = "库位管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BinLocation binLocation)
    {
        List<BinLocation> list = binLocationService.selectBinLocationList(binLocation);
        ExcelUtil<BinLocation> util = new ExcelUtil<BinLocation>(BinLocation.class);
        util.exportExcel(response, list, "库位管理数据");
    }

    /**
     * 获取库位管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:location:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return success(binLocationService.selectBinLocationById(id));
    }

    /**
     * 新增库位管理
     */
    @PreAuthorize("@ss.hasPermi('mes:location:add')")
    @Log(title = "库位管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BinLocation binLocation)
    {
        return toAjax(binLocationService.insertBinLocation(binLocation));
    }

    /**
     * 修改库位管理
     */
    @PreAuthorize("@ss.hasPermi('mes:location:edit')")
    @Log(title = "库位管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BinLocation binLocation)
    {
        return toAjax(binLocationService.updateBinLocation(binLocation));
    }

    /**
     * 删除库位管理
     */
    @PreAuthorize("@ss.hasPermi('mes:location:remove')")
    @Log(title = "库位管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(binLocationService.deleteBinLocationByIds(ids));
    }

    /**
     * 新增库位
     */
    @PostMapping("/addLocation")
    public AjaxResult addLocation(@RequestBody BinDepotVo binDepotVo)
    {
        return binLocationService.insertaddLocation(binDepotVo);
    }
}
