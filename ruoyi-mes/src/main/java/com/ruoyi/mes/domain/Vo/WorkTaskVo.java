package com.ruoyi.mes.domain.Vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

@Data
public class WorkTaskVo {
    /** 车间任务id */
    private Long id;

    /** 计划开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planStartTime;

    /** 计划截止日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划截止日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planOverTime;

}
