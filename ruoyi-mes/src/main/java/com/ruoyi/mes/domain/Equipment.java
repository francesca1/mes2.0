package com.ruoyi.mes.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 设备信息对象 equipment
 * 
 * @author g4
 * @date 2023-04-17
 */
public class Equipment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 设备 */
    private Long id;

    /** 设备名称 */
    @Excel(name = "设备名称")
    private String equName;

    /** 设备编码 */
    @Excel(name = "设备编码")
    private String equCode;

    /** 设备类型 */
    @Excel(name = "设备类型")
    private String equType;

    /** 设备状态 */
    @Excel(name = "设备状态")
    private String equStatus;

    /** 工序id */
    @Excel(name = "工序id")
    private Long processId;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setEquName(String equName) 
    {
        this.equName = equName;
    }

    public String getEquName() 
    {
        return equName;
    }
    public void setEquCode(String equCode) 
    {
        this.equCode = equCode;
    }

    public String getEquCode() 
    {
        return equCode;
    }
    public void setEquType(String equType) 
    {
        this.equType = equType;
    }

    public String getEquType() 
    {
        return equType;
    }
    public void setEquStatus(String equStatus) 
    {
        this.equStatus = equStatus;
    }

    public String getEquStatus() 
    {
        return equStatus;
    }
    public void setProcessId(Long processId) 
    {
        this.processId = processId;
    }

    public Long getProcessId() 
    {
        return processId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("equName", getEquName())
            .append("equCode", getEquCode())
            .append("equType", getEquType())
            .append("equStatus", getEquStatus())
            .append("processId", getProcessId())
            .append("note", getNote())
            .toString();
    }
}
