package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.Material;

/**
 * 物料信息Mapper接口
 * 
 * @author g4
 * @date 2023-04-17
 */
public interface MaterialMapper 
{
    /**
     * 查询物料信息
     * 
     * @param id 物料信息主键
     * @return 物料信息
     */
    public Material selectMaterialById(Long id);

    /**
     * 查询物料信息列表
     * 
     * @param material 物料信息
     * @return 物料信息集合
     */
    public List<Material> selectMaterialList(Material material);

    /**
     * 新增物料信息
     * 
     * @param material 物料信息
     * @return 结果
     */
    public int insertMaterial(Material material);

    /**
     * 修改物料信息
     * 
     * @param material 物料信息
     * @return 结果
     */
    public int updateMaterial(Material material);

    /**
     * 删除物料信息
     * 
     * @param id 物料信息主键
     * @return 结果
     */
    public int deleteMaterialById(Long id);

    /**
     * 批量删除物料信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMaterialByIds(Long[] ids);


    Material selectMaterialByName(String materialName);

    List<Material> selectMaterialByMatName(String materialName);


    List<Material> selectMaterialByProcessName(String proccessName);

    List<Material> selectMaterialListByCode(String materialCode);

}
