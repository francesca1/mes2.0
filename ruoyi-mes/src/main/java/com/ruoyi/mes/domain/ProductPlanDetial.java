package com.ruoyi.mes.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 生产计划详情对象 product_plan_detial
 * 
 * @author g4
 * @date 2023-05-15
 */
@Data
public class ProductPlanDetial extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 生产计划明细id */
    private Long id;

    /** 生产计划编码 */
    @Excel(name = "生产计划编码")
    private String proplanCode;

    /** 计划开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planStartTime;

    /** 计划截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划截止时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planOverTime;

    /** 下达日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "下达日期")
    private Date issueDate;

    /** 下达数量 */
    @Excel(name = "下达数量")
    private Long issueNum;

    /** 计划下达状态 */
    @Excel(name = "计划下达状态")
    private String issueStatus;

    /** 计划数量 */
    @Excel(name = "计划数量")
    private Long planNum;

    /** 已下达数量 */
    @Excel(name = "已下达数量")
    private Long alreadyIssueNum;

    /** 还需下达数量 */
    @Excel(name = "还需下达数量")
    private Long needIssueNum;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public void setPlanStartTime(Date planStartTime) 
    {
        this.planStartTime = planStartTime;
    }

    public String getProplanCode() {
        return proplanCode;
    }

    public void setProplanCode(String proplanCode) {
        this.proplanCode = proplanCode;
    }

    public Date getPlanStartTime()
    {
        return planStartTime;
    }
    public void setPlanOverTime(Date planOverTime) 
    {
        this.planOverTime = planOverTime;
    }

    public Date getPlanOverTime() 
    {
        return planOverTime;
    }

    public Long getIssueNum() {
        return issueNum;
    }

    public void setIssueNum(Long issueNum) {
        this.issueNum = issueNum;
    }

    public void setIssueStatus(String issueStatus)
    {
        this.issueStatus = issueStatus;
    }

    public String getIssueStatus() 
    {
        return issueStatus;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Long getPlanNum() {
        return planNum;
    }

    public void setPlanNum(Long planNum) {
        this.planNum = planNum;
    }

    public Long getAlreadyIssueNum() {
        return alreadyIssueNum;
    }

    public void setAlreadyIssueNum(Long alreadyIssueNum) {
        this.alreadyIssueNum = alreadyIssueNum;
    }

    public Long getNeedIssueNum() {
        return needIssueNum;
    }

    public void setNeedIssueNum(Long needIssueNum) {
        this.needIssueNum = needIssueNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("proplanCode", getProplanCode())
            .append("planStartTime", getPlanStartTime())
            .append("planOverTime", getPlanOverTime())
            .append("issueDate", getIssueDate())
            .append("issueNum", getIssueNum())
            .append("issueStatus", getIssueStatus())
            .append("planNum", getPlanNum())
            .append("alreadyIssueNum", getAlreadyIssueNum())
            .append("needIssueNum", getNeedIssueNum())
            .toString();
    }
}
