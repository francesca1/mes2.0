package com.ruoyi.mes.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 生产领料对象 picking
 * 
 * @author g4
 * @date 2023-05-23
 */
public class Picking extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 领料id */
    private Long id;

    /** 领料编码 */
    @Excel(name = "领料编码")
    private String pickCode;

    /** 车间任务编码 */
    @Excel(name = "车间任务编码")
    private String workTaskCode;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    /** 领料状态 */
    @Excel(name = "领料状态")
    private String pickingStatus;

    /** 需求数量 */
    @Excel(name = "需求数量")
    private Long needNum;

    /** 已领数量 */
    @Excel(name = "已领数量")
    private Long alreadyNum;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPickCode(String pickCode) 
    {
        this.pickCode = pickCode;
    }

    public String getPickCode() 
    {
        return pickCode;
    }
    public void setWorkTaskCode(String workTaskCode) 
    {
        this.workTaskCode = workTaskCode;
    }

    public String getWorkTaskCode() 
    {
        return workTaskCode;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public void setPickingStatus(String pickingStatus)
    {
        this.pickingStatus = pickingStatus;
    }

    public String getPickingStatus() 
    {
        return pickingStatus;
    }
    public void setNeedNum(Long needNum) 
    {
        this.needNum = needNum;
    }

    public Long getNeedNum() 
    {
        return needNum;
    }
    public void setAlreadyNum(Long alreadyNum) 
    {
        this.alreadyNum = alreadyNum;
    }

    public Long getAlreadyNum() 
    {
        return alreadyNum;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pickCode", getPickCode())
            .append("workTaskCode", getWorkTaskCode())
            .append("materialName", getMaterialName())
            .append("pickingStatus", getPickingStatus())
            .append("needNum", getNeedNum())
            .append("alreadyNum", getAlreadyNum())
            .append("note", getNote())
            .toString();
    }
}
