package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.InboundWork;
import com.ruoyi.mes.domain.Vo.InboundVo;

/**
 * 完工入库Service接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface IInboundWorkService 
{
    /**
     * 查询完工入库
     * 
     * @param id 完工入库主键
     * @return 完工入库
     */
    public InboundWork selectInboundWorkById(Long id);

    /**
     * 查询完工入库列表
     * 
     * @param inboundWork 完工入库
     * @return 完工入库集合
     */
    public List<InboundWork> selectInboundWorkList(InboundWork inboundWork);

    /**
     * 新增完工入库
     * 
     * @param inboundWork 完工入库
     * @return 结果
     */
    public int insertInboundWork(InboundWork inboundWork);

    /**
     * 修改完工入库
     * 
     * @param inboundWork 完工入库
     * @return 结果
     */
    public int updateInboundWork(InboundWork inboundWork);

    /**
     * 批量删除完工入库
     * 
     * @param ids 需要删除的完工入库主键集合
     * @return 结果
     */
    public int deleteInboundWorkByIds(Long[] ids);

    /**
     * 删除完工入库信息
     * 
     * @param id 完工入库主键
     * @return 结果
     */
    public int deleteInboundWorkById(Long id);

    /**
     * 确认完工入库
     */
    AjaxResult submitInboundByVo(InboundVo inboundVo);

    /**
     * 查看完工入库单
     */
    AjaxResult lookInboundByCode(String taskcardCode);

}
