package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.ProductPlanDetial;
import com.ruoyi.mes.domain.Vo.PlanIssueVo;

/**
 * 生产计划详情Service接口
 * 
 * @author g4
 * @date 2023-05-15
 */
public interface IProductPlanDetialService 
{
    /**
     * 查询生产计划详情
     * 
     * @param id 生产计划详情主键
     * @return 生产计划详情
     */
    public ProductPlanDetial selectProductPlanDetialById(Long id);

    /**
     * 查询生产计划详情列表
     * 
     * @param productPlanDetial 生产计划详情
     * @return 生产计划详情集合
     */
    public List<ProductPlanDetial> selectProductPlanDetialList(ProductPlanDetial productPlanDetial);

    /**
     * 新增生产计划详情
     * 
     * @param productPlanDetial 生产计划详情
     * @return 结果
     */
    public int insertProductPlanDetial(ProductPlanDetial productPlanDetial);

    /**
     * 修改生产计划详情
     * 
     * @param productPlanDetial 生产计划详情
     * @return 结果
     */
    public int updateProductPlanDetial(ProductPlanDetial productPlanDetial);

    /**
     * 批量删除生产计划详情
     * 
     * @param ids 需要删除的生产计划详情主键集合
     * @return 结果
     */
    public int deleteProductPlanDetialByIds(Long[] ids);

    /**
     * 删除生产计划详情信息
     * 
     * @param id 生产计划详情主键
     * @return 结果
     */
    public int deleteProductPlanDetialById(Long id);


    /**
     * 计划下达
     *
     * @param planIssueVo
     * @return
     */
    AjaxResult insertplanIssue(PlanIssueVo planIssueVo);
    /**
     * 获取下达计划详情
     */
    AjaxResult selectgetIssueInfoByCode(String proplanCode);

}
