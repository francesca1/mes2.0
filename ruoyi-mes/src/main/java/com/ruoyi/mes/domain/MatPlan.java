package com.ruoyi.mes.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物料计划对象 mat_plan
 * 
 * @author g4
 * @date 2023-05-08
 */
@Data
public class MatPlan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 物料计划id */
    private Long id;

    /** 物料计划编码 */
    @Excel(name = "物料计划编码")
    private String matplanCode;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String ordCode;

    /** 订单名称 */
    @Excel(name = "订单名称")
    private String ordName;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 产品编码 */
    @Excel(name = "产品编码")
    private String productCode;

    /** 物料计划状态 */
    @Excel(name = "物料计划状态")
    private String matPlanStatus;

    /** 创建人 */
    @Excel(name = "创建人")
    private String userId;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 产品数量 */
    @TableField(exist = false)
    @Excel(name = "产品数量")
    private Long productNum;


    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMatplanCode(String matplanCode) 
    {
        this.matplanCode = matplanCode;
    }

    public String getMatplanCode() 
    {
        return matplanCode;
    }
    public void setOrdCode(String ordCode) 
    {
        this.ordCode = ordCode;
    }

    public String getOrdCode() 
    {
        return ordCode;
    }
    public void setOrdName(String ordName) 
    {
        this.ordName = ordName;
    }

    public String getOrdName() 
    {
        return ordName;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setProductCode(String productCode) 
    {
        this.productCode = productCode;
    }

    public String getProductCode() 
    {
        return productCode;
    }
    public void setMatPlanStatus(String matPlanStatus) 
    {
        this.matPlanStatus = matPlanStatus;
    }

    public String getMatPlanStatus() 
    {
        return matPlanStatus;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("matplanCode", getMatplanCode())
            .append("ordCode", getOrdCode())
            .append("ordName", getOrdName())
            .append("productName", getProductName())
            .append("productCode", getProductCode())
            .append("matPlanStatus", getMatPlanStatus())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("note", getNote())
            .toString();
    }
}
