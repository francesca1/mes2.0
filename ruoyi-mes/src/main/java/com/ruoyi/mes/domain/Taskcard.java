package com.ruoyi.mes.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 车间任务卡对象 taskcard
 * 
 * @author g4
 * @date 2023-04-11
 */
public class Taskcard extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 任务卡编号 */
    @Excel(name = "任务卡编号")
    private String taskcardCode;

    /** 车间任务编码 */
    @Excel(name = "车间任务编码")
    private String  workTaskCode;

    /** 任务进度 */
    @Excel(name = "任务进度")
    private float taskSchedule;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;


    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 工位 */
    @Excel(name = "工位")
    private String station;

    /** 计划生产数量 */
    @Excel(name = "计划生产数量")
    private Long planNum;

    /** 已生产数量 */
    @Excel(name = "已生产数量")
    private Long alreadyNum;

    /** 备注 */
    private String note;



    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTaskcardCode(String taskcardCode) 
    {
        this.taskcardCode = taskcardCode;
    }

    public String getTaskcardCode() 
    {
        return taskcardCode;
    }

    public String getWorkTaskCode() {
        return workTaskCode;
    }

    public void setWorkTaskCode(String workTaskCode) {
        this.workTaskCode = workTaskCode;
    }

    public float getTaskSchedule() {
        return taskSchedule;
    }

    public void setTaskSchedule(float taskSchedule) {
        this.taskSchedule = taskSchedule;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setStation(String station)
    {
        this.station = station;
    }

    public String getStation() 
    {
        return station;
    }
    public void setPlanNum(Long planNum) 
    {
        this.planNum = planNum;
    }

    public Long getPlanNum() 
    {
        return planNum;
    }
    public void setAlreadyNum(Long alreadyNum) 
    {
        this.alreadyNum = alreadyNum;
    }

    public Long getAlreadyNum() 
    {
        return alreadyNum;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("taskcardCode", getTaskcardCode())
            .append("workTaskCode", getWorkTaskCode())
            .append("taskSchedule", getTaskSchedule())
            .append("status", getStatus())
            .append("station", getStation())
            .append("planNum", getPlanNum())
            .append("alreadyNum", getAlreadyNum())
            .append("note", getNote())
            .toString();
    }
}
