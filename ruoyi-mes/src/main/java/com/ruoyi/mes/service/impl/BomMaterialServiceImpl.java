package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.mes.domain.Material;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.BomMaterialMapper;
import com.ruoyi.mes.domain.BomMaterial;
import com.ruoyi.mes.service.IBomMaterialService;

/**
 * bom物料Service业务层处理
 * 
 * @author g4
 * @date 2023-04-20
 */
@Service
public class BomMaterialServiceImpl implements IBomMaterialService 
{
    @Autowired
    private BomMaterialMapper bomMaterialMapper;

    /**
     * 查询bom物料
     * 
     * @param id bom物料主键
     * @return bom物料
     */
    @Override
    public BomMaterial selectBomMaterialById(Long id)
    {
        return bomMaterialMapper.selectBomMaterialById(id);
    }

    /**
     * 查询bom物料列表
     * 
     * @param bomMaterial bom物料
     * @return bom物料
     */
    @Override
    public List<BomMaterial> selectBomMaterialList(BomMaterial bomMaterial)
    {
        return bomMaterialMapper.selectBomMaterialList(bomMaterial);
    }

    /**
     * 新增bom物料
     * 
     * @param bomMaterial bom物料
     * @return 结果
     */
    @Override
    public int insertBomMaterial(BomMaterial bomMaterial)
    {
        return bomMaterialMapper.insertBomMaterial(bomMaterial);
    }

    /**
     * 修改bom物料
     * 
     * @param bomMaterial bom物料
     * @return 结果
     */
    @Override
    public int updateBomMaterial(BomMaterial bomMaterial)
    {
        return bomMaterialMapper.updateBomMaterial(bomMaterial);
    }

    /**
     * 批量删除bom物料
     * 
     * @param ids 需要删除的bom物料主键
     * @return 结果
     */
    @Override
    public int deleteBomMaterialByIds(Long[] ids)
    {
        return bomMaterialMapper.deleteBomMaterialByIds(ids);
    }

    /**
     * 删除bom物料信息
     * 
     * @param id bom物料主键
     * @return 结果
     */
    @Override
    public int deleteBomMaterialById(Long id)
    {
        return bomMaterialMapper.deleteBomMaterialById(id);
    }

    /**
     * 查 - Bom物料信息 - bomCode
     * @param bomCode
     * @return
     */
    @Override
    public List<BomMaterial> selectMaterialsByBomCode(String bomCode) {
        return bomMaterialMapper.selectMaterialsByBomCode(bomCode);
    }
}
