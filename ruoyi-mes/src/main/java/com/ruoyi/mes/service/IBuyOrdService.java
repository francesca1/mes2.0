package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.BuyOrd;
import com.ruoyi.mes.domain.Vo.BuyNeedOrdVo;

/**
 * 采购订单Service接口
 * 
 * @author g4
 * @date 2023-05-08
 */
public interface IBuyOrdService 
{
    /**
     * 查询采购订单
     * 
     * @param id 采购订单主键
     * @return 采购订单
     */
    public BuyOrd selectBuyOrdById(Long id);

    /**
     * 查询采购订单列表
     * 
     * @param buyOrd 采购订单
     * @return 采购订单集合
     */
    public List<BuyOrd> selectBuyOrdList(BuyOrd buyOrd);

    /**
     * 新增采购订单
     * 
     * @param buyOrd 采购订单
     * @return 结果
     */
    public int insertBuyOrd(BuyOrd buyOrd);

    /**
     * 修改采购订单
     * 
     * @param buyOrd 采购订单
     * @return 结果
     */
    public int updateBuyOrd(BuyOrd buyOrd);

    /**
     * 批量删除采购订单
     * 
     * @param ids 需要删除的采购订单主键集合
     * @return 结果
     */
    public int deleteBuyOrdByIds(Long[] ids);

    /**
     * 删除采购订单信息
     * 
     * @param id 采购订单主键
     * @return 结果
     */
    public int deleteBuyOrdById(Long id);

    /**
     * 新增采购订单-物料
     */
    AjaxResult insertAddBuyOrd(BuyNeedOrdVo buyNeedOrdVo);

    /**
     * 新增采购订单-all
     */
    AjaxResult insertAddBuyOrdAll(BuyNeedOrdVo buyNeedOrdVo);

    /**
     * 获取采购订单详细信息
     */
    AjaxResult selectBuyOrdByCode(String buyordCode);

    /**
     * 确认到货
     */
    AjaxResult insertconfirmArrival(BuyOrd buyOrd);

}
