package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.mes.domain.Vo.ProMatVo;
import com.ruoyi.mes.service.IProcessMaterialService;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.Process;
import com.ruoyi.mes.service.IProcessService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 工序管理Controller
 * 
 * @author g4
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/process")
public class ProcessController extends BaseController
{
    @Autowired
    private IProcessService processService;

    @Autowired
    private IProcessMaterialService processMaterialService;

    /**
     * 查询工序管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:process:list')")
    @GetMapping("/list")
    public TableDataInfo list(Process process)
    {
        startPage();
        List<Process> list = processService.selectProcessList(process);
        return getDataTable(list);
    }


    /**
     * 导出工序管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:process:export')")
    @Log(title = "工序管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Process process)
    {
        List<Process> list = processService.selectProcessList(process);
        ExcelUtil<Process> util = new ExcelUtil<Process>(Process.class);
        util.exportExcel(response, list, "工序管理数据");
    }

    /**
     * 获取工序管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:process:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(processService.selectProcessById(id));
    }

    /**
     * 新增工序管理
     */
    @PreAuthorize("@ss.hasPermi('mes:process:add')")
    @Log(title = "工序管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Process process)
    {
        if(UserConstants.PRONOT_UNIQUE.equals(processService.checkProcessCodeUnique(process))){
            return AjaxResult.error("工序编码已存在");
        }
        if(UserConstants.PRONOT_UNIQUE.equals(processService.checkProcessNameUnique(process))){
            return AjaxResult.error("工序名称已存在");
        }

        return processService.insertProcess(process);
    }

    /**
     * 修改工序管理
     */
    @PreAuthorize("@ss.hasPermi('mes:process:edit')")
    @Log(title = "工序管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Process process)
    {
        return toAjax(processService.updateProcess(process));
    }

    /**
     * 删除工序管理
     */
    @PreAuthorize("@ss.hasPermi('mes:process:remove')")
    @Log(title = "工序管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(processService.deleteProcessByIds(ids));
    }


    /**
     * 查 - 工艺详情
     * @param craftId
     * @return
     */

    @GetMapping("/getCraftDetails/{craftId}")
    public AjaxResult getCraftDetails(@PathVariable("craftId") Long craftId)
    {
        return processService.getCraftDetails(craftId);
    }

    /**
     * 查工序物料
     * @param proMatVo
     * @return
     */
    @PostMapping ("/getMatInfo")
    public AjaxResult getMatInfo(@RequestBody ProMatVo proMatVo)
    {
        return processService.getMatInfo(proMatVo);
    }
}
