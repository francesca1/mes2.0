package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.MatPlan;

/**
 * 物料计划Mapper接口
 * 
 * @author g4
 * @date 2023-05-08
 */
public interface MatPlanMapper 
{
    /**
     * 查询物料计划
     * 
     * @param id 物料计划主键
     * @return 物料计划
     */
    public MatPlan selectMatPlanById(Long id);

    /**
     * 查询物料计划列表
     * 
     * @param matPlan 物料计划
     * @return 物料计划集合
     */
    public List<MatPlan> selectMatPlanList(MatPlan matPlan);

    /**
     * 新增物料计划
     * 
     * @param matPlan 物料计划
     * @return 结果
     */
    public int insertMatPlan(MatPlan matPlan);

    /**
     * 修改物料计划
     * 
     * @param matPlan 物料计划
     * @return 结果
     */
    public int updateMatPlan(MatPlan matPlan);

    /**
     * 删除物料计划
     * 
     * @param id 物料计划主键
     * @return 结果
     */
    public int deleteMatPlanById(Long id);

    /**
     * 批量删除物料计划
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMatPlanByIds(Long[] ids);

    MatPlan selectMatPlanByCode(String matplanCode);

//    MatPlan selectMatPlanByCode(String matplanCode);

}
