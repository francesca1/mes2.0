package com.ruoyi.mes.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 工艺管理对象 craft
 * 
 * @author g4
 * @date 2023-04-18
 */
@Data
public class Craft extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 工艺名称 */
    @Excel(name = "工艺名称")
    private String craftName;

    /** 工艺编码 */
    @Excel(name = "工艺编码")
    private String craftCode;

    /** 产品id */
    @Excel(name = "产品id")
    private Long productId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 创建人 */
    @Excel(name = "创建人")
    private String userId;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    private List<Process> processes;

    public List<Process> getProcesses() {
        return processes;
    }

    public void setProcesses(List<Process> processes) {
        this.processes = processes;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCraftName(String craftName) 
    {
        this.craftName = craftName;
    }

    public String getCraftName() 
    {
        return craftName;
    }
    public void setCraftCode(String craftCode) 
    {
        this.craftCode = craftCode;
    }

    public String getCraftCode() 
    {
        return craftCode;
    }
    public void setProductId(Long productId) 
    {
        this.productId = productId;
    }

    public Long getProductId() 
    {
        return productId;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("craftName", getCraftName())
            .append("craftCode", getCraftCode())
            .append("productId", getProductId())
            .append("productName", getProductName())
            .append("status", getStatus())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("note", getNote())
            .toString();
    }
}
