package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.BuyNeed;

/**
 * 采购需求单Service接口
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
public interface IBuyNeedService 
{
    /**
     * 查询采购需求单
     * 
     * @param id 采购需求单主键
     * @return 采购需求单
     */
    public BuyNeed selectBuyNeedById(Long id);

    /**
     * 查询采购需求单列表
     * 
     * @param buyNeed 采购需求单
     * @return 采购需求单集合
     */
    public List<BuyNeed> selectBuyNeedList(BuyNeed buyNeed);

    /**
     * 新增采购需求单
     * 
     * @param buyNeed 采购需求单
     * @return 结果
     */
    public int insertBuyNeed(BuyNeed buyNeed);

    /**
     * 修改采购需求单
     * 
     * @param buyNeed 采购需求单
     * @return 结果
     */
    public int updateBuyNeed(BuyNeed buyNeed);

    /**
     * 批量删除采购需求单
     * 
     * @param ids 需要删除的采购需求单主键集合
     * @return 结果
     */
    public int deleteBuyNeedByIds(Long[] ids);

    /**
     * 删除采购需求单信息
     * 
     * @param id 采购需求单主键
     * @return 结果
     */
    public int deleteBuyNeedById(Long id);

    /**
     * 获取生成采购订单-物料信息
     */
    AjaxResult selectBuyNeedByMtCode(String buyneedCode);
}
