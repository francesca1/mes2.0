package com.ruoyi.mes.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 物料信息对象 material
 * 
 * @author g4
 * @date 2023-04-17
 */
@Data
public class Material extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 物料编号 */
    @Excel(name = "物料编号")
    private String materialCode;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    /** 物料类型 */
    @Excel(name = "物料类型")
    private String materialType;

    /** 物料数量 */
    @Excel(name = "物料数量")
    private Long materialNum;

    /** 供应商 */
    @Excel(name = "供应商")
    private String provider;

    /** 采购订单id */
    @Excel(name = "采购订单id")
    private Long buyOrdId;

    /** 仓库 */
    @Excel(name = "仓库")
    private Long depotId;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 所需物料数量 */
    @TableField(exist = false)
    private Long proMatNum;

    /** 采购物料数量 */
    @TableField(exist = false)
    @Excel(name = "采购物料数量")
    private Long buyNum;

    /** 到货日期 */
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到货日期")
    private Date alreadyDate;


    public Long getProMatNum() {
        return proMatNum;
    }

    public void setProMatNum(Long proMatNum) {
        this.proMatNum = proMatNum;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMaterialCode(String materialCode) 
    {
        this.materialCode = materialCode;
    }

    public String getMaterialCode() 
    {
        return materialCode;
    }
    public void setMaterialName(String materialName) 
    {
        this.materialName = materialName;
    }

    public String getMaterialName() 
    {
        return materialName;
    }
    public void setMaterialType(String materialType) 
    {
        this.materialType = materialType;
    }

    public String getMaterialType() 
    {
        return materialType;
    }
    public void setMaterialNum(Long materialNum) 
    {
        this.materialNum = materialNum;
    }

    public Long getMaterialNum() 
    {
        return materialNum;
    }
    public void setProvider(String provider) 
    {
        this.provider = provider;
    }

    public String getProvider() 
    {
        return provider;
    }
    public void setBuyOrdId(Long buyOrdId) 
    {
        this.buyOrdId = buyOrdId;
    }

    public Long getBuyOrdId() 
    {
        return buyOrdId;
    }
    public void setDepotId(Long depotId) 
    {
        this.depotId = depotId;
    }

    public Long getDepotId() 
    {
        return depotId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("materialCode", getMaterialCode())
            .append("materialName", getMaterialName())
            .append("materialType", getMaterialType())
            .append("materialNum", getMaterialNum())
            .append("provider", getProvider())
            .append("buyOrdId", getBuyOrdId())
            .append("depotId", getDepotId())
            .append("note", getNote())
            .toString();
    }
}
