package com.ruoyi.mes.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库位管理对象 bin_location
 * 
 * @author g4
 * @date 2023-04-11
 */
@Data
public class BinLocation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 库位id */
    private Integer id;

    /** 库位编号 */
    @Excel(name = "库位")
    private String binLocation;

    /** 仓库id */
    @Excel(name = "仓库名称")
    private String depotName;

    /** 负责人 */
    @Excel(name = "负责人")
    private String headman;

    /** 备注 */
    private String note;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }

    public String getBinLocation() {
        return binLocation;
    }

    public void setBinLocation(String binLocation) {
        this.binLocation = binLocation;
    }

    public String getDepotName() {
        return depotName;
    }

    public void setDepotName(String depotName) {
        this.depotName = depotName;
    }

    public void setHeadman(String headman)
    {
        this.headman = headman;
    }

    public String getHeadman() 
    {
        return headman;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("binLocation", getBinLocation())
            .append("depotName", getDepotName())
            .append("headman", getHeadman())
            .append("note", getNote())
            .toString();
    }
}
