package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.BinLocation;
import com.ruoyi.mes.domain.Vo.BinDepotVo;

/**
 * 库位管理Service接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface IBinLocationService 
{
    /**
     * 查询库位管理
     * 
     * @param id 库位管理主键
     * @return 库位管理
     */
    public BinLocation selectBinLocationById(Integer id);

    /**
     * 查询库位管理列表
     * 
     * @param binLocation 库位管理
     * @return 库位管理集合
     */
    public List<BinLocation> selectBinLocationList(BinLocation binLocation);

    /**
     * 新增库位管理
     * 
     * @param binLocation 库位管理
     * @return 结果
     */
    public int insertBinLocation(BinLocation binLocation);

    /**
     * 修改库位管理
     * 
     * @param binLocation 库位管理
     * @return 结果
     */
    public int updateBinLocation(BinLocation binLocation);

    /**
     * 批量删除库位管理
     * 
     * @param ids 需要删除的库位管理主键集合
     * @return 结果
     */
    public int deleteBinLocationByIds(Integer[] ids);

    /**
     * 删除库位管理信息
     * 
     * @param id 库位管理主键
     * @return 结果
     */
    public int deleteBinLocationById(Integer id);

    /**
     * 新增库位
     */
    AjaxResult insertaddLocation(BinDepotVo binDepotVo);

}
