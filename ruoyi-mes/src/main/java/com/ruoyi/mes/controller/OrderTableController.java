package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.mes.domain.Vo.OrdproVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.OrderTable;
import com.ruoyi.mes.service.IOrderTableService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 销售订单Controller
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/order")
public class OrderTableController extends BaseController
{
    @Autowired
    private IOrderTableService orderTableService;

    /**
     * 查询销售订单列表
     */
    @PreAuthorize("@ss.hasPermi('mes:order:list')")
    @GetMapping("/list")
    public TableDataInfo list(OrderTable orderTable)
    {
        startPage();
        List<OrderTable> list = orderTableService.selectOrderTableList(orderTable);
        return getDataTable(list);
    }

    /**
     * 导出销售订单列表
     */
    @PreAuthorize("@ss.hasPermi('mes:order:export')")
    @Log(title = "销售订单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OrderTable orderTable)
    {
        List<OrderTable> list = orderTableService.selectOrderTableList(orderTable);
        ExcelUtil<OrderTable> util = new ExcelUtil<OrderTable>(OrderTable.class);
        util.exportExcel(response, list, "销售订单数据");
    }

    /**
     * 获取销售订单详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:order:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(orderTableService.selectOrderTableById(id));
    }

    /**
     * 新增销售订单
     */
    @PreAuthorize("@ss.hasPermi('mes:order:add')")
    @Log(title = "销售订单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OrderTable orderTable)
    {
        return toAjax(orderTableService.insertOrderTable(orderTable));
    }

    /**
     * 修改销售订单
     */
    @PreAuthorize("@ss.hasPermi('mes:order:edit')")
    @Log(title = "销售订单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OrderTable orderTable)
    {
        return toAjax(orderTableService.updateOrderTable(orderTable));
    }

    /**
     * 删除销售订单
     */
    @PreAuthorize("@ss.hasPermi('mes:order:remove')")
    @Log(title = "销售订单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(orderTableService.deleteOrderTableByIds(ids));
    }

    /**
     * 新增产品销售订单
     */
    @PostMapping("/addOrder")
    public AjaxResult addOrder(@RequestBody OrdproVo ordproVo)
    {
        return orderTableService.insertaddOrderTable(ordproVo);
    }

    /**
     * 查看订单详细信息-产品
     */
    @PreAuthorize("@ss.hasPermi('mes:order:query')")
    @GetMapping( "/getdetial/{ordCode}")
    public AjaxResult getdetialInfo(@PathVariable("ordCode") String ordCode)
    {
        return orderTableService.selectOrderTableByCode(ordCode);
    }
}
