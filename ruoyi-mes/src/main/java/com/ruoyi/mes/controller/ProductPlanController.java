package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.mes.domain.Vo.PlanIssueVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.ProductPlan;
import com.ruoyi.mes.service.IProductPlanService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 生产计划Controller
 * 
 * @author g4
 * @date 2023-05-08
 */
@RestController
@RequestMapping("/mes/Proplan")
public class ProductPlanController extends BaseController
{
    @Autowired
    private IProductPlanService productPlanService;

    /**
     * 查询生产计划列表
     */
    @PreAuthorize("@ss.hasPermi('mes:Proplan:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductPlan productPlan)
    {
        startPage();
        List<ProductPlan> list = productPlanService.selectProductPlanList(productPlan);
        return getDataTable(list);
    }

    /**
     * 导出生产计划列表
     */
    @PreAuthorize("@ss.hasPermi('mes:Proplan:export')")
    @Log(title = "生产计划", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductPlan productPlan)
    {
        List<ProductPlan> list = productPlanService.selectProductPlanList(productPlan);
        ExcelUtil<ProductPlan> util = new ExcelUtil<ProductPlan>(ProductPlan.class);
        util.exportExcel(response, list, "生产计划数据");
    }

    /**
     * 获取生产计划详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:Proplan:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(productPlanService.selectProductPlanById(id));
    }

    /**
     * 新增生产计划
     */
    @PreAuthorize("@ss.hasPermi('mes:Proplan:add')")
    @Log(title = "生产计划", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductPlan productPlan)
    {
        return toAjax(productPlanService.insertProductPlan(productPlan));
    }

    /**
     * 修改生产计划
     */
    @PreAuthorize("@ss.hasPermi('mes:Proplan:edit')")
    @Log(title = "生产计划", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductPlan productPlan)
    {
        return toAjax(productPlanService.updateProductPlan(productPlan));
    }

    /**
     * 删除生产计划
     */
    @PreAuthorize("@ss.hasPermi('mes:Proplan:remove')")
    @Log(title = "生产计划", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(productPlanService.deleteProductPlanByIds(ids));
    }

    /**
     * 获取生产计划明细
     */
    @GetMapping("/getProPlanDetInfo/{proplanCode}")
    public AjaxResult getInfo(@PathVariable("proplanCode") String proplanCode)
    {
        return success(productPlanService.selectProductPlanByproplanCode(proplanCode));
    }



}
