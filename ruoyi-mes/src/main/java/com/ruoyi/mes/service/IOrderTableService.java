package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.OrderTable;
import com.ruoyi.mes.domain.Vo.OrdproVo;

/**
 * 销售订单Service接口
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
public interface IOrderTableService 
{
    /**
     * 查询销售订单
     * 
     * @param id 销售订单主键
     * @return 销售订单
     */
    public OrderTable selectOrderTableById(Long id);

    /**
     * 查询销售订单列表
     * 
     * @param orderTable 销售订单
     * @return 销售订单集合
     */
    public List<OrderTable> selectOrderTableList(OrderTable orderTable);

    /**
     * 新增销售订单
     * 
     * @param orderTable 销售订单
     * @return 结果
     */
    public int insertOrderTable(OrderTable orderTable);

    /**
     * 修改销售订单
     * 
     * @param orderTable 销售订单
     * @return 结果
     */
    public int updateOrderTable(OrderTable orderTable);

    /**
     * 批量删除销售订单
     * 
     * @param ids 需要删除的销售订单主键集合
     * @return 结果
     */
    public int deleteOrderTableByIds(Long[] ids);

    /**
     * 删除销售订单信息
     * 
     * @param id 销售订单主键
     * @return 结果
     */
    public int deleteOrderTableById(Long id);

    /**
     * 新增产品销售订单
     */
    AjaxResult insertaddOrderTable(OrdproVo ordproVo);

    /**
     * 查看订单详细信息-产品
     */
    AjaxResult selectOrderTableByCode(String ordCode);
}



