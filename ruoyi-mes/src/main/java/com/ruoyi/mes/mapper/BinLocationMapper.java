package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.BinLocation;

/**
 * 库位管理Mapper接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface BinLocationMapper 
{
    /**
     * 查询库位管理
     * 
     * @param id 库位管理主键
     * @return 库位管理
     */
    public BinLocation selectBinLocationById(Integer id);

    /**
     * 查询库位管理列表
     * 
     * @param binLocation 库位管理
     * @return 库位管理集合
     */
    public List<BinLocation> selectBinLocationList(BinLocation binLocation);

    /**
     * 新增库位管理
     * 
     * @param binLocation 库位管理
     * @return 结果
     */
    public int insertBinLocation(BinLocation binLocation);

    /**
     * 修改库位管理
     * 
     * @param binLocation 库位管理
     * @return 结果
     */
    public int updateBinLocation(BinLocation binLocation);

    /**
     * 删除库位管理
     * 
     * @param id 库位管理主键
     * @return 结果
     */
    public int deleteBinLocationById(Integer id);

    /**
     * 批量删除库位管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBinLocationByIds(Integer[] ids);
}
