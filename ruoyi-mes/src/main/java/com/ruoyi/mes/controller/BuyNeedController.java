package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.BuyNeed;
import com.ruoyi.mes.service.IBuyNeedService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 采购需求单Controller
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/needcard")
public class BuyNeedController extends BaseController
{
    @Autowired
    private IBuyNeedService buyNeedService;

    /**
     * 查询采购需求单列表
     */
    @PreAuthorize("@ss.hasPermi('mes:needcard:list')")
    @GetMapping("/list")
    public TableDataInfo list(BuyNeed buyNeed)
    {
        startPage();
        List<BuyNeed> list = buyNeedService.selectBuyNeedList(buyNeed);
        return getDataTable(list);
    }

    /**
     * 导出采购需求单列表
     */
    @PreAuthorize("@ss.hasPermi('mes:needcard:export')")
    @Log(title = "采购需求单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BuyNeed buyNeed)
    {
        List<BuyNeed> list = buyNeedService.selectBuyNeedList(buyNeed);
        ExcelUtil<BuyNeed> util = new ExcelUtil<BuyNeed>(BuyNeed.class);
        util.exportExcel(response, list, "采购需求单数据");
    }

    /**
     * 获取采购需求单详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:needcard:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(buyNeedService.selectBuyNeedById(id));
    }

    /**
     * 新增采购需求单
     */
    @PreAuthorize("@ss.hasPermi('mes:needcard:add')")
    @Log(title = "采购需求单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BuyNeed buyNeed)
    {
        return toAjax(buyNeedService.insertBuyNeed(buyNeed));
    }

    /**
     * 修改采购需求单
     */
    @PreAuthorize("@ss.hasPermi('mes:needcard:edit')")
    @Log(title = "采购需求单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BuyNeed buyNeed)
    {
        return toAjax(buyNeedService.updateBuyNeed(buyNeed));
    }

    /**
     * 删除采购需求单
     */
    @PreAuthorize("@ss.hasPermi('mes:needcard:remove')")
    @Log(title = "采购需求单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(buyNeedService.deleteBuyNeedByIds(ids));
    }

    /**
     * 获取生成采购订单-物料信息
     */
    @GetMapping(value = "getMatInfo/{buyneedCode}")
    public AjaxResult getMatInfo(@PathVariable("buyneedCode") String buyneedCode)
    {
        return buyNeedService.selectBuyNeedByMtCode(buyneedCode);
    }
}
