package com.ruoyi.mes.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 车间任务对象 work_task
 * 
 * @author g4
 * @date 2023-05-22
 */
@Data
public class WorkTask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 车间任务id */
    private Long id;

    /** 车间任务编号 */
    @Excel(name = "车间任务编号")
    private String workTaskCode;

    /** 车间任务名称 */
    @Excel(name = "车间任务名称")
    private String workTaskName;

    /** 生产计划编码 */
    @Excel(name = "生产计划编码")
    private String proplanCode;

    /** 生产进度 */
    @Excel(name = "生产进度")
    private float proSchedule;

    /** 生产部门 */
    @Excel(name = "生产部门")
    private String proDepart;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 产品编码 */
    @TableField(exist = false)
    @Excel(name = "产品编码")
    private String productCode;

    /** 领料id */
    private Long pickingId;

    /** 计划生产数量 */
    @Excel(name = "计划生产数量")
    private Long planNum;

    /** 已生产数量 */
    @Excel(name = "已生产数量")
    private Long alreadyNum;

    /** 计划开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planStartTime;

    /** 计划截止日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "计划截止日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date planOverTime;

    /** 任务状态 */
    @Excel(name = "任务状态")
    private String taskStatus;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 创建人 */
    @Excel(name = "创建人")
    private String userId;

    /** 备注 */
    private String note;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setWorkTaskCode(String workTaskCode) 
    {
        this.workTaskCode = workTaskCode;
    }

    public String getWorkTaskCode() 
    {
        return workTaskCode;
    }
    public void setWorkTaskName(String workTaskName) 
    {
        this.workTaskName = workTaskName;
    }

    public String getWorkTaskName() 
    {
        return workTaskName;
    }
    public void setProplanCode(String proplanCode) 
    {
        this.proplanCode = proplanCode;
    }

    public String getProplanCode() 
    {
        return proplanCode;
    }

    public float getProSchedule() {
        return proSchedule;
    }

    public void setProSchedule(float proSchedule) {
        this.proSchedule = proSchedule;
    }

    public void setProDepart(String proDepart)
    {
        this.proDepart = proDepart;
    }

    public String getProDepart() 
    {
        return proDepart;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setPickingId(Long pickingId) 
    {
        this.pickingId = pickingId;
    }

    public Long getPickingId() 
    {
        return pickingId;
    }
    public void setPlanNum(Long planNum) 
    {
        this.planNum = planNum;
    }

    public Long getPlanNum() 
    {
        return planNum;
    }
    public void setAlreadyNum(Long alreadyNum) 
    {
        this.alreadyNum = alreadyNum;
    }

    public Long getAlreadyNum() 
    {
        return alreadyNum;
    }
    public void setPlanStartTime(Date planStartTime) 
    {
        this.planStartTime = planStartTime;
    }

    public Date getPlanStartTime() 
    {
        return planStartTime;
    }
    public void setPlanOverTime(Date planOverTime) 
    {
        this.planOverTime = planOverTime;
    }

    public Date getPlanOverTime() 
    {
        return planOverTime;
    }
    public void setTaskStatus(String taskStatus) 
    {
        this.taskStatus = taskStatus;
    }

    public String getTaskStatus() 
    {
        return taskStatus;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("workTaskCode", getWorkTaskCode())
            .append("workTaskName", getWorkTaskName())
            .append("proplanCode", getProplanCode())
            .append("proSchedule", getProSchedule())
            .append("proDepart", getProDepart())
            .append("productName", getProductName())
            .append("pickingId", getPickingId())
            .append("planNum", getPlanNum())
            .append("alreadyNum", getAlreadyNum())
            .append("planStartTime", getPlanStartTime())
            .append("planOverTime", getPlanOverTime())
            .append("taskStatus", getTaskStatus())
            .append("startTime", getStartTime())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("note", getNote())
            .toString();
    }
}
