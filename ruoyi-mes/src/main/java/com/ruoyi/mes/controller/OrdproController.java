package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.Ordpro;
import com.ruoyi.mes.service.IOrdproService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单产品表Controller
 * 
 * @author ruoyi
 * @date 2023-05-05
 */
@RestController
@RequestMapping("/mes/ordpro")
public class OrdproController extends BaseController
{
    @Autowired
    private IOrdproService ordproService;

    /**
     * 查询订单产品表列表
     */
    @PreAuthorize("@ss.hasPermi('mes:ordpro:list')")
    @GetMapping("/list")
    public TableDataInfo list(Ordpro ordpro)
    {
        startPage();
        List<Ordpro> list = ordproService.selectOrdproList(ordpro);
        return getDataTable(list);
    }

    /**
     * 导出订单产品表列表
     */
    @PreAuthorize("@ss.hasPermi('mes:ordpro:export')")
    @Log(title = "订单产品表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Ordpro ordpro)
    {
        List<Ordpro> list = ordproService.selectOrdproList(ordpro);
        ExcelUtil<Ordpro> util = new ExcelUtil<Ordpro>(Ordpro.class);
        util.exportExcel(response, list, "订单产品表数据");
    }

    /**
     * 获取订单产品表详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:ordpro:query')")
    @GetMapping(value = "/{ordCode}")
    public AjaxResult getInfo(@PathVariable("ordCode") String ordCode)
    {
        return success(ordproService.selectOrdproByOrdCode(ordCode));
    }

    /**
     * 新增订单产品表
     */
    @PreAuthorize("@ss.hasPermi('mes:ordpro:add')")
    @Log(title = "订单产品表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Ordpro ordpro)
    {
        return toAjax(ordproService.insertOrdpro(ordpro));
    }

    /**
     * 修改订单产品表
     */
    @PreAuthorize("@ss.hasPermi('mes:ordpro:edit')")
    @Log(title = "订单产品表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Ordpro ordpro)
    {
        return toAjax(ordproService.updateOrdpro(ordpro));
    }

    /**
     * 删除订单产品表
     */
    @PreAuthorize("@ss.hasPermi('mes:ordpro:remove')")
    @Log(title = "订单产品表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ordCodes}")
    public AjaxResult remove(@PathVariable String[] ordCodes)
    {
        return toAjax(ordproService.deleteOrdproByOrdCodes(ordCodes));
    }
}
