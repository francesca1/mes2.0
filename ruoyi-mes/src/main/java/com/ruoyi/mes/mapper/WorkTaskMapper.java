package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.WorkTask;

/**
 * 车间任务Mapper接口
 * 
 * @author g4
 * @date 2023-05-22
 */
public interface WorkTaskMapper 
{
    /**
     * 查询车间任务
     * 
     * @param id 车间任务主键
     * @return 车间任务
     */
    public WorkTask selectWorkTaskById(Long id);

    /**
     * 查询车间任务列表
     * 
     * @param workTask 车间任务
     * @return 车间任务集合
     */
    public List<WorkTask> selectWorkTaskList(WorkTask workTask);

    /**
     * 新增车间任务
     * 
     * @param workTask 车间任务
     * @return 结果
     */
    public int insertWorkTask(WorkTask workTask);

    /**
     * 修改车间任务
     * 
     * @param workTask 车间任务
     * @return 结果
     */
    public int updateWorkTask(WorkTask workTask);

    /**
     * 删除车间任务
     * 
     * @param id 车间任务主键
     * @return 结果
     */
    public int deleteWorkTaskById(Long id);

    /**
     * 批量删除车间任务
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWorkTaskByIds(Long[] ids);

    WorkTask selectWorkTaskByCode(String workTaskCode);

}
