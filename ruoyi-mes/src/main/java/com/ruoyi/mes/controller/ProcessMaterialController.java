package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.weaver.loadtime.Aj;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.ProcessMaterial;
import com.ruoyi.mes.service.IProcessMaterialService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 工序物料Controller
 * 
 * @author g4
 * @date 2023-04-20
 */
@RestController
@RequestMapping("/mes/processmat")
public class ProcessMaterialController extends BaseController
{
    @Autowired
    private IProcessMaterialService processMaterialService;

    /**
     * 查询工序物料列表
     */
    @PreAuthorize("@ss.hasPermi('mes:processmat:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProcessMaterial processMaterial)
    {
        startPage();
        List<ProcessMaterial> list = processMaterialService.selectProcessMaterialList(processMaterial);
        return getDataTable(list);
    }

    /**
     * 导出工序物料列表
     */
    @PreAuthorize("@ss.hasPermi('mes:processmat:export')")
    @Log(title = "工序物料", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProcessMaterial processMaterial)
    {
        List<ProcessMaterial> list = processMaterialService.selectProcessMaterialList(processMaterial);
        ExcelUtil<ProcessMaterial> util = new ExcelUtil<ProcessMaterial>(ProcessMaterial.class);
        util.exportExcel(response, list, "工序物料数据");
    }

    /**
     * 获取工序物料详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:processmat:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(processMaterialService.selectProcessMaterialById(id));
    }

    /**
     * 新增工序物料
     */
    @PreAuthorize("@ss.hasPermi('mes:processmat:add')")
    @Log(title = "工序物料", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProcessMaterial processMaterial)
    {
        return toAjax(processMaterialService.insertProcessMaterial(processMaterial));
    }

    /**
     * 修改工序物料
     */
    @PreAuthorize("@ss.hasPermi('mes:processmat:edit')")
    @Log(title = "工序物料", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProcessMaterial processMaterial)
    {
        return toAjax(processMaterialService.updateProcessMaterial(processMaterial));
    }

    /**
     * 删除工序物料
     */
    @PreAuthorize("@ss.hasPermi('mes:processmat:remove')")
    @Log(title = "工序物料", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(processMaterialService.deleteProcessMaterialByIds(ids));
    }

    /**
     * 查 - 工序对应物料和设备
     * @param processName
     * @return
     */
    @PreAuthorize("@ss.hasPermi('mes:processmat:list')")
    @GetMapping("getProMatInfoList/{processName}")
    public AjaxResult getProMatInfoList(@PathVariable("processName") String processName)
    {
        return processMaterialService.selectProcessMaterialByName(processName);
    }
}
