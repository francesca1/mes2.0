package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.mes.domain.*;
import com.ruoyi.mes.domain.Vo.PickVo;
import com.ruoyi.mes.domain.Vo.WorkTaskVo;
import com.ruoyi.mes.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.service.IWorkTaskService;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 车间任务Service业务层处理
 * 
 * @author g4
 * @date 2023-05-22
 */
@Service
public class WorkTaskServiceImpl implements IWorkTaskService 
{
    @Value("${ruoyi.taskCodeLast}")
    private int taskCodeLast;
    @Autowired
    private WorkTaskMapper workTaskMapper;

    @Autowired
    private TaskcardMapper taskcardMapper;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private CraftMapper craftMapper;

    @Autowired
    private ProcessMaterialMapper processMaterialMapper;

    @Autowired
    private PickingMapper pickingMapper;

    @Autowired
    private MaterialMapper materialMapper;

    /**
     * 查询车间任务
     * 
     * @param id 车间任务主键
     * @return 车间任务
     */
    @Override
    public WorkTask selectWorkTaskById(Long id)
    {
        return workTaskMapper.selectWorkTaskById(id);
    }

    /**
     * 查询车间任务列表
     * 
     * @param workTask 车间任务
     * @return 车间任务
     */
    @Override
    public List<WorkTask> selectWorkTaskList(WorkTask workTask)
    {
        List<WorkTask> workTasks = workTaskMapper.selectWorkTaskList(workTask);
        if(workTasks.size()!=0){
            for (WorkTask task : workTasks) {
                if(task.getAlreadyNum()== task.getPlanNum()){
                    task.setTaskStatus("已完成");
                    workTaskMapper.updateWorkTask(task);
                }
                task.setProSchedule((float)task.getAlreadyNum()/ task.getPlanNum()*100);
                workTaskMapper.updateWorkTask(task);
            }
        }
        return workTasks;
    }

    /**
     * 新增车间任务
     * 
     * @param workTask 车间任务
     * @return 结果
     */
    @Override
    public int insertWorkTask(WorkTask workTask)
    {
        workTask.setCreateTime(DateUtils.getNowDate());
        return workTaskMapper.insertWorkTask(workTask);
    }

    /**
     * 修改车间任务
     * 
     * @param workTask 车间任务
     * @return 结果
     */
    @Override
    public int updateWorkTask(WorkTask workTask)
    {
        return workTaskMapper.updateWorkTask(workTask);
    }

    /**
     * 批量删除车间任务
     * 
     * @param ids 需要删除的车间任务主键
     * @return 结果
     */
    @Override
    public int deleteWorkTaskByIds(Long[] ids)
    {
        return workTaskMapper.deleteWorkTaskByIds(ids);
    }

    /**
     * 删除车间任务信息
     * 
     * @param id 车间任务主键
     * @return 结果
     */
    @Override
    public int deleteWorkTaskById(Long id)
    {
        return workTaskMapper.deleteWorkTaskById(id);
    }

    /**
     * 确认开工
     */
    @Override
    public AjaxResult insertsubmitStart(WorkTaskVo workTaskVo) {
        WorkTask workTask = workTaskMapper.selectWorkTaskById(workTaskVo.getId());
        workTask.setTaskStatus("已开始");
        workTask.setPlanStartTime(workTaskVo.getPlanStartTime());
        workTask.setPlanOverTime(workTaskVo.getPlanOverTime());
        workTaskMapper.updateWorkTask(workTask);

        ProductInfo productInfo = productInfoMapper.selectProductInfoByName(workTask.getProductName());
        Craft craft = craftMapper.selectCraftById(productInfo.getCraftId());
        List<ProcessMaterial> processMaterials = processMaterialMapper.selectProMatByCode(craft.getCraftCode());

        for (int i = 0; i < workTask.getPlanNum(); i++) {
            //任务卡编号
            String takCardCode = workTask.getWorkTaskCode() + "-" + String.valueOf(taskCodeLast++);
            Taskcard taskcard = new Taskcard();
            taskcard.setTaskcardCode(takCardCode);
            taskcard.setWorkTaskCode(workTask.getWorkTaskCode());
            taskcard.setPlanNum((long) processMaterials.size());
            taskcard.setProductName(workTask.getProductName());
            taskcard.setStatus("未报工");
            taskcard.setAlreadyNum(0L);
            taskcard.setTaskSchedule(0L);
            taskcardMapper.insertTaskcard(taskcard);
        }
        return AjaxResult.success("确认开工");
    }

    /**
     * 领料清单
     */
    @Override
    public AjaxResult selectgetPickMatByCode(String workTaskCode) {
        WorkTask workTask = workTaskMapper.selectWorkTaskByCode(workTaskCode);
        ProductInfo productInfo = productInfoMapper.selectProductInfoByName(workTask.getProductName());
        Craft craft = craftMapper.selectCraftById(productInfo.getCraftId());
        List<ProcessMaterial> processMaterials = processMaterialMapper.selectProMatByCode(craft.getCraftCode());
        for (ProcessMaterial processMaterial : processMaterials) {
            processMaterial.setNeedNum(processMaterial.getProMatNum()*workTask.getPlanNum());
        }
        return AjaxResult.success(processMaterials);
    }

    /**
     * 确定领料
     */
    @Override
    public AjaxResult insertsubmitPicking(PickVo pickVo) {
        List<ProcessMaterial> processMaterials = pickVo.getProcessMaterials();
        for (ProcessMaterial processMaterial : processMaterials) {
            Picking picking = new Picking();
            picking.setWorkTaskCode(pickVo.getWorkTaskCode());
            picking.setMaterialName(processMaterial.getMaterialName());
            picking.setPickingStatus("已领料");
            picking.setNeedNum(processMaterial.getNeedNum());
            pickingMapper.insertPicking(picking);

            Material material = materialMapper.selectMaterialByName(processMaterial.getMaterialName());
            material.setMaterialNum(material.getMaterialNum()-processMaterial.getNeedNum());
            materialMapper.updateMaterial(material);
        }
        return AjaxResult.success("领料成功");
    }

}


