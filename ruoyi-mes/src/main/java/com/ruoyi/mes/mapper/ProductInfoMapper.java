package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.ProductInfo;

/**
 * 产品管理Mapper接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface ProductInfoMapper 
{
    /**
     * 查询产品管理
     * 
     * @param id 产品管理主键
     * @return 产品管理
     */
    public ProductInfo selectProductInfoById(Long id);

    /**
     * 查询产品管理列表
     * 
     * @param productInfo 产品管理
     * @return 产品管理集合
     */
    public List<ProductInfo> selectProductInfoList(ProductInfo productInfo);

    /**
     * 新增产品管理
     * 
     * @param productInfo 产品管理
     * @return 结果
     */
    public int insertProductInfo(ProductInfo productInfo);

    /**
     * 查询bom为空的产品
     * @param productInfo
     * @return
     */
    List<ProductInfo> selectnullbomlistList(ProductInfo productInfo);
    /**
     * 修改产品管理
     * 
     * @param productInfo 产品管理
     * @return 结果
     */
    public int updateProductInfo(ProductInfo productInfo);

    /**
     * 删除产品管理
     * 
     * @param id 产品管理主键
     * @return 结果
     */
    public int deleteProductInfoById(Long id);

    /**
     * 批量删除产品管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductInfoByIds(Long[] ids);


    ProductInfo selectProductInfoByName(String productlName);

    ProductInfo selectProductInfoByBomCode(String BomCode);

    List<ProductInfo> selectnullCraftList(ProductInfo productInfo);

    ProductInfo selectProductInfoByCraftId(Long id);

    ProductInfo selectProductInfoByCode(String productCode);
}
