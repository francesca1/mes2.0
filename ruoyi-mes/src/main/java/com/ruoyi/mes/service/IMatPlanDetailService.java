package com.ruoyi.mes.service;

import java.util.List;
import com.ruoyi.mes.domain.MatPlanDetail;

/**
 * 物料计划详细Service接口
 * 
 * @author g4
 * @date 2023-05-08
 */
public interface IMatPlanDetailService 
{
    /**
     * 查询物料计划详细
     * 
     * @param id 物料计划详细主键
     * @return 物料计划详细
     */
    public MatPlanDetail selectMatPlanDetailById(Long id);

    /**
     * 查询物料计划详细列表
     * 
     * @param matPlanDetail 物料计划详细
     * @return 物料计划详细集合
     */
    public List<MatPlanDetail> selectMatPlanDetailList(MatPlanDetail matPlanDetail);

    /**
     * 新增物料计划详细
     * 
     * @param matPlanDetail 物料计划详细
     * @return 结果
     */
    public int insertMatPlanDetail(MatPlanDetail matPlanDetail);

    /**
     * 修改物料计划详细
     * 
     * @param matPlanDetail 物料计划详细
     * @return 结果
     */
    public int updateMatPlanDetail(MatPlanDetail matPlanDetail);

    /**
     * 批量删除物料计划详细
     * 
     * @param ids 需要删除的物料计划详细主键集合
     * @return 结果
     */
    public int deleteMatPlanDetailByIds(Long[] ids);

    /**
     * 删除物料计划详细信息
     * 
     * @param id 物料计划详细主键
     * @return 结果
     */
    public int deleteMatPlanDetailById(Long id);
}
