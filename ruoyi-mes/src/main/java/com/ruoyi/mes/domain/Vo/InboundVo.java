package com.ruoyi.mes.domain.Vo;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class InboundVo {

    /** 任务卡编号 */
    @Excel(name = "任务卡编号")
    private String taskcardCode;

    /** 库位 */
    @Excel(name = "库位")
    private String binLocation;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String depotName;
}
