package com.ruoyi.mes.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * BOM管理对象 bom_info
 * 
 * @author g4
 * @date 2023-04-11
 */
@Data
public class BomInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** bomid */
    private Long id;

    /** bom编码 */
    @Excel(name = "bom编码")
    private String bomCode;

    /** 物料id */
    @Excel(name = "物料id")
    private Long materialId;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    /** 物料数量 */
    @Excel(name = "物料数量")
    private Long materialNum;

    /** 物料类型 */
    @Excel(name = "物料类型")
    private String materialType;

    /** 产品名称 */
    @TableField(exist = false)
    private String productName;

    /** 产品编码 */
    @TableField(exist = false)
    private String productCode;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 创建人 */
    @Excel(name = "创建人")
    private String userId;

    public String getProductCode() {
        return productCode;
    }

    private List<BomMaterial> bomMaterials;

    public List<BomMaterial> getBomMaterials() {
        return bomMaterials;
    }

    public void setBomMaterials(List<BomMaterial> bomMaterials) {
        this.bomMaterials = bomMaterials;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBomCode(String bomCode) 
    {
        this.bomCode = bomCode;
    }

    public String getBomCode() 
    {
        return bomCode;
    }
    public void setMaterialId(Long materialId) 
    {
        this.materialId = materialId;
    }

    public Long getMaterialId() 
    {
        return materialId;
    }
    public void setMaterialName(String materialName) 
    {
        this.materialName = materialName;
    }

    public String getMaterialName() 
    {
        return materialName;
    }
    public void setMaterialNum(Long materialNum) 
    {
        this.materialNum = materialNum;
    }

    public Long getMaterialNum() 
    {
        return materialNum;
    }
    public void setMaterialType(String materialType) 
    {
        this.materialType = materialType;
    }

    public String getMaterialType() 
    {
        return materialType;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("bomCode", getBomCode())
            .append("materialId", getMaterialId())
            .append("materialName", getMaterialName())
            .append("materialNum", getMaterialNum())
            .append("materialType", getMaterialType())
            .append("note", getNote())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
                .append("productName",getProductName())
            .toString();
    }
}
