package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.mes.domain.BomInfo;
import com.ruoyi.mes.domain.BomMaterial;
import com.ruoyi.mes.domain.Craft;
import com.ruoyi.mes.mapper.BomInfoMapper;
import com.ruoyi.mes.mapper.BomMaterialMapper;
import com.ruoyi.mes.mapper.CraftMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.ProductInfoMapper;
import com.ruoyi.mes.domain.ProductInfo;
import com.ruoyi.mes.service.IProductInfoService;

/**
 * 产品管理Service业务层处理
 * 
 * @author g4
 * @date 2023-04-11
 */
@Service
public class ProductInfoServiceImpl implements IProductInfoService 
{
    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private BomInfoMapper bomInfoMapper;

    @Autowired
    private CraftMapper craftMapper;

    @Autowired
    private BomMaterialMapper bomMaterialMapper;

    /**
     * 查询产品管理
     * 
     * @param id 产品管理主键
     * @return 产品管理
     */
    @Override
    public ProductInfo selectProductInfoById(Long id)
    {
        return productInfoMapper.selectProductInfoById(id);
    }

    /**
     * 查询产品管理列表
     * 
     * @param productInfo 产品管理
     * @return 产品管理
     */
    @Override
    public List<ProductInfo> selectProductInfoList(ProductInfo productInfo)
    {
        return productInfoMapper.selectProductInfoList(productInfo);
    }
    /**
     * 获取bom为空的产品
     * @param productInfo
     * @return
     */
    @Override
    public List<ProductInfo> selectnullbomlistList(ProductInfo productInfo) {
        return productInfoMapper.selectnullbomlistList(productInfo);
    }

    @Override
    public List<ProductInfo> selectnullCraftList(ProductInfo productInfo) {
        return productInfoMapper.selectnullCraftList(productInfo);
    }

    /**
     * 新增产品管理
     *
     * @param productInfo 产品管理
     * @return 结果
     */
    @Override
    public AjaxResult insertProductInfo(ProductInfo productInfo)
    {
        ProductInfo productInfo1 = new ProductInfo();
        productInfo1.setProductName(productInfo.getProductName());
        productInfo.setInvenNum(0L);
        productInfo.setCreateDate(DateUtils.getNowDate());
        productInfo.setUserId(SecurityUtils.getLoginUser().getUser().getNickName());
        List<ProductInfo> productInfos = productInfoMapper.selectProductInfoList(productInfo1);
        if(productInfos.size() !=0) {
            return AjaxResult.error("产品名称已存在");
        }
        productInfoMapper.insertProductInfo(productInfo);
        return AjaxResult.success("新增成功");
    }

    /**
     * 修改产品管理
     * 
     * @param productInfo 产品管理
     * @return 结果
     */
    @Override
    public int updateProductInfo(ProductInfo productInfo)
    {
        return productInfoMapper.updateProductInfo(productInfo);
    }

    /**
     * 批量删除产品管理
     * 
     * @param ids 需要删除的产品管理主键
     * @return 结果
     */
    @Override
    public int deleteProductInfoByIds(Long[] ids)
    {

        return productInfoMapper.deleteProductInfoByIds(ids);
    }

    /**
     * 删除产品
     * 
     * @param id 产品管理主键
     * @return 结果
     */
    @Override
    public int deleteProductInfoById(Long id)
    {
        //删除产品对应bom
        ProductInfo productInfo = productInfoMapper.selectProductInfoById(id);
        bomInfoMapper.deleteBomInfoByCode(productInfo.getBomCode());
        //删除bom对应物料
        BomMaterial bomMaterial = bomMaterialMapper.selectBomMaterialByCode(productInfo.getBomCode());
        bomMaterialMapper.deleteBomMaterialByCode(bomMaterial.getBomCode());

        return productInfoMapper.deleteProductInfoById(id);
    }

    /**
     * 获取产品bom信息
     * @param productId
     * @return
     */
    @Override
    public AjaxResult getbomInfo(Long productId) {
        ProductInfo productInfo = selectProductInfoById(productId);
        BomInfo bomInfo = bomInfoMapper.selectBomInfoByCode(productInfo.getBomCode());
        return AjaxResult.success(bomInfo);
    }

    /**
     * 获取产品工艺信息
     * @param productId
     * @return
     */
    @Override
    public AjaxResult getcraftInfo(Long productId) {
        ProductInfo productInfo = selectProductInfoById(productId);
        Craft craft = craftMapper.selectCraftById(productInfo.getCraftId());
        return AjaxResult.success(craft);
    }

}
