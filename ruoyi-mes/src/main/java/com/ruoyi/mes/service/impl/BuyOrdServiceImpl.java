package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.mes.Util.CodeUtils;
import com.ruoyi.mes.domain.InboundBuy;
import com.ruoyi.mes.domain.MatPlanDetail;
import com.ruoyi.mes.domain.Material;
import com.ruoyi.mes.domain.Vo.BuyNeedOrdVo;
import com.ruoyi.mes.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.domain.BuyOrd;
import com.ruoyi.mes.service.IBuyOrdService;

/**
 * 采购订单Service业务层处理
 * 
 * @author g4
 * @date 2023-05-08
 */
@Service
public class BuyOrdServiceImpl implements IBuyOrdService 
{
    @Autowired
    private BuyOrdMapper buyOrdMapper;

    @Autowired
    private MatPlanMapper matPlanMapper;

    @Autowired
    private MatPlanDetailMapper matPlanDetailMapper;

    @Autowired
    private MaterialMapper materialMapper;

    @Autowired
    private InboundBuyMapper inboundBuyMapper;

    /**
     * 查询采购订单
     * 
     * @param id 采购订单主键
     * @return 采购订单
     */
    @Override
    public BuyOrd selectBuyOrdById(Long id)
    {
        return buyOrdMapper.selectBuyOrdById(id);
    }

    /**
     * 查询采购订单列表
     * 
     * @param buyOrd 采购订单
     * @return 采购订单
     */
    @Override
    public List<BuyOrd> selectBuyOrdList(BuyOrd buyOrd)
    {
        return buyOrdMapper.selectBuyOrdList(buyOrd);
    }

    /**
     * 新增采购订单
     * 
     * @param buyOrd 采购订单
     * @return 结果
     */
    @Override
    public int insertBuyOrd(BuyOrd buyOrd)
    {
        buyOrd.setCreateTime(DateUtils.getNowDate());
        return buyOrdMapper.insertBuyOrd(buyOrd);
    }

    /**
     * 修改采购订单
     * 
     * @param buyOrd 采购订单
     * @return 结果
     */
    @Override
    public int updateBuyOrd(BuyOrd buyOrd)
    {
        return buyOrdMapper.updateBuyOrd(buyOrd);
    }

    /**
     * 批量删除采购订单
     * 
     * @param ids 需要删除的采购订单主键
     * @return 结果
     */
    @Override
    public int deleteBuyOrdByIds(Long[] ids)
    {
        return buyOrdMapper.deleteBuyOrdByIds(ids);
    }

    /**
     * 删除采购订单信息
     * 
     * @param id 采购订单主键
     * @return 结果
     */
    @Override
    public int deleteBuyOrdById(Long id)
    {
        return buyOrdMapper.deleteBuyOrdById(id);
    }

    /**
     * 新增采购订单-物料
     *
     * @param buyNeedOrdVo
     * @return
     */
    @Override
    public AjaxResult insertAddBuyOrd(BuyNeedOrdVo buyNeedOrdVo) {
        String buyOrderCode = CodeUtils.BuyOrderCodeCreat();
        BuyOrd buyOrd1 = new BuyOrd();
        buyOrd1.setBuyordCode(buyOrderCode);
        buyOrd1.setName(buyNeedOrdVo.getName());
        buyOrd1.setBuyneedCode(buyNeedOrdVo.getBuyneedCode());
        buyOrd1.setMatplanCode(buyNeedOrdVo.getMatplanCode());
        MatPlanDetail matPlanDetail = matPlanDetailMapper.selectMatPlanDetailByCodeName(buyNeedOrdVo.getMatplanCode(),buyNeedOrdVo.getMaterialName());
        buyOrd1.setMaterialName(matPlanDetail.getMaterialName());
        Material material = materialMapper.selectMaterialByName(matPlanDetail.getMaterialName());
        buyOrd1.setBuyNum(matPlanDetail.getNeedNum()-material.getMaterialNum());
//        buyOrd1.setAlreadyDate(buyNeedOrdVo.getAlreadyDate());
        buyOrd1.setOrdStatus("未完成");
        buyOrd1.setAlreadyStatus("未完成");
        buyOrdMapper.insertBuyOrd(buyOrd1);
        return AjaxResult.success();
    }

    /**
     * 新增采购订单-all
     */
    @Override
    public AjaxResult insertAddBuyOrdAll(BuyNeedOrdVo buyNeedOrdVo) {
        String buyOrderCode = CodeUtils.BuyOrderCodeCreat();
        BuyOrd buyOrd1 = new BuyOrd();
        buyOrd1.setBuyordCode(buyOrderCode);
        buyOrd1.setName(buyNeedOrdVo.getName());
        buyOrd1.setBuyneedCode(buyNeedOrdVo.getBuyneedCode());
        buyOrd1.setMatplanCode(buyNeedOrdVo.getMatplanCode());
        buyOrd1.setBuyNum(buyOrd1.getBuyNum());
        buyOrd1.setOrdStatus("未完成");
        buyOrd1.setAlreadyStatus("未完成");


        List<Material> materials = buyNeedOrdVo.getMaterials();
        for (Material material : materials) {
            buyOrd1.setMaterialCode(material.getMaterialCode());
            buyOrd1.setMaterialName(material.getMaterialName());
            buyOrd1.setBuyNum(material.getBuyNum());
            buyOrd1.setAlreadyDate(material.getAlreadyDate());
        }
        buyOrdMapper.insertBuyOrd(buyOrd1);
        return AjaxResult.success();
    }

    /**
     * 获取采购订单详细信息
     */
    @Override
    public AjaxResult selectBuyOrdByCode(String buyordCode) {
        BuyOrd buyOrd = buyOrdMapper.selectBuyOrdByCode(buyordCode);
        List<Material> materials = materialMapper.selectMaterialByMatName(buyOrd.getMaterialName());
        for (Material material : materials) {
            material.setBuyNum(buyOrd.getBuyNum());
            material.setAlreadyDate(buyOrd.getAlreadyDate());
        }
        return AjaxResult.success(materials);
    }

    /**
     * 确认到货
     */
    @Override
    public AjaxResult insertconfirmArrival(BuyOrd buyOrd) {
        InboundBuy inboundBuy = new InboundBuy();
        inboundBuy.setBuyordCode(buyOrd.getBuyordCode());
        inboundBuy.setInboundNum(buyOrd.getBuyNum());
        inboundBuy.setInboundDate(DateUtils.getNowDate());
        inboundBuy.setInboundType("采购入库");
        inboundBuy.setInboundStatus("未入库");
        inboundBuy.setMaterialName(buyOrd.getMaterialName());
        inboundBuyMapper.insertInboundBuy(inboundBuy);

        buyOrd.setAlreadyStatus("已完成");
        buyOrd.setOrdStatus("已完成");
        buyOrdMapper.updateBuyOrd(buyOrd);

        return AjaxResult.success("确认收货");
    }
}
