package com.ruoyi.mes.service;

import java.util.List;
import com.ruoyi.mes.domain.ProductPlan;
import com.ruoyi.mes.domain.Vo.PlanIssueVo;
import com.ruoyi.mes.domain.Vo.ProPlanDetailVo;

/**
 * 生产计划Service接口
 * 
 * @author g4
 * @date 2023-05-08
 */
public interface IProductPlanService 
{
    /**
     * 查询生产计划
     * 
     * @param id 生产计划主键
     * @return 生产计划
     */
    public ProductPlan selectProductPlanById(Long id);

    /**
     * 查询生产计划列表
     * 
     * @param productPlan 生产计划
     * @return 生产计划集合
     */
    public List<ProductPlan> selectProductPlanList(ProductPlan productPlan);

    /**
     * 新增生产计划
     * 
     * @param productPlan 生产计划
     * @return 结果
     */
    public int insertProductPlan(ProductPlan productPlan);

    /**
     * 修改生产计划
     * 
     * @param productPlan 生产计划
     * @return 结果
     */
    public int updateProductPlan(ProductPlan productPlan);

    /**
     * 批量删除生产计划
     * 
     * @param ids 需要删除的生产计划主键集合
     * @return 结果
     */
    public int deleteProductPlanByIds(Long[] ids);

    /**
     * 删除生产计划信息
     * 
     * @param id 生产计划主键
     * @return 结果
     */
    public int deleteProductPlanById(Long id);
    /**
     * 获取生产计划明细
     */
    ProPlanDetailVo selectProductPlanByproplanCode(String proplanCode);



}
