package com.ruoyi.mes.mapper;

import java.util.List;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.BomMaterial;
import com.ruoyi.mes.domain.Material;
import com.ruoyi.mes.domain.Vo.BomMatVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * bom物料Mapper接口
 * 
 * @author g4
 * @date 2023-04-20
 */
public interface BomMaterialMapper 
{
    /**
     * 查询bom物料
     * 
     * @param id bom物料主键
     * @return bom物料
     */
    public BomMaterial selectBomMaterialById(Long id);

    /**
     * 查询bom物料列表
     * 
     * @param bomMaterial bom物料
     * @return bom物料集合
     */
    public List<BomMaterial> selectBomMaterialList(BomMaterial bomMaterial);

    /**
     * 新增bom物料
     * 
     * @param bomMaterial bom物料
     * @return 结果
     */
    public int insertBomMaterial(BomMaterial bomMaterial);

    /**
     * 修改bom物料
     * 
     * @param bomMaterial bom物料
     * @return 结果
     */
    public int updateBomMaterial(BomMaterial bomMaterial);

    /**
     * 删除bom物料
     * 
     * @param id bom物料主键
     * @return 结果
     */
    public int deleteBomMaterialById(Long id);

    /**
     * 批量删除bom物料
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBomMaterialByIds(Long[] ids);

    BomMaterial selectBomMaterialByCode(String bomCode);


    /**
     * 查 - Bom物料信息 - bomCode
     * @param bomCode
     * @return
     */
    List<BomMaterial> selectMaterialsByBomCode(String bomCode);

    BomMatVo checkBomCodeUnique(BomMatVo bomMatVo);

    List<BomMatVo> selectbomMatVo(BomMatVo bomMatVo1);

    /**
     * 删除产品管理
     */
    void deleteBomMaterialByCode(String bomCode);
}
