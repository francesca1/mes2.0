package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.ProductInfo;

/**
 * 产品管理Service接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface IProductInfoService 
{
    /**
     * 查询产品管理
     * 
     * @param id 产品管理主键
     * @return 产品管理
     */
    public ProductInfo selectProductInfoById(Long id);

    /**
     * 查询产品管理列表
     * 
     * @param productInfo 产品管理
     * @return 产品管理集合
     */
    public List<ProductInfo> selectProductInfoList(ProductInfo productInfo);

    /**
     * 新增产品管理
     * 
     * @param productInfo 产品管理
     * @return 结果
     */
    public AjaxResult insertProductInfo(ProductInfo productInfo);

    /**
     * 修改产品管理
     * 
     * @param productInfo 产品管理
     * @return 结果
     */
    public int updateProductInfo(ProductInfo productInfo);

    /**
     * 批量删除产品管理
     * 
     * @param ids 需要删除的产品管理主键集合
     * @return 结果
     */
    public int deleteProductInfoByIds(Long[] ids);

    /**
     * 删除产品管理信息
     * 
     * @param id 产品管理主键
     * @return 结果
     */
    public int deleteProductInfoById(Long id);

    /**
     * 获取产品bom信息
     * @param productId
     * @return
     */
    AjaxResult getbomInfo(Long productId);

    AjaxResult getcraftInfo(Long productId);

    /**
     * 查询bom为空的产品列表
     */
    List<ProductInfo> selectnullbomlistList(ProductInfo productInfo);
    /**
     * 查询工艺为空的产品列表
     */
    List<ProductInfo> selectnullCraftList(ProductInfo productInfo);
}
