package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.MatPlanDetail;
import com.ruoyi.mes.service.IMatPlanDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物料计划详细Controller
 * 
 * @author g4
 * @date 2023-05-08
 */
@RestController
@RequestMapping("/mes/matplandetail")
public class MatPlanDetailController extends BaseController
{
    @Autowired
    private IMatPlanDetailService matPlanDetailService;

    /**
     * 查询物料计划详细列表
     */
    @PreAuthorize("@ss.hasPermi('mes:matplandetail:list')")
    @GetMapping("/list")
    public TableDataInfo list(MatPlanDetail matPlanDetail)
    {
        startPage();
        List<MatPlanDetail> list = matPlanDetailService.selectMatPlanDetailList(matPlanDetail);
        return getDataTable(list);
    }

    /**
     * 导出物料计划详细列表
     */
    @PreAuthorize("@ss.hasPermi('mes:matplandetail:export')")
    @Log(title = "物料计划详细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, MatPlanDetail matPlanDetail)
    {
        List<MatPlanDetail> list = matPlanDetailService.selectMatPlanDetailList(matPlanDetail);
        ExcelUtil<MatPlanDetail> util = new ExcelUtil<MatPlanDetail>(MatPlanDetail.class);
        util.exportExcel(response, list, "物料计划详细数据");
    }

    /**
     * 获取物料计划详细详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:matplandetail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(matPlanDetailService.selectMatPlanDetailById(id));
    }

    /**
     * 新增物料计划详细
     */
    @PreAuthorize("@ss.hasPermi('mes:matplandetail:add')")
    @Log(title = "物料计划详细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody MatPlanDetail matPlanDetail)
    {
        return toAjax(matPlanDetailService.insertMatPlanDetail(matPlanDetail));
    }

    /**
     * 修改物料计划详细
     */
    @PreAuthorize("@ss.hasPermi('mes:matplandetail:edit')")
    @Log(title = "物料计划详细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody MatPlanDetail matPlanDetail)
    {
        return toAjax(matPlanDetailService.updateMatPlanDetail(matPlanDetail));
    }

    /**
     * 删除物料计划详细
     */
    @PreAuthorize("@ss.hasPermi('mes:matplandetail:remove')")
    @Log(title = "物料计划详细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(matPlanDetailService.deleteMatPlanDetailByIds(ids));
    }
}
