package com.ruoyi.mes.mapper;

import java.util.List;

import com.ruoyi.mes.domain.OrderTable;
import com.ruoyi.mes.domain.Ordpro;

/**
 * 订单产品表Mapper接口
 * 
 * @author ruoyi
 * @date 2023-05-05
 */
public interface OrdproMapper 
{
    /**
     * 查询订单产品表
     * 
     * @param ordCode 订单产品表主键
     * @return 订单产品表
     */
    public Ordpro selectOrdproByOrdCode(String ordCode);

    /**
     * 查询订单产品表列表
     * 
     * @param ordpro 订单产品表
     * @return 订单产品表集合
     */
    public List<Ordpro> selectOrdproList(Ordpro ordpro);

    /**
     * 新增订单产品表
     * 
     * @param ordpro 订单产品表
     * @return 结果
     */
    public int insertOrdpro(Ordpro ordpro);

    /**
     * 修改订单产品表
     * 
     * @param ordpro 订单产品表
     * @return 结果
     */
    public int updateOrdpro(Ordpro ordpro);

    /**
     * 删除订单产品表
     * 
     * @param ordCode 订单产品表主键
     * @return 结果
     */
    public int deleteOrdproByOrdCode(String ordCode);

    /**
     * 批量删除订单产品表
     * 
     * @param ordCodes 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteOrdproByOrdCodes(String[] ordCodes);


}
