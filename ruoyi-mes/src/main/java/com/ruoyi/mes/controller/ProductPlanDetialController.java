package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.mes.domain.Vo.PlanIssueVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.ProductPlanDetial;
import com.ruoyi.mes.service.IProductPlanDetialService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 生产计划详情Controller
 * 
 * @author g4
 * @date 2023-05-15
 */
@RestController
@RequestMapping("/mes/proplandetial")
public class ProductPlanDetialController extends BaseController
{
    @Autowired
    private IProductPlanDetialService productPlanDetialService;

    /**
     * 查询生产计划详情列表
     */
    @PreAuthorize("@ss.hasPermi('mes:proplandetial:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductPlanDetial productPlanDetial)
    {
        startPage();
        List<ProductPlanDetial> list = productPlanDetialService.selectProductPlanDetialList(productPlanDetial);
        return getDataTable(list);
    }

    /**
     * 导出生产计划详情列表
     */
    @PreAuthorize("@ss.hasPermi('mes:proplandetial:export')")
    @Log(title = "生产计划详情", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductPlanDetial productPlanDetial)
    {
        List<ProductPlanDetial> list = productPlanDetialService.selectProductPlanDetialList(productPlanDetial);
        ExcelUtil<ProductPlanDetial> util = new ExcelUtil<ProductPlanDetial>(ProductPlanDetial.class);
        util.exportExcel(response, list, "生产计划详情数据");
    }

    /**
     * 获取生产计划详情详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:proplandetial:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(productPlanDetialService.selectProductPlanDetialById(id));
    }

    /**
     * 新增生产计划详情
     */
    @PreAuthorize("@ss.hasPermi('mes:proplandetial:add')")
    @Log(title = "生产计划详情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductPlanDetial productPlanDetial)
    {
        return toAjax(productPlanDetialService.insertProductPlanDetial(productPlanDetial));
    }

    /**
     * 修改生产计划详情
     */
    @PreAuthorize("@ss.hasPermi('mes:proplandetial:edit')")
    @Log(title = "生产计划详情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductPlanDetial productPlanDetial)
    {
        return toAjax(productPlanDetialService.updateProductPlanDetial(productPlanDetial));
    }

    /**
     * 删除生产计划详情
     */
    @PreAuthorize("@ss.hasPermi('mes:proplandetial:remove')")
    @Log(title = "生产计划详情", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(productPlanDetialService.deleteProductPlanDetialByIds(ids));
    }

    /**
     * 计划下达
     * @param planIssueVo
     * @return
     */
    @PostMapping("/planIssue")
    public AjaxResult planIssue(@RequestBody PlanIssueVo planIssueVo)
    {
        return productPlanDetialService.insertplanIssue(planIssueVo);
    }

    /**
     * 获取下达计划详情
     */
    @GetMapping( "getIssueInfo/{proplanCode}")
    public AjaxResult getIssueInfo(@PathVariable("proplanCode") String proplanCode)
    {
        return productPlanDetialService.selectgetIssueInfoByCode(proplanCode);
    }
}
