package com.ruoyi.mes.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 采购需求单对象 buy_need
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
public class BuyNeed extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 采购需求单编码 */
    @Excel(name = "采购需求单编码")
    private String buyneedCode;

    /** 物料计划编码 */
    @Excel(name = "物料计划编码")
    private String matplanCode;

    /** 订单编码 */
    @Excel(name = "订单编码")
    private String ordCode;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 创建人 */
    @Excel(name = "创建人")
    private String userId;

    /** 备注 */
    private String note;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBuyneedCode(String buyneedCode) 
    {
        this.buyneedCode = buyneedCode;
    }

    public String getBuyneedCode() 
    {
        return buyneedCode;
    }

    public String getMatplanCode() {
        return matplanCode;
    }

    public void setMatplanCode(String matplanCode) {
        this.matplanCode = matplanCode;
    }

    public String getOrdCode() {
        return ordCode;
    }

    public void setOrdCode(String ordCode) {
        this.ordCode = ordCode;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("buyneedCode", getBuyneedCode())
            .append("matplanCode", getMatplanCode())
            .append("ordCode", getOrdCode())
            .append("status", getStatus())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("note", getNote())
            .append("materialName", getMaterialName())
            .toString();
    }
}
