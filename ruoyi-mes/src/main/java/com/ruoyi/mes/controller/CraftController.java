package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.mes.domain.Process;
import com.ruoyi.mes.domain.Vo.CraftProVo;
import com.ruoyi.mes.domain.Vo.ProMatVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.Craft;
import com.ruoyi.mes.service.ICraftService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 工艺管理Controller
 * 
 * @author g4
 * @date 2023-04-18
 */
@RestController
@RequestMapping("/mes/craft")
public class CraftController extends BaseController
{
    @Autowired
    private ICraftService craftService;

    /**
     * 查询工艺管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:craft:list')")
    @GetMapping("/list")
    public TableDataInfo list(Craft craft)
    {
        startPage();
        List<Craft> list = craftService.selectCraftList(craft);
        return getDataTable(list);
    }

    /**
     * 导出工艺管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:craft:export')")
    @Log(title = "工艺管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Craft craft)
    {
        List<Craft> list = craftService.selectCraftList(craft);
        ExcelUtil<Craft> util = new ExcelUtil<Craft>(Craft.class);
        util.exportExcel(response, list, "工艺管理数据");
    }

    /**
     * 获取工艺管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:craft:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(craftService.selectCraftById(id));
    }

    /**
     * 新增工艺管理
     */
    @PreAuthorize("@ss.hasPermi('mes:craft:add')")
    @Log(title = "工艺管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Craft craft)
    {
        if(UserConstants.PRONOT_UNIQUE.equals(craftService.checkCraftCodeUnique(craft))){
            return AjaxResult.error("工序编码已存在");
        }
        if(UserConstants.PRONOT_UNIQUE.equals(craftService.checkCraftNameUnique(craft))){
            return AjaxResult.error("工序名称已存在");
        }
        return craftService.insertCraft(craft);
    }


    /**
     * 修改工艺管理
     */
    @PreAuthorize("@ss.hasPermi('mes:craft:edit')")
    @Log(title = "工艺管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Craft craft)
    {
        return toAjax(craftService.updateCraft(craft));
    }


    /**
     * 删除工艺管理
     */
    @PreAuthorize("@ss.hasPermi('mes:craft:remove')")
    @Log(title = "工艺管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(craftService.deleteCraftByIds(ids));
    }


    /**
     * 新增工艺的工序
     * @param craftProVo
     * @return
     */
    @PostMapping("/addProcess")
    public AjaxResult addprocess(@RequestBody CraftProVo craftProVo)
    {
        return craftService.insertCraftProcess(craftProVo);
    }


    /**
     * 查-工艺的工序 在ProcessController
     */

    /**
     * 删除工艺
     */
    @DeleteMapping("remove/{id}")
    public AjaxResult removeCraft(@PathVariable Long id)
    {
        return toAjax(craftService.deleteCraftById(id));
    }


    /**
     * 修改工艺管理
     */
    @PreAuthorize("@ss.hasPermi('mes:craft:edit')")
    @Log(title = "工艺管理", businessType = BusinessType.UPDATE)
    @PutMapping("/editcraft")
    public AjaxResult editcraft(@RequestBody CraftProVo craftProVo)
    {
        return craftService.updateCraft(craftProVo);
    }
}
