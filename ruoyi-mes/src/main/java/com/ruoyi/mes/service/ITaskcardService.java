package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.Taskcard;

/**
 * 车间任务卡Service接口
 * 
 * @author g4
 * @date 2023-04-11
 */
public interface ITaskcardService 
{
    /**
     * 查询车间任务卡
     * 
     * @param id 车间任务卡主键
     * @return 车间任务卡
     */
    public Taskcard selectTaskcardById(Long id);

    /**
     * 查询车间任务卡列表
     * 
     * @param taskcard 车间任务卡
     * @return 车间任务卡集合
     */
    public List<Taskcard> selectTaskcardList(Taskcard taskcard);

    /**
     * 新增车间任务卡
     * 
     * @param taskcard 车间任务卡
     * @return 结果
     */
    public int insertTaskcard(Taskcard taskcard);

    /**
     * 修改车间任务卡
     * 
     * @param taskcard 车间任务卡
     * @return 结果
     */
    public int updateTaskcard(Taskcard taskcard);

    /**
     * 批量删除车间任务卡
     * 
     * @param ids 需要删除的车间任务卡主键集合
     * @return 结果
     */
    public int deleteTaskcardByIds(Long[] ids);

    /**
     * 删除车间任务卡信息
     * 
     * @param id 车间任务卡主键
     * @return 结果
     */
    public int deleteTaskcardById(Long id);

    /**
     * 获取工序
     *
     * @return
     */
    AjaxResult getProcessInfo(Long id);

    /**
     * 确认报工
     */
    int insertsubmitReport(Long id);

}
