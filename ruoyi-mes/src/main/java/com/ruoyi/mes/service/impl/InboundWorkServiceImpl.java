package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.*;
import com.ruoyi.mes.domain.Vo.InboundVo;
import com.ruoyi.mes.mapper.*;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.service.IInboundWorkService;

/**
 * 完工入库Service业务层处理
 * 
 * @author g4
 * @date 2023-04-11
 */
@Service
public class InboundWorkServiceImpl implements IInboundWorkService 
{
    @Autowired
    private InboundWorkMapper inboundWorkMapper;

    @Autowired
    private ProductPlanMapper productPlanMapper;

    @Autowired
    private WorkTaskMapper workTaskMapper;

    @Autowired
    private TaskcardMapper taskcardMapper;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private OutboundSellMapper outboundSellMapper;
    /**
     * 查询完工入库
     * 
     * @param id 完工入库主键
     * @return 完工入库
     */
    @Override
    public InboundWork selectInboundWorkById(Long id)
    {
        return inboundWorkMapper.selectInboundWorkById(id);
    }

    /**
     * 查询完工入库列表
     * 
     * @param inboundWork 完工入库
     * @return 完工入库
     */
    @Override
    public List<InboundWork> selectInboundWorkList(InboundWork inboundWork)
    {
        return inboundWorkMapper.selectInboundWorkList(inboundWork);
    }

    /**
     * 新增完工入库
     * 
     * @param inboundWork 完工入库
     * @return 结果
     */
    @Override
    public int insertInboundWork(InboundWork inboundWork)
    {
        return inboundWorkMapper.insertInboundWork(inboundWork);
    }

    /**
     * 修改完工入库
     * 
     * @param inboundWork 完工入库
     * @return 结果
     */
    @Override
    public int updateInboundWork(InboundWork inboundWork)
    {
        return inboundWorkMapper.updateInboundWork(inboundWork);
    }

    /**
     * 批量删除完工入库
     * 
     * @param ids 需要删除的完工入库主键
     * @return 结果
     */
    @Override
    public int deleteInboundWorkByIds(Long[] ids)
    {
        return inboundWorkMapper.deleteInboundWorkByIds(ids);
    }

    /**
     * 删除完工入库信息
     * 
     * @param id 完工入库主键
     * @return 结果
     */
    @Override
    public int deleteInboundWorkById(Long id)
    {
        return inboundWorkMapper.deleteInboundWorkById(id);
    }

    /**
     * 确认完工入库
     */
    @Override
    public AjaxResult submitInboundByVo(InboundVo inboundVo) {
        InboundWork inboundWork = inboundWorkMapper.selectInboundWorkByCode(inboundVo.getTaskcardCode());
        inboundWork.setDepotName(inboundVo.getDepotName());
        inboundWork.setBinLocation(inboundVo.getBinLocation());
        inboundWork.setInboundStatus("已入库");
        inboundWorkMapper.updateInboundWork(inboundWork);

        //更新产品库存
        Taskcard taskcard = taskcardMapper.selectTaskcardByCode(inboundVo.getTaskcardCode());
        ProductInfo productInfo = productInfoMapper.selectProductInfoByName(taskcard.getProductName());
        productInfo.setInvenNum(productInfo.getInvenNum()+inboundWork.getInboundNum());
        productInfoMapper.updateProductInfo(productInfo);

        return AjaxResult.success(inboundWork);
    }

    /**
     * 查看完工入库单
     */
    @Override
    public AjaxResult lookInboundByCode(String taskcardCode) {
        Taskcard taskcard = taskcardMapper.selectTaskcardByCode(taskcardCode);
        return AjaxResult.success(taskcard);
    }
}
