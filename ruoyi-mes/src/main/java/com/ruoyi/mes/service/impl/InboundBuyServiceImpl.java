package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.BuyOrd;
import com.ruoyi.mes.domain.Material;
import com.ruoyi.mes.domain.Vo.BinDepotVo;
import com.ruoyi.mes.mapper.BuyOrdMapper;
import com.ruoyi.mes.mapper.MaterialMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.InboundBuyMapper;
import com.ruoyi.mes.domain.InboundBuy;
import com.ruoyi.mes.service.IInboundBuyService;

/**
 * 采购入库Service业务层处理
 * 
 * @author g4
 * @date 2023-04-11
 */
@Service
public class InboundBuyServiceImpl implements IInboundBuyService 
{
    @Autowired
    private InboundBuyMapper inboundBuyMapper;

    @Autowired
    private MaterialMapper materialMapper;
    @Autowired
    private BuyOrdMapper buyOrdMapper;

    /**
     * 查询采购入库
     * 
     * @param id 采购入库主键
     * @return 采购入库
     */
    @Override
    public InboundBuy selectInboundBuyById(Long id)
    {
        return inboundBuyMapper.selectInboundBuyById(id);
    }

    /**
     * 查询采购入库列表
     * 
     * @param inboundBuy 采购入库
     * @return 采购入库
     */
    @Override
    public List<InboundBuy> selectInboundBuyList(InboundBuy inboundBuy)
    {
        return inboundBuyMapper.selectInboundBuyList(inboundBuy);
    }

    /**
     * 新增采购入库
     * 
     * @param inboundBuy 采购入库
     * @return 结果
     */
    @Override
    public int insertInboundBuy(InboundBuy inboundBuy)
    {
        return inboundBuyMapper.insertInboundBuy(inboundBuy);
    }

    /**
     * 修改采购入库
     * 
     * @param inboundBuy 采购入库
     * @return 结果
     */
    @Override
    public int updateInboundBuy(InboundBuy inboundBuy)
    {
        return inboundBuyMapper.updateInboundBuy(inboundBuy);
    }

    /**
     * 批量删除采购入库
     * 
     * @param ids 需要删除的采购入库主键
     * @return 结果
     */
    @Override
    public int deleteInboundBuyByIds(Long[] ids)
    {
        return inboundBuyMapper.deleteInboundBuyByIds(ids);
    }

    /**
     * 删除采购入库信息
     * 
     * @param id 采购入库主键
     * @return 结果
     */
    @Override
    public int deleteInboundBuyById(Long id)
    {
        return inboundBuyMapper.deleteInboundBuyById(id);
    }

    /**
     * 确认采购入库-确认入库库存增加
     */
    @Override
    public AjaxResult insertaddstocks(BinDepotVo binDepotVo) {
        BuyOrd buyOrd = buyOrdMapper.selectBuyOrdByCode(binDepotVo.getBuyordCode());
        InboundBuy inboundBuy = inboundBuyMapper.selectInboundBuyByCode(binDepotVo.getBuyordCode());
        Material material = materialMapper.selectMaterialByName(buyOrd.getMaterialName());
        if(buyOrd.getMaterialName().equals(material.getMaterialName())) {
            material.setMaterialNum(material.getMaterialNum()+inboundBuy.getInboundNum());
            materialMapper.updateMaterial(material);
            inboundBuy.setDepotName(binDepotVo.getDepotName());
            inboundBuy.setBinLocation(binDepotVo.getBinLocation());
            inboundBuy.setInboundStatus("已入库");
            inboundBuyMapper.updateInboundBuy(inboundBuy);

        }
//        else{
//            Material material1 = new Material();
//            material1.setMaterialName(inboundBuy.getMaterialName());
//            material1.setMaterialNum(inboundBuy.getInboundNum());
//            materialMapper.insertMaterial(material1);
//        }
        return AjaxResult.success("入库成功");
    }
}
