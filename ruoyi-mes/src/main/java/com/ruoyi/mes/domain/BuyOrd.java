package com.ruoyi.mes.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;
import java.util.List;

/**
 * 采购订单对象 buy_ord
 * 
 * @author g4
 * @date 2023-05-08
 */
@Data
public class BuyOrd extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 采购订单id */
    private Long id;

    /** 采购订单编号 */
    @Excel(name = "采购订单编号")
    private String buyordCode;

    /** 采购订单名称 */
    @Excel(name = "采购订单名称")
    private String name;

    /** 采购需求单编号 */
    @Excel(name = "采购需求单编号")
    private String buyneedCode;

    /** 物料计划编码 */
    @Excel(name = "物料计划编码")
    private String matplanCode;

    /** 物料名称 */
    @Excel(name = "物料名称")
    private String materialName;

    /** 合同编号 */
    @Excel(name = "合同编号")
    private String contractNum;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private String ordStatus;

    /** 到货状态 */
    @Excel(name = "到货状态")
    private String alreadyStatus;


    /** 到货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到货日期")
    private Date alreadyDate;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long userId;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    /** 采购物料数量 */
    @Excel(name = "采购物料数量")
    private Long buyNum;

    /** 物料编号 */
    @TableField(exist = false)
    @Excel(name = "物料编号")
    private String materialCode;

    /** 物料详情 */
    @Excel(name = "物料详情")
    @TableField(exist = false)
    private List<Material> materials;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBuyordCode(String buyordCode) 
    {
        this.buyordCode = buyordCode;
    }

    public String getBuyordCode() 
    {
        return buyordCode;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public Long getBuyNum() {
        return buyNum;
    }

    public void setBuyNum(Long buyNum) {
        this.buyNum = buyNum;
    }

    public String getMatplanCode() {
        return matplanCode;
    }

    public void setMatplanCode(String matplanCode) {
        this.matplanCode = matplanCode;
    }



    public String getName()
    {
        return name;
    }
    public void setBuyneedCode(String buyneedCode) 
    {
        this.buyneedCode = buyneedCode;
    }

    public String getBuyneedCode() 
    {
        return buyneedCode;
    }
    public void setMaterialName(String materialName) 
    {
        this.materialName = materialName;
    }

    public String getMaterialName() 
    {
        return materialName;
    }
    public void setContractNum(String contractNum) 
    {
        this.contractNum = contractNum;
    }

    public String getContractNum() 
    {
        return contractNum;
    }
    public void setOrdStatus(String ordStatus) 
    {
        this.ordStatus = ordStatus;
    }

    public String getOrdStatus() 
    {
        return ordStatus;
    }
    public void setAlreadyStatus(String alreadyStatus) 
    {
        this.alreadyStatus = alreadyStatus;
    }

    public String getAlreadyStatus() 
    {
        return alreadyStatus;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("buyordCode", getBuyordCode())
            .append("name", getName())
            .append("buyneedCode", getBuyneedCode())
            .append("matplanCode", getMatplanCode())
            .append("materialName", getMaterialName())
            .append("contractNum", getContractNum())
            .append("ordStatus", getOrdStatus())
            .append("alreadyStatus", getAlreadyStatus())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("note", getNote())
            .append("buyNum", getBuyNum())
            .toString();
    }
}
