package com.ruoyi.mes.service;

import java.util.List;
import com.ruoyi.mes.domain.Picking;

/**
 * 生产领料Service接口
 * 
 * @author g4
 * @date 2023-05-23
 */
public interface IPickingService 
{
    /**
     * 查询生产领料
     * 
     * @param id 生产领料主键
     * @return 生产领料
     */
    public Picking selectPickingById(Long id);

    /**
     * 查询生产领料列表
     * 
     * @param picking 生产领料
     * @return 生产领料集合
     */
    public List<Picking> selectPickingList(Picking picking);

    /**
     * 新增生产领料
     * 
     * @param picking 生产领料
     * @return 结果
     */
    public int insertPicking(Picking picking);

    /**
     * 修改生产领料
     * 
     * @param picking 生产领料
     * @return 结果
     */
    public int updatePicking(Picking picking);

    /**
     * 批量删除生产领料
     * 
     * @param ids 需要删除的生产领料主键集合
     * @return 结果
     */
    public int deletePickingByIds(Long[] ids);

    /**
     * 删除生产领料信息
     * 
     * @param id 生产领料主键
     * @return 结果
     */
    public int deletePickingById(Long id);
}
