package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.BuyNeed;

/**
 * 采购需求单Mapper接口
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
public interface BuyNeedMapper 
{
    /**
     * 查询采购需求单
     * 
     * @param id 采购需求单主键
     * @return 采购需求单
     */
    public BuyNeed selectBuyNeedById(Long id);

    /**
     * 查询采购需求单列表
     * 
     * @param buyNeed 采购需求单
     * @return 采购需求单集合
     */
    public List<BuyNeed> selectBuyNeedList(BuyNeed buyNeed);

    /**
     * 新增采购需求单
     * 
     * @param buyNeed 采购需求单
     * @return 结果
     */
    public int insertBuyNeed(BuyNeed buyNeed);

    /**
     * 修改采购需求单
     * 
     * @param buyNeed 采购需求单
     * @return 结果
     */
    public int updateBuyNeed(BuyNeed buyNeed);

    /**
     * 删除采购需求单
     * 
     * @param id 采购需求单主键
     * @return 结果
     */
    public int deleteBuyNeedById(Long id);

    /**
     * 批量删除采购需求单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBuyNeedByIds(Long[] ids);

    BuyNeed selectBuyNeedByCode(String buyneedCode);

}
