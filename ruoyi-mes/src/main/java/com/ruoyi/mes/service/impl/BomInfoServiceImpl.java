package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.BomMaterial;
import com.ruoyi.mes.domain.Material;
import com.ruoyi.mes.domain.ProductInfo;
import com.ruoyi.mes.domain.Vo.BomMatVo;
import com.ruoyi.mes.mapper.*;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.domain.BomInfo;
import com.ruoyi.mes.service.IBomInfoService;

/**
 * BOM管理Service业务层处理
 * 
 * @author g4
 * @date 2023-04-11
 */
@Service
public class BomInfoServiceImpl implements IBomInfoService 
{
    @Autowired
    private IBomInfoService bomInfoService;

    @Autowired
    private BomInfoMapper bomInfoMapper;
    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private MaterialMapper materialMapper;

    @Autowired
    private BomMaterialMapper bomMaterialMapper;
    /**
     * 查询BOM管理
     * 
     * @param id BOM管理主键
     * @return BOM管理
     */
    @Override
    public BomInfo selectBomInfoById(Long id)
    {
        return bomInfoMapper.selectBomInfoById(id);
    }

    /**
     * 查询BOM管理列表
     * 
     * @param bomInfo BOM管理
     * @return BOM管理
     */
    @Override
    public List<BomInfo> selectBomInfoList(BomInfo bomInfo)
    {
        List<BomInfo> b = bomInfoMapper.selectBomInfoList(bomInfo);
        for (BomInfo bs : b) {
            ProductInfo p = productInfoMapper.selectProductInfoByBomCode(bs.getBomCode());
            System.out.println(p.getProductName());
            System.out.println(p.getBomCode());
            bs.setProductName(p.getProductName());
            bs.setProductCode(p.getBomCode());
        }
        return b;
    }

    /**
     * 新增BOM管理
     *
     * @param bomInfo BOM管理
     * @return 结果
     */
    @Override
    public AjaxResult insertBomInfo(BomInfo bomInfo)
    {
        //判断bom编码是否重复
        BomInfo bomInfo1 = new BomInfo();
        bomInfo1.setBomCode(bomInfo.getBomCode());
        List<BomInfo> bomInfos = bomInfoMapper.selectBomInfoList(bomInfo1);
        if(bomInfos.size() != 0){
            return AjaxResult.error("Bom编码已存在!");
        }
        bomInfoMapper.insertBomInfo(bomInfo1);
        return AjaxResult.success("新增成功!");
    }

    /**
     * 修改BOM管理
     * 
     * @param bomInfo BOM管理
     * @return 结果
     */
    @Override
    public int updateBomInfo(BomInfo bomInfo)
    {
        return bomInfoMapper.updateBomInfo(bomInfo);
    }

    /**
     * 批量删除BOM管理
     * 
     * @param ids 需要删除的BOM管理主键
     * @return 结果
     */
    @Override
    public int deleteBomInfoByIds(Long[] ids)
    {
        return bomInfoMapper.deleteBomInfoByIds(ids);
    }

    /**
     * 删除BOM管理信息
     * 
     * @param id BOM管理主键
     * @return 结果
     */
    @Override
    public int deleteBomInfoById(Long id)
    {
        return bomInfoMapper.deleteBomInfoById(id);
    }






    /**
     * 增 - bom物料信息
     * @param bomMatVo
     * @return
     */
    @Override
    public AjaxResult insertBomMatInfo(BomMatVo bomMatVo) {
        BomMatVo bomMatVo1 = new BomMatVo();
        bomMatVo1.setBomCode(bomMatVo.getBomCode());
        List<BomMatVo> bomMatVos = bomMaterialMapper.selectbomMatVo(bomMatVo1);
        if(bomMatVos.size() != 0){
            return AjaxResult.error("BOM编码已存在");
        }


        //新增一条Bom数据
        BomInfo bomInfo = new BomInfo();
        //把接到的bomcode放到bom表里
        bomInfo.setBomCode(bomMatVo.getBomCode());
        bomInfoService.insertBomInfo(bomInfo);

            //往Bom物料中间表赛数据 遍历物料list
            List<Material> materials = bomMatVo.getMaterials();
            for (Material material : materials) {
                BomMaterial bomMaterial = new BomMaterial();
                bomMaterial.setBomCode(bomMatVo.getBomCode());
                bomMaterial.setMaterialName(material.getMaterialName());
                bomMaterial.setNeedMatNum(material.getMaterialNum());
                bomMaterialMapper.insertBomMaterial(bomMaterial);
            }

        //将bomCode添加到产品表
        BomInfo bomInfo1 = bomInfoMapper.selectBomInfoByCode(bomMatVo.getBomCode());
        ProductInfo productInfo = productInfoMapper.selectProductInfoByName(bomMatVo.getProductName());
        productInfo.setBomCode(bomInfo1.getBomCode());
        productInfoMapper.updateProductInfo(productInfo);
        return AjaxResult.success("新增成功！");
    }




    /**
     * 查 - bom物料信息 - bomcode
     * @param bomCode
     * @return
     */
    @Override
    public AjaxResult selectBomMatInfoByCode(String bomCode) {
        List<BomMaterial> materials = bomMaterialMapper.selectMaterialsByBomCode(bomCode);
        return AjaxResult.success(materials);
    }


    /**
     * 修改BOM管理
     */
    @Override
    public int editbomBomInfo(BomMatVo bomMatVo) {
        bomInfoMapper.deleteBomInfoByCode(bomMatVo.getBomCode());
        insertBomMatInfo(bomMatVo);
        return 1;
    }

    /**
     * 删除BOM
     */
    @Override
    public int deleteBomInfoByCode(String bomCode) {
        bomInfoMapper.deleteBomInfoByCode(bomCode);
        ProductInfo productInfo = productInfoMapper.selectProductInfoByBomCode(bomCode);
        productInfo.setBomCode(null);
        productInfoMapper.updateProductInfo(productInfo);
        return 1;
    }


}
