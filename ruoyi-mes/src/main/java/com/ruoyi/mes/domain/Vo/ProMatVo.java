package com.ruoyi.mes.domain.Vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.mes.domain.Material;
import com.ruoyi.mes.domain.ProcessMaterial;
import lombok.Data;

import java.util.List;

@Data
public class ProMatVo {

    /**
     * 工艺编码
     */
    private String craftCode;

    /** 工序名称 */
    @Excel(name = "工序名称")
    private String proccessName;

    /** 工序编码 */
    private String processCode;

    /** 原料集合 */
    public List<Material> materials;




}
