package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.mes.domain.Vo.SellOutVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.OutboundSell;
import com.ruoyi.mes.service.IOutboundSellService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 销售出库Controller
 * 
 * @author g4
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/sell")
public class OutboundSellController extends BaseController
{
    @Autowired
    private IOutboundSellService outboundSellService;

    /**
     * 查询销售出库列表
     */
    @PreAuthorize("@ss.hasPermi('mes:sell:list')")
    @GetMapping("/list")
    public TableDataInfo list(OutboundSell outboundSell)
    {
        startPage();
        List<OutboundSell> list = outboundSellService.selectOutboundSellList(outboundSell);
        return getDataTable(list);
    }

    /**
     * 导出销售出库列表
     */
    @PreAuthorize("@ss.hasPermi('mes:sell:export')")
    @Log(title = "销售出库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, OutboundSell outboundSell)
    {
        List<OutboundSell> list = outboundSellService.selectOutboundSellList(outboundSell);
        ExcelUtil<OutboundSell> util = new ExcelUtil<OutboundSell>(OutboundSell.class);
        util.exportExcel(response, list, "销售出库数据");
    }

    /**
     * 获取销售出库详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:sell:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(outboundSellService.selectOutboundSellById(id));
    }

    /**
     * 新增销售出库
     */
    @PreAuthorize("@ss.hasPermi('mes:sell:add')")
    @Log(title = "销售出库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody OutboundSell outboundSell)
    {
        return toAjax(outboundSellService.insertOutboundSell(outboundSell));
    }

    /**
     * 修改销售出库
     */
    @PreAuthorize("@ss.hasPermi('mes:sell:edit')")
    @Log(title = "销售出库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody OutboundSell outboundSell)
    {
        return toAjax(outboundSellService.updateOutboundSell(outboundSell));
    }

    /**
     * 删除销售出库
     */
    @PreAuthorize("@ss.hasPermi('mes:sell:remove')")
    @Log(title = "销售出库", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(outboundSellService.deleteOutboundSellByIds(ids));
    }

    /**
     * 销售出货单
     * @param sellOutVo
     * @return
     */
    @PostMapping("/sellout")
    public AjaxResult sellout(@RequestBody SellOutVo sellOutVo){
        return outboundSellService.selloutByVo(sellOutVo);
    }

    /**
     * 查看出货单
     */
    @GetMapping("/looksellout/{ordCode}")
    public AjaxResult looksellout(@PathVariable String ordCode){
        return outboundSellService.lookselloutByCode(ordCode);
    }

}
