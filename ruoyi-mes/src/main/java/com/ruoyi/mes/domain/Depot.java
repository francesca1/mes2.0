package com.ruoyi.mes.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 库房管理对象 depot
 * 
 * @author g4
 * @date 2023-04-11
 */
public class Depot extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 仓库id */
    private Long id;

    /** 仓库编码 */
    @Excel(name = "仓库编码")
    private String deportCode;

    /** 仓库名称 */
    @Excel(name = "仓库名称")
    private String depotName;

    /** 仓库类型 */
    @Excel(name = "仓库类型")
    private String depotType;

    /** 仓库状态 */
    @Excel(name = "仓库状态")
    private String depotStatus;

    /** 总存储数量 */
    @Excel(name = "总存储数量")
    private Long allNum;

    /** 已存储数量 */
    private Long alreadyNum;

    /** 库位编码 */
    private Integer binLocation;

    /** 创建人 */
    @Excel(name = "创建人")
    private String userId;

    /** 备注 */
    private String note;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDeportCode(String deportCode) 
    {
        this.deportCode = deportCode;
    }

    public String getDeportCode() 
    {
        return deportCode;
    }
    public void setDepotName(String depotName) 
    {
        this.depotName = depotName;
    }

    public String getDepotName() 
    {
        return depotName;
    }
    public void setDepotType(String depotType) 
    {
        this.depotType = depotType;
    }

    public String getDepotType() 
    {
        return depotType;
    }
    public void setDepotStatus(String depotStatus) 
    {
        this.depotStatus = depotStatus;
    }

    public String getDepotStatus() 
    {
        return depotStatus;
    }
    public void setAllNum(Long allNum) 
    {
        this.allNum = allNum;
    }

    public Long getAllNum() 
    {
        return allNum;
    }
    public void setAlreadyNum(Long alreadyNum) 
    {
        this.alreadyNum = alreadyNum;
    }

    public Long getAlreadyNum() 
    {
        return alreadyNum;
    }
    public void setBinLocation(Integer binLocation) 
    {
        this.binLocation = binLocation;
    }

    public Integer getBinLocation() 
    {
        return binLocation;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("deportCode", getDeportCode())
            .append("depotName", getDepotName())
            .append("depotType", getDepotType())
            .append("depotStatus", getDepotStatus())
            .append("allNum", getAllNum())
            .append("alreadyNum", getAlreadyNum())
            .append("binLocation", getBinLocation())
            .append("userId", getUserId())
            .append("createTime", getCreateTime())
            .append("note", getNote())
            .toString();
    }
}
