package com.ruoyi.mes.domain.Vo;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.mes.domain.Material;
import com.ruoyi.mes.domain.Process;
import com.ruoyi.mes.domain.ProcessMaterial;
import lombok.Data;

import java.util.List;

@Data
public class CraftProVo {

      /**
       * 工艺id
       */
      private Long craftId;

      /**
       * 工艺编码
       */
      private String craftCode;
      /**
       * 工艺名称
       */
      private String craftName;

      /**
       * 产品名称
       */
      private String productName;
      /**
       * 工序集合
       */
      public List<Process> processes ;


      public List<Material> materials;

      /**
       * 工序物料集合
       */
      public List<ProcessMaterial> processMaterials;

      /** 备注 */
      private String note;


}
