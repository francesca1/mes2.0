package com.ruoyi.mes.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.mapper.DepotMapper;
import com.ruoyi.mes.domain.Depot;
import com.ruoyi.mes.service.IDepotService;

/**
 * 库房管理Service业务层处理
 * 
 * @author g4
 * @date 2023-04-11
 */
@Service
public class DepotServiceImpl implements IDepotService 
{
    @Autowired
    private DepotMapper depotMapper;

    /**
     * 查询库房管理
     * 
     * @param id 库房管理主键
     * @return 库房管理
     */
    @Override
    public Depot selectDepotById(Long id)
    {
        return depotMapper.selectDepotById(id);
    }

    /**
     * 查询库房管理列表
     * 
     * @param depot 库房管理
     * @return 库房管理
     */
    @Override
    public List<Depot> selectDepotList(Depot depot)
    {
        return depotMapper.selectDepotList(depot);
    }

    /**
     * 新增库房管理
     * 
     * @param depot 库房管理
     * @return 结果
     */
    @Override
    public int insertDepot(Depot depot)
    {
        depot.setCreateTime(DateUtils.getNowDate());
        return depotMapper.insertDepot(depot);
    }

    /**
     * 修改库房管理
     * 
     * @param depot 库房管理
     * @return 结果
     */
    @Override
    public int updateDepot(Depot depot)
    {
        return depotMapper.updateDepot(depot);
    }

    /**
     * 批量删除库房管理
     * 
     * @param ids 需要删除的库房管理主键
     * @return 结果
     */
    @Override
    public int deleteDepotByIds(Long[] ids)
    {
        return depotMapper.deleteDepotByIds(ids);
    }

    /**
     * 删除库房管理信息
     * 
     * @param id 库房管理主键
     * @return 结果
     */
    @Override
    public int deleteDepotById(Long id)
    {
        return depotMapper.deleteDepotById(id);
    }
}
