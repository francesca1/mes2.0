package com.ruoyi.mes.mapper;

import java.util.List;
import com.ruoyi.mes.domain.ProductPlan;

/**
 * 生产计划Mapper接口
 * 
 * @author g4
 * @date 2023-05-08
 */
public interface ProductPlanMapper 
{
    /**
     * 查询生产计划
     * 
     * @param id 生产计划主键
     * @return 生产计划
     */
    public ProductPlan selectProductPlanById(Long id);

    /**
     * 查询生产计划列表
     * 
     * @param productPlan 生产计划
     * @return 生产计划集合
     */
    public List<ProductPlan> selectProductPlanList(ProductPlan productPlan);

    /**
     * 新增生产计划
     * 
     * @param productPlan 生产计划
     * @return 结果
     */
    public int insertProductPlan(ProductPlan productPlan);

    /**
     * 修改生产计划
     * 
     * @param productPlan 生产计划
     * @return 结果
     */
    public int updateProductPlan(ProductPlan productPlan);

    /**
     * 删除生产计划
     * 
     * @param id 生产计划主键
     * @return 结果
     */
    public int deleteProductPlanById(Long id);

    /**
     * 批量删除生产计划
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteProductPlanByIds(Long[] ids);

    ProductPlan selectProductPlanByCode(String proplanCode);

}
