package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.mes.service.IBomInfoService;
import com.ruoyi.mes.service.ICraftService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.ProductInfo;
import com.ruoyi.mes.service.IProductInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 产品管理Controller
 * 
 * @author g4
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/product")
public class ProductInfoController extends BaseController
{
    @Autowired
    private IProductInfoService productInfoService;

    @Autowired
    private IBomInfoService bomInfoService;

    @Autowired
    private ICraftService craftService;


    /**
     * 查询产品管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:product:list')")
    @GetMapping("/list")
    public TableDataInfo list(ProductInfo productInfo)
    {
        startPage();
        List<ProductInfo> list = productInfoService.selectProductInfoList(productInfo);
        return getDataTable(list);
    }


    /**
     * 导出产品管理列表
     */
    @PreAuthorize("@ss.hasPermi('mes:product:export')")
    @Log(title = "产品管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ProductInfo productInfo)
    {
        List<ProductInfo> list = productInfoService.selectProductInfoList(productInfo);
        ExcelUtil<ProductInfo> util = new ExcelUtil<ProductInfo>(ProductInfo.class);
        util.exportExcel(response, list, "产品管理数据");
    }

    /**
     * 获取产品管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:product:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(productInfoService.selectProductInfoById(id));
    }

    /**
     * 新增产品管理
     */
    @PreAuthorize("@ss.hasPermi('mes:product:add')")
    @Log(title = "产品管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ProductInfo productInfo)
    {

        return productInfoService.insertProductInfo(productInfo);
    }

    /**
     * 修改产品管理
     */
    @PreAuthorize("@ss.hasPermi('mes:product:edit')")
    @Log(title = "产品管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ProductInfo productInfo)
    {
        return toAjax(productInfoService.updateProductInfo(productInfo));
    }

    /**
     * 删除产品管理
     */
    @PreAuthorize("@ss.hasPermi('mes:product:remove')")
    @Log(title = "产品管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(productInfoService.deleteProductInfoByIds(ids));
    }

    /**
     * 删除产品管理
     */
    @PreAuthorize("@ss.hasPermi('mes:product:remove')")
    @Log(title = "产品管理", businessType = BusinessType.DELETE)
    @DeleteMapping("removePro/{id}")
    public AjaxResult removePro(@PathVariable Long id)
    {
        return toAjax(productInfoService.deleteProductInfoById(id));
    }

    /**
     * 获取产品bom信息
     * @param productId
     * @return
     */
    @PreAuthorize("@ss.hasPermi('mes:product:query')")
    @GetMapping(value = "/bomInfo/{productId}")
    public AjaxResult getbomInfo(@PathVariable("productId") Long productId)
    {
        return productInfoService.getbomInfo(productId);

    }

    /**
     * 获取产品工艺信息
     * @param productId
     * @return
     */
    @PreAuthorize("@ss.hasPermi('mes:product:query')")
    @GetMapping(value = "/craft/{productId}")
    public AjaxResult getcraftInfo(@PathVariable("productId") Long productId)
    {
        return productInfoService.getcraftInfo(productId);
    }

    /**
     * 查询bom为空的产品列表
     */
    @PreAuthorize("@ss.hasPermi('mes:product:list')")
    @GetMapping("/nullbomlist")
    public TableDataInfo nullbomlist(ProductInfo productInfo)
    {
        startPage();
        List<ProductInfo> list = productInfoService.selectnullbomlistList(productInfo);
        return getDataTable(list);
    }

    /**
     * 查询工艺为空的产品列表
     */
    @PreAuthorize("@ss.hasPermi('mes:product:list')")
    @GetMapping("/nullCraftList")
    public TableDataInfo nullCraftList(ProductInfo productInfo)
    {
        startPage();
        List<ProductInfo> list = productInfoService.selectnullCraftList(productInfo);
        return getDataTable(list);
    }


}
