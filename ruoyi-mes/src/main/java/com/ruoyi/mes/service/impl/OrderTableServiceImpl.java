package com.ruoyi.mes.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.mes.Util.CodeUtils;
import com.ruoyi.mes.domain.*;
import com.ruoyi.mes.domain.Vo.OrdproVo;
import com.ruoyi.mes.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mes.service.IOrderTableService;

/**
 * 销售订单Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-04-11
 */
@Service
public class OrderTableServiceImpl implements IOrderTableService 
{
    @Autowired
    private OrderTableMapper orderTableMapper;

    @Autowired
    private ProductInfoMapper productInfoMapper;

    @Autowired
    private OrdproMapper ordproMapper;

    @Autowired
    private MatPlanMapper matPlanMapper;

    @Autowired
    private ProductPlanMapper productPlanMapper;

    @Autowired
    private BomMaterialMapper bomMaterialMapper;

    @Autowired
    private MatPlanDetailMapper MatPlanDetailMapper;

    @Autowired
    private MaterialMapper materialMapper;

    @Autowired
    private ProductPlanDetialMapper productPlanDetialMapper;

    @Autowired
    private OutboundSellMapper outboundSellMapper;



    /**
     * 查询销售订单
     * 
     * @param id 销售订单主键
     * @return 销售订单
     */
    @Override
    public OrderTable selectOrderTableById(Long id)
    {
        return orderTableMapper.selectOrderTableById(id);
    }

    /**
     * 查询销售订单列表
     * 
     * @param orderTable 销售订单
     * @return 销售订单
     */
    @Override
    public List<OrderTable> selectOrderTableList(OrderTable orderTable)
    {
        return orderTableMapper.selectOrderTableList(orderTable);
    }

    /**
     * 新增销售订单
     * 
     * @param orderTable 销售订单
     * @return 结果
     */
    @Override
    public int insertOrderTable(OrderTable orderTable)
    {
        orderTable.setCreateTime(DateUtils.getNowDate());
        return orderTableMapper.insertOrderTable(orderTable);
    }

    /**
     * 修改销售订单
     * 
     * @param orderTable 销售订单
     * @return 结果
     */
    @Override
    public int updateOrderTable(OrderTable orderTable)
    {
        return orderTableMapper.updateOrderTable(orderTable);
    }

    /**
     * 批量删除销售订单
     * 
     * @param ids 需要删除的销售订单主键
     * @return 结果
     */
    @Override
    public int deleteOrderTableByIds(Long[] ids)
    {
        return orderTableMapper.deleteOrderTableByIds(ids);
    }

    /**
     * 删除销售订单信息
     * 
     * @param id 销售订单主键
     * @return 结果
     */
    @Override
    public int deleteOrderTableById(Long id)
    {
        return orderTableMapper.deleteOrderTableById(id);
    }

    /**
     * 新增产品销售订单
     *
     * @param ordproVo
     * @return
     */
    @Override
    public AjaxResult insertaddOrderTable(OrdproVo ordproVo) {
        //新增销售订单
        OrderTable orderTable = new OrderTable();
        orderTable.setOrdCode(ordproVo.getOrdCode());
        orderTable.setOrdName(ordproVo.getOrdName());
        orderTable.setOrdType(ordproVo.getOrdType());
        orderTable.setSignDate(ordproVo.getSignDate());
        orderTable.setDeadline(ordproVo.getDeadline());
        orderTable.setCustomName(ordproVo.getCustomName());
        orderTableMapper.insertOrderTable(orderTable);


         //销售订单的产品
        List<ProductInfo> productInfos = ordproVo.getProductInfos();
        for (ProductInfo productInfo : productInfos) {
            Ordpro ordpro = new Ordpro();
            ordpro.setOrdCode(ordproVo.getOrdCode());
            ordpro.setProductName(productInfo.getProductName());
            ordpro.setProductCode(productInfo.getProductCode());
            ordpro.setProductNum(productInfo.getProductNum());
            ordpro.setProprice(productInfo.getProprice());
            ordproMapper.insertOrdpro(ordpro);
        }

        //新增物料计划
        String mtrPlanCode = CodeUtils.MtrPlanCodeCreat();
        MatPlan matPlan = new MatPlan();
        matPlan.setMatplanCode(mtrPlanCode);
        matPlan.setOrdCode(ordproVo.getOrdCode());
        matPlan.setOrdName(ordproVo.getOrdName());
        Ordpro ordpro = ordproMapper.selectOrdproByOrdCode(ordproVo.getOrdCode());
        matPlan.setProductName(ordpro.getProductName());
        matPlan.setProductCode(ordpro.getProductCode());
        matPlan.setProductNum(ordpro.getProductNum());
        matPlan.setMatPlanStatus("未生成");
        matPlanMapper.insertMatPlan(matPlan);

        //生成物料计划明细
        ProductInfo productInfo = productInfoMapper.selectProductInfoByName(ordpro.getProductName());
        List<BomMaterial> bomMaterials = bomMaterialMapper.selectMaterialsByBomCode(productInfo.getBomCode());
        for (BomMaterial bomMaterial : bomMaterials) {
            MatPlanDetail matPlanDetail = new MatPlanDetail();
            matPlanDetail.setMatplanCode(matPlan.getMatplanCode());
            matPlanDetail.setMaterialName(bomMaterial.getMaterialName());
            Material material = materialMapper.selectMaterialByName(bomMaterial.getMaterialName());
            matPlanDetail.setMaterialCode(material.getMaterialCode());
            matPlanDetail.setNeedNum(bomMaterial.getNeedMatNum()*ordpro.getProductNum());
            MatPlanDetailMapper.insertMatPlanDetail(matPlanDetail);
        }


        //新增生产计划
        String progCode = CodeUtils.ProgCodeCreat();
        ProductPlan productPlan = new ProductPlan();
        productPlan.setProplanCode(progCode);
        productPlan.setOrdCode(ordpro.getOrdCode());
        productPlan.setOrdName(ordproVo.getOrdName());
        productPlan.setProductName(ordpro.getProductName());
        productPlan.setProductCode(ordpro.getProductCode());
        productPlan.setNeedNum(ordpro.getProductNum());
        productPlan.setPlanStatus("未生成");
        productPlanMapper.insertProductPlan(productPlan);

        //新增销售出货单

//        ProductPlan productPlan1 = productPlanMapper.selectProductPlanByCode(ordproVo.getOrdCode());
        OutboundSell outboundSell = new OutboundSell();
        outboundSell.setOrdCode(ordproVo.getOrdCode());
        outboundSell.setProplanCode(productPlan.getProplanCode());
        outboundSell.setOutboundStatus("未出库");
        outboundSell.setOutboundNum(productPlan.getNeedNum());
        outboundSell.setOutboundType("销售出库");
        outboundSellMapper.insertOutboundSell(outboundSell);

        return AjaxResult.success("新增成功");
    }

    /**
     * 查看订单详细信息-产品
     */
    @Override
    public AjaxResult selectOrderTableByCode(String ordCode) {
        Ordpro ordpro = new Ordpro();
        ordpro.setOrdCode(ordCode);
        List<Ordpro> ordpros = ordproMapper.selectOrdproList(ordpro);

        return AjaxResult.success(ordpros);
    }


}
