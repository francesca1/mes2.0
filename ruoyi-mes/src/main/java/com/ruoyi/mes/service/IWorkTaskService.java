package com.ruoyi.mes.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.mes.domain.Vo.PickVo;
import com.ruoyi.mes.domain.Vo.WorkTaskVo;
import com.ruoyi.mes.domain.WorkTask;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 车间任务Service接口
 * 
 * @author g4
 * @date 2023-05-22
 */
public interface IWorkTaskService 
{
    /**
     * 查询车间任务
     * 
     * @param id 车间任务主键
     * @return 车间任务
     */
    public WorkTask selectWorkTaskById(Long id);

    /**
     * 查询车间任务列表
     * 
     * @param workTask 车间任务
     * @return 车间任务集合
     */
    public List<WorkTask> selectWorkTaskList(WorkTask workTask);

    /**
     * 新增车间任务
     * 
     * @param workTask 车间任务
     * @return 结果
     */
    public int insertWorkTask(WorkTask workTask);

    /**
     * 修改车间任务
     * 
     * @param workTask 车间任务
     * @return 结果
     */
    public int updateWorkTask(WorkTask workTask);

    /**
     * 批量删除车间任务
     * 
     * @param ids 需要删除的车间任务主键集合
     * @return 结果
     */
    public int deleteWorkTaskByIds(Long[] ids);

    /**
     * 删除车间任务信息
     * 
     * @param id 车间任务主键
     * @return 结果
     */
    public int deleteWorkTaskById(Long id);

    /**
     * 确认开工
     */
    AjaxResult insertsubmitStart(WorkTaskVo workTaskVo);

    /**
     * 领料清单
     */
    AjaxResult selectgetPickMatByCode(String workTaskCode);

    /**
     * 确定领料
     */
    AjaxResult insertsubmitPicking(PickVo pickVo);
}
