package com.ruoyi.mes.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 产品管理对象 product_info
 * 
 * @author g4
 * @date 2023-04-11
 */
@Data
public class ProductInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 产品id */
    private Long id;

    /** 产品名称 */
    @Excel(name = "产品名称")
    private String productName;

    /** 产品类型 */
    @Excel(name = "产品类型")
    private String productType;

    /** 产品编码 */
    @Excel(name = "产品编码")
    private String productCode;

    /** 产品规格 */
    @Excel(name = "产品规格")
    private String productSpecs;

    /** 状态 */
    private String status;

    /** 父产品编码 */
    private String parentProNum;

    /** bomcode */
    @Excel(name = "bomCode")
    private String bomCode;

    /** 工艺编码 */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @Excel(name = "工艺编码")
    private Long craftId;

    /** 仓库id */
    private Long depotId;

    /** 库存数量 */
    private Long invenNum;

    /** 价格 */
    private String price;

    /** 备注 */
    private String note;

    /** 创建日期 */
    private Date createDate;

    /** 创建人 */
    @Excel(name = "创建人")
    private String userId;

    @TableField
    /** 产品数量 */
    @Excel(name = "产品数量")
    private Long productNum;

    @TableField
    /** 产品价格 */
    @Excel(name = "产品价格")
    private BigDecimal proprice;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProductName(String productName) 
    {
        this.productName = productName;
    }

    public String getProductName() 
    {
        return productName;
    }
    public void setProductType(String productType) 
    {
        this.productType = productType;
    }

    public String getProductType() 
    {
        return productType;
    }
    public void setProductCode(String productCode) 
    {
        this.productCode = productCode;
    }

    public String getProductCode() 
    {
        return productCode;
    }
    public void setProductSpecs(String productSpecs) 
    {
        this.productSpecs = productSpecs;
    }

    public String getProductSpecs() 
    {
        return productSpecs;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setParentProNum(String parentProNum) 
    {
        this.parentProNum = parentProNum;
    }

    public String getParentProNum() 
    {
        return parentProNum;
    }

    public String getBomCode() {
        return bomCode;
    }

    public void setBomCode(String bomCode) {
        this.bomCode = bomCode;
    }
    public void setCraftCode(String craftCode) {
    }



    public void setDepotId(Long depotId)
    {
        this.depotId = depotId;
    }

    public Long getDepotId() 
    {
        return depotId;
    }
    public void setInvenNum(Long invenNum) 
    {
        this.invenNum = invenNum;
    }

    public Long getInvenNum() 
    {
        return invenNum;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setNote(String note)
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setCreateDate(Date createDate) 
    {
        this.createDate = createDate;
    }

    public Date getCreateDate() 
    {
        return createDate;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("productName", getProductName())
            .append("productType", getProductType())
            .append("productCode", getProductCode())
            .append("productSpecs", getProductSpecs())
            .append("status", getStatus())
            .append("parentProNum", getParentProNum())
            .append("bomCode", getBomCode())
            .append("craftId", getCraftId())
            .append("depotId", getDepotId())
            .append("invenNum", getInvenNum())
            .append("price", getPrice())
            .append("note", getNote())
            .append("createDate", getCreateDate())
            .append("userId", getUserId())
            .toString();
    }


}
