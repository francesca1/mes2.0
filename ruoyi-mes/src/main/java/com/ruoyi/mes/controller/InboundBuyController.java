package com.ruoyi.mes.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.mes.domain.Vo.BinDepotVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.mes.domain.InboundBuy;
import com.ruoyi.mes.service.IInboundBuyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 采购入库Controller
 * 
 * @author g4
 * @date 2023-04-11
 */
@RestController
@RequestMapping("/mes/Inboundbuy")
public class InboundBuyController extends BaseController
{
    @Autowired
    private IInboundBuyService inboundBuyService;

    /**
     * 查询采购入库列表
     */
    @PreAuthorize("@ss.hasPermi('mes:Inboundbuy:list')")
    @GetMapping("/list")
    public TableDataInfo list(InboundBuy inboundBuy)
    {
        startPage();
        List<InboundBuy> list = inboundBuyService.selectInboundBuyList(inboundBuy);
        return getDataTable(list);
    }

    /**
     * 导出采购入库列表
     */
    @PreAuthorize("@ss.hasPermi('mes:Inboundbuy:export')")
    @Log(title = "采购入库", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, InboundBuy inboundBuy)
    {
        List<InboundBuy> list = inboundBuyService.selectInboundBuyList(inboundBuy);
        ExcelUtil<InboundBuy> util = new ExcelUtil<InboundBuy>(InboundBuy.class);
        util.exportExcel(response, list, "采购入库数据");
    }

    /**
     * 获取采购入库详细信息
     */
    @PreAuthorize("@ss.hasPermi('mes:Inboundbuy:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(inboundBuyService.selectInboundBuyById(id));
    }

    /**
     * 新增采购入库
     */
    @PreAuthorize("@ss.hasPermi('mes:Inboundbuy:add')")
    @Log(title = "采购入库", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody InboundBuy inboundBuy)
    {
        return toAjax(inboundBuyService.insertInboundBuy(inboundBuy));
    }

    /**
     * 修改采购入库
     */
    @PreAuthorize("@ss.hasPermi('mes:Inboundbuy:edit')")
    @Log(title = "采购入库", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody InboundBuy inboundBuy)
    {
        return toAjax(inboundBuyService.updateInboundBuy(inboundBuy));
    }

    /**
     * 删除采购入库
     */
    @PreAuthorize("@ss.hasPermi('mes:Inboundbuy:remove')")
    @Log(title = "采购入库", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(inboundBuyService.deleteInboundBuyByIds(ids));
    }

    /**
     * 确认采购入库-确认入库库存增加
     */
    @PostMapping("/addstocks")
    public AjaxResult addstocks(@RequestBody BinDepotVo binDepotVo)
    {
        return inboundBuyService.insertaddstocks(binDepotVo);
    }
}
